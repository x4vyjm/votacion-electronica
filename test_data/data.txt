#####################################
#### INFORMACION GENERAL ####
#####################################
Title: Proceso electoral de elecciones generales
Description:  Proceso electoral de elecciones generales para el año 2020 el cual se desarrollará con el fin de elegir Al presidente, vicepresidente y congresistas de nuestra nación. Así mismo, este es un proceso electoral electrónico.

#####################################
#### LISTA DE PARTIDOS POLITICOS ####
#####################################
Partido político La Luz
    Candidatos:
        Randolph Tilliards MacEllen
        Miner Alishoner Junkinson
        Cobbie Statham Grunder
        Gail Bohje Fredy
        Whittaker Hirsch Marguerite
        Sindee Albury Luney
    
Partido político Nuevo Amanecer
    Candidatos:
        Thorin Ambrose Linck
        Wilie McCullogh Hegden
        Cherise Mac Drissell
        Wilfred Pelchat Kaine

Partido político La cosecha
    Candidatos:
        Caterina Wallwood Jukubczak
        Pip Onians Knoles
        Rasia Aireton Dict
        Patrizia Bodman Fardy

Partido político Somos Peru
    Candidatos:
        Quincey Feye Cramond
        Locke Queripel Kindred
        Kristan Archbald Uzielli
        Theodore Clibbery Rackstraw
        Daniel Orro Ambrosoli
        Aubrette Purry Spivie
        Christi Verbrugge Bello

Partido político Peru pa'lante
    Candidatos:
        Emlyn Trulock Munehay
        Datha Castree Carmont
        Merrili Cullerne Streeten
        Keely Gillard Longmaid
        Kelci Thorneywork Berston
        Cyndi Charman Skym
        Arni Russon Paolazzi

Partido político Vamos creciendo
    Candidatos:
        Garnette Clendening Cleeves
        Dilly Kumaar Maliphant
        Danila MacCarrick Jeduch
        Nadia Perez Elsom
        Baxter Broadhead York
        Ajay Lowden Tatlow
        Nert Verbeek Abrahams
        Erik Isham Ainsbury


#####################################
######### LISTA DE VOTACIÓN #######
#####################################
----------------------------------------------------------------
Titulo: Lista de votación para elección de presidente
Description: Seleccione al partido político de su preferencia para ser elegido presidente. 
    Partido político La Luz
        Candidatos:
            Randolph Tilliards MacEllen
        
    Partido político Nuevo Amanecer
        Candidatos:
            Thorin Ambrose Linck

    Partido político La cosecha
        Candidatos:
            Caterina Wallwood Jukubczak

    Partido político Somos Peru
        Candidatos:
            Quincey Feye Cramond

    Partido político Peru pa'lante
        Candidatos:
            Emlyn Trulock Munehay

    Partido político Vamos creciendo
        Candidatos:
            Garnette Clendening Cleeves

-----------------------------------------------------------------
Titulo: Lista de votación para elección de vicepresidente
Description: Seleccione al partido político de su preferencia para ser elegido vicepresidente. 
    Partido político La Luz
        Candidatos:
            Miner Alishoner Junkinson
        
    Partido político Nuevo Amanecer
        Candidatos:
            Wilie McCullogh Hegden

    Partido político La cosecha
        Candidatos:
            Pip Onians Knoles

    Partido político Somos Peru
        Candidatos:
            Locke Queripel Kindred

    Partido político Peru pa'lante
        Candidatos:
            Datha Castree Carmont

    Partido político Vamos creciendo
        Candidatos:
            Dilly Kumaar Maliphant

-----------------------------------------------------------------
Titulo: Lista de votación para elección de congresistas
Description: Seleccione al partido político de su preferencia en el cual se encuentre algún congresista de su preferencia. 
    Partido político La Luz
        Candidatos:
            Cobbie Statham Grunder
            Gail Bohje Fredy
        
    Partido político Nuevo Amanecer
        Candidatos:
            Cherise Mac Drissell

    Partido político La cosecha
        Candidatos:
            Rasia Aireton Dict

    Partido político Somos Peru
        Candidatos:
            Kristan Archbald Uzielli
            Theodore Clibbery Rackstraw
            Daniel Orro Ambrosoli

    Partido político Peru pa'lante
        Candidatos:
            Merrili Cullerne Streeten
            Keely Gillard Longmaid
            Kelci Thorneywork Berston

    Partido político Vamos creciendo
        Candidatos:
            Danila MacCarrick Jeduch
            Nadia Perez Elsom
            Baxter Broadhead York
            Ajay Lowden Tatlow
            Nert Verbeek Abrahams
            Erik Isham Ainsbury

-----------------------------------------------------------------
Titulo: Lista de votación para elección del parlamento andino
Description: Seleccione al partido político de su preferencia en el cual se encuentre algun miembro del parlamento andino de su preferencia. 
    Partido político La Luz
        Candidatos:
            Whittaker Hirsch Marguerite
            Sindee Albury Luney
        
    Partido político Nuevo Amanecer
        Candidatos:
            Wilfred Pelchat Kaine

    Partido político La cosecha
        Candidatos:
            Patrizia Bodman Fardy

    Partido político Somos Peru
        Candidatos:
            Aubrette Purry Spivie
            Christi Verbrugge Bello

    Partido político Peru pa'lante
        Candidatos:
            Cyndi Charman Skym
            Arni Russon Paolazzi