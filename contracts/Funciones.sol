// SPDX-License-Identifier: MIT
pragma solidity >=0.4.21 <0.7.0;

contract Funciones {
    //Compare strings
    function compareStrings(string memory cad1, string memory cad2)
        public
        pure
        returns (bool)
    {
        return (keccak256(abi.encodePacked(cad1)) ==
            keccak256(abi.encodePacked(cad2)));
    }

    function parseAddr(string memory _a)
        public
        pure
        returns (address _parsedAddress)
    {
        bytes memory tmp = bytes(_a);
        uint160 iaddr = 0;
        uint160 b1;
        uint160 b2;
        for (uint256 i = 2; i < 2 + 2 * 20; i += 2) {
            iaddr *= 256;
            b1 = uint160(uint8(tmp[i]));
            b2 = uint160(uint8(tmp[i + 1]));
            if ((b1 >= 97) && (b1 <= 102)) {
                b1 -= 87;
            } else if ((b1 >= 65) && (b1 <= 70)) {
                b1 -= 55;
            } else if ((b1 >= 48) && (b1 <= 57)) {
                b1 -= 48;
            }
            if ((b2 >= 97) && (b2 <= 102)) {
                b2 -= 87;
            } else if ((b2 >= 65) && (b2 <= 70)) {
                b2 -= 55;
            } else if ((b2 >= 48) && (b2 <= 57)) {
                b2 -= 48;
            }
            iaddr += (b1 * 16 + b2);
        }
        return address(iaddr);
    }

    function getSlice(string memory cad, uint256 pos)
        public
        pure
        returns (string memory)
    {
        bytes memory strBytes = bytes(cad);
        bytes memory result = new bytes(1);
        result[0] = strBytes[pos];

        return string(result);
    }

    function randModulus(uint256 mod, string memory _dni)
        public
        view
        returns (uint256)
    {
        uint256 rpta = uint256(
            keccak256(
                abi.encodePacked(
                    _dni,
                    block.timestamp,
                    block.difficulty,
                    msg.sender
                )
            )
        ) % mod;
        return rpta;
    }

    function toBinaryString(uint256 n, uint256 _length)
        public
        pure
        returns (string memory)
    {
        bytes memory output = new bytes(_length);

        for (uint8 i = 0; i < _length; i++) {
            output[_length - 1 - i] = (n % 2 == 1) ? bytes1("1") : bytes1("0");
            n /= 2;
        }

        return string(output);
    }
}
