pragma solidity >=0.5.0;
pragma experimental ABIEncoderV2;
import "./Funciones.sol";

contract Election {
    struct VotingList {
        uint256 index;
        string title;
        string description;
        string votoType;
    }

    struct Candidate {
        uint256 index;
        string name;
        uint256 voteCount;
        uint256 indexVotingList;
        uint256 indexPoliticalPartie;
    }

    struct PoliticalPartie {
        uint256 index;
        string name;
        mapping(uint256 => Candidate) candidates;
        uint256[] keyCandidatesList;
        uint256 cantCandidates;
        uint256 voteCount;
        uint256 indexVotingList;
    }

    struct User {
        string dni;
        string role;
        string name;
        string lastName;
    }

    struct Voter {
        uint256 index;
        string name;
        string lastName;
        string email;
        string dni;
        address publicKey;
        bool castVote;
        int [][] voteEncrypted;
    }
    mapping(string => Voter) public voters;
    string[] public keyVoterList;
    uint256 public cantVoterLists;

    // Atributes
    string public title;
    string public description;
    string public dateIni;
    string public dateEnd;
    bool public flagProcessStart;
    bool public flagProcessFinish;
    bool public flagSentEmailElectors;
    bool public flagSentEmailTallyAuthorities;
    User admin;

    string [] _reference = ["0","0","0"];
    uint256[][]  ciphertext_A;
    uint256[][]  ciphertext_B;
    uint256[][] y;

    function createCiphertextVoters() public onlyOwner {
        y = new uint256[][](cantVoterLists);
        ciphertext_A = new uint256[][](cantVoterLists);
        ciphertext_B = new uint256[][](cantVoterLists);

        for (uint256 pos = 0; pos < cantVoterLists; pos++) {
            uint256 _auxCiphertext = funciones.randModulus(
                cantPoliticalParties,
                keyVoterList[pos]
            );

            _reference[pos] = funciones.toBinaryString(
                _auxCiphertext,
                2**cantPoliticalParties - 1
            );
            y[pos] = new uint256[](cantPoliticalParties);
            ciphertext_A[pos] = new uint256[](cantPoliticalParties);
            ciphertext_B[pos] = new uint256[](cantPoliticalParties);

            for (
                uint256 posCand = 0;
                posCand < cantPoliticalParties;
                posCand++
            ) {
                y[pos][posCand] =
                    uint256(
                        keccak256(
                            abi.encodePacked(
                                posCand,
                                _reference[pos],
                                block.timestamp,
                                block.difficulty,
                                msg.sender
                            )
                        )
                    ) %
                    q_upper_limit;
                ciphertext_A[pos][posCand] = g_generator**y[pos][posCand];

                ciphertext_B[pos][posCand] = 1;
                for (uint256 posB = 0; posB < nTally_authorities; posB++) {
                    ciphertext_B[pos][posCand] =  ciphertext_B[pos][posCand] * tally_authorities[keyTallyAuthoritiesList[posB]].y_public_key;
                }
                ciphertext_B[pos][posCand] = ciphertext_B[pos][posCand]**y[pos][posCand];
                if (funciones.compareStrings(funciones.getSlice(_reference[pos], posCand), "0")) {
                    ciphertext_B[pos][posCand] = ciphertext_B[pos][posCand] * g_generator;
                } else {
                    ciphertext_B[pos][posCand] = ciphertext_B[pos][posCand] / g_generator;
                }
            }
        }
    }

    // Store accounts that have voted
    Funciones funciones;
    
    // Store political parties
    mapping(uint256 => PoliticalPartie) public politicalParties;
    uint256[] public keyPoliticalPartieList;
    uint256 public cantPoliticalParties;

    // Store voting list
    mapping(uint256 => VotingList) public votingLists;
    uint256[] public keyVotingList; 
    uint256 public cantVotingLists;

    //Owner of the smart contract
    address public owner;

    uint256 public q_upper_limit;
    uint256 public g_generator;

    // TallyAuthoritys
    struct TallyAuthority {
        string dni;
        address publicKey; //Ethereum Wallet
        string email;
        uint256 x_private_key; //For encryption
        uint256 y_public_key; //For encryption
    }

    // Store accounts that have voted
    mapping(string => TallyAuthority) public tally_authorities;
    string[] public keyTallyAuthoritiesList;
    uint256 public nTally_authorities;

    //CONSTRUCTOR
    constructor() public {
        funciones = new Funciones();
        owner = msg.sender;
        flagSentEmailElectors = false;
        flagSentEmailTallyAuthorities = false;
        flagProcessStart = false;
        flagProcessFinish = false;
        cantPoliticalParties = 0;
        cantVotingLists = 0;
        cantVoterLists = 0;
        admin = User({
            role: "admin",
            name: "Sebastian",
            lastName: "Sanchez Herrera",
            dni: "1111"
        });
        q_upper_limit = 0;
        g_generator = 0;
        title="";
        description="";
        dateIni="";
        dateEnd="";
    }

    // Modifiers
    modifier onlyOwner() {
        require(
            msg.sender == owner,
            "This function is restricted to the contract's owner"
        );
        _;
    }

    //voted event
    event votedEvent(address _addressCandidate, string dni);

    function addNewTallyAuthoritie(
        string memory _dni,
        string memory _publicKey,
        string memory _email
    ) public payable {
        TallyAuthority storage t_Authority = tally_authorities[_dni];

        if (keyTallyAuthoritiesList.length > nTally_authorities) {
            keyTallyAuthoritiesList[nTally_authorities] = _dni;
        } else {
            keyTallyAuthoritiesList.push(_dni);
        }

        nTally_authorities = nTally_authorities + 1;
        t_Authority.dni = _dni;
        t_Authority.publicKey = funciones.parseAddr(_publicKey);
        t_Authority.email = _email;
        t_Authority.x_private_key = 1;
        t_Authority.y_public_key = 1;
    }

    //Update tally authority
    function updateWalletTallyAuthoritie(
        string memory _dni,
        string memory _publicKey,
        string memory _email
    ) public payable {
        TallyAuthority storage t_Authority = tally_authorities[_dni];
        t_Authority.publicKey = funciones.parseAddr(_publicKey);
        t_Authority.email = _email;
    }

    //Delete tally authority
    function deleteWalletTallyAuthoritie(string memory _dni) public payable {
        for (uint8 pos = 0; pos < nTally_authorities; pos++) {
            if (funciones.compareStrings(keyTallyAuthoritiesList[pos], _dni)) {
                nTally_authorities = nTally_authorities - 1;
                delete keyTallyAuthoritiesList[pos];
                break;
            }
        }
    }

    //Get all tally authorities
    function getTallyAuthoritie(string memory _dni, address authoritie)
        public
        view
        returns (
            string memory dni,
            uint256 x_private_key, //For encryption
            uint256 y_public_key //For encryption
        )
    {
        if (authoritie == tally_authorities[_dni].publicKey) {
            return (
                _dni,
                tally_authorities[_dni].x_private_key,
                tally_authorities[_dni].y_public_key
            );
        } else {
            return (_dni, 0, 0);
        }
    }

    function setFlagProcessStart() public onlyOwner {
        flagProcessStart = true;
        //createCiphertextVoters();
    }

    //Set q and g values
    function save_Q_G_values(uint256 q, uint256 g) public payable onlyOwner {
        q_upper_limit = q;
        g_generator = g;
    }

    function setPublicKeyAndPrivateKeyEncryption(
        string memory _dni,
        uint256 _private_key
    ) public payable returns (uint256) {
        if (funciones.compareStrings(tally_authorities[_dni].dni, _dni)) {
            tally_authorities[_dni].x_private_key = _private_key;
            tally_authorities[_dni].y_public_key = g_generator**_private_key;

            return tally_authorities[_dni].y_public_key;
        } else {
            return 0;
        }
    }

    
    //CRUD Voters
    function getVoters() public view returns (string[] memory, string[] memory, string[] memory, string[] memory, address[] memory ) {
        string[] memory _dni = new string[](cantVoterLists);
        string[] memory _email = new string[](cantVoterLists);
        string[] memory _name = new string[](cantVoterLists);
        string[] memory _lastName = new string[](cantVoterLists);
        address[] memory _publicKey = new address[](cantVoterLists);

        for (uint256 pos = 0; pos < cantVoterLists; pos++) {
            _dni[pos] = voters[keyVoterList[pos]].dni;
            _email[pos] = voters[keyVoterList[pos]].email;
            _name[pos] = voters[keyVoterList[pos]].name;
            _lastName[pos] = voters[keyVoterList[pos]].lastName;
            _publicKey[pos] = voters[keyVoterList[pos]].publicKey;
        }
        return (_dni, _email, _name, _lastName, _publicKey);
    }

    function addVoter(
        string memory _name,
        string memory _lastName,
        string memory _email,
        string memory _dni,
        string memory _publicKey
    ) private {
        Voter storage voter = voters[_dni];

        if (keyVoterList.length > cantVoterLists) {
            keyVoterList[cantVoterLists] = _dni;
        } else {
            keyVoterList.push(_dni);
        }

        cantVoterLists = cantVoterLists + 1;
        voter.index = keyVoterList.length;
        voter.name = _name;
        voter.lastName = _lastName;
        voter.email = _email;
        voter.dni = _dni;
        voter.publicKey = funciones.parseAddr(_publicKey);
        voter.castVote = false;
    }

    function addVoters(
        string[] memory _dni,
        string[] memory _email,
        string[] memory _name,
        string[] memory _lastName,
        string[] memory _publicKey
    ) public payable onlyOwner {
        for (uint256 i = 0; i < keyVoterList.length; i++) {
            delete keyVoterList[i];
        }
        cantVoterLists = 0;

        for (uint256 pos = 0; pos < _dni.length; pos++) {
            addVoter(_name[pos], _lastName[pos], _email[pos], _dni[pos], _publicKey[pos]);
        }
    }

    function emitVote(string memory _dni, uint [] memory _vote) public{
        emit votedEvent(msg.sender, _dni);
        if (msg.sender == voters[_dni].publicKey) {
            int[][] memory voteEncrypted = new int[][](cantVotingLists);
            voters[_dni].castVote = true;
            
            for (uint256 posVL = 0; posVL < cantVotingLists; posVL++) {
                voteEncrypted[posVL] = new int[](cantPoliticalParties);
                
                for (uint256 posPP = 0; posPP < cantPoliticalParties; posPP++) {
                    if (funciones.compareStrings(funciones.getSlice(_reference[posVL], posPP), "0")) {
                        if (_vote[posVL] == posPP){ //YES
                            voteEncrypted[posVL][posPP] = 1;
                        }else {//NO
                            voteEncrypted[posVL][posPP] = -1;
                        }
                    }else {  // = "1"
                        if (_vote[posVL] == posPP){ //YES
                            voteEncrypted[posVL][posPP] = -1;
                        }else { //NO
                            voteEncrypted[posVL][posPP] = 1;
                        }
                    }
                }    
                voters[_dni].voteEncrypted = voteEncrypted;
            }
        } 
    }
    
    function getVoteEncrypted (string memory _dni,uint _votingList) public returns (int[] memory) {
        return voters[_dni].voteEncrypted[_votingList];
    }

    function getTallyAuthorities()
        public
        view
        returns (
            string[] memory,
            address[] memory,
            string[] memory,
            uint256[] memory
        )
    {
        string[] memory _dni = new string[](nTally_authorities);
        address[] memory _publicKey = new address[](nTally_authorities);
        string[] memory _email = new string[](nTally_authorities);
        uint256[] memory y_public_key = new uint256[](nTally_authorities);

        for (uint256 pos = 0; pos < nTally_authorities; pos++) {
            _dni[pos] = tally_authorities[keyTallyAuthoritiesList[pos]].dni;
            _publicKey[pos] = tally_authorities[keyTallyAuthoritiesList[pos]]
                .publicKey;
            _email[pos] = tally_authorities[keyTallyAuthoritiesList[pos]].email;
            y_public_key[pos] = tally_authorities[keyTallyAuthoritiesList[pos]]
                .y_public_key;
        }
        return (_dni, _publicKey, _email, y_public_key);
    }

    //Validate Login
    function validateLoginAdmin(string memory _dni, address _publicKey)
        public
        view
        returns (
            bool flag,
            string memory role,
            string memory name,
            string memory lastName
        )
    {
        if (_publicKey == owner && funciones.compareStrings(_dni, admin.dni)) {
            return (true, admin.role, admin.name, admin.lastName);
        }

        if (tally_authorities[_dni].publicKey == _publicKey) {
            return (true, "auditor", "Auditor", _dni);
        }
        return (false, "", "", "");
    }

    function validateLoginElector(string memory _publicKey, string memory _dni)
        public
        view
        returns (bool flag, bool _castVote, string memory _email)
    {
        if (funciones.parseAddr(_publicKey) == voters[_dni].publicKey) {
            if (funciones.compareStrings(_dni, voters[_dni].dni)) {
                return (true, voters[_dni].castVote, voters[_dni].email);
            }
            return (false, false, "");
        }
        return (false,  false, "");
    }

    //CRUD General Information
    function setGeneralInformation(
        string memory _title,
        string memory _description,
        string memory _dateIni,
        string memory _dateEnd
    ) public payable onlyOwner {
        title = _title;
        description = _description;
        dateIni = _dateIni;
        dateEnd = _dateEnd;
    }

    //CRUD PoliticalPartie
    function addCandidate(
        uint256 _key,
        string memory _name,
        uint256 _keyPoliticalPartie
    ) public {

            PoliticalPartie storage _politicalPartie
         = politicalParties[_keyPoliticalPartie];

        if (
            _politicalPartie.keyCandidatesList.length >
            _politicalPartie.cantCandidates
        ) {
            _politicalPartie.keyCandidatesList[_politicalPartie
                .cantCandidates] = _key;
        } else {
            _politicalPartie.keyCandidatesList.push(_key);
        }

        _politicalPartie.cantCandidates = _politicalPartie.cantCandidates + 1;

        Candidate storage candidate = _politicalPartie.candidates[_key];
        candidate.index = _politicalPartie.keyCandidatesList.length;
        candidate.name = _name;
        candidate.voteCount = 0;
        candidate.indexVotingList = 0;
        candidate.indexPoliticalPartie = _politicalPartie.index;
    }

    function addPoliticalPartie(uint256 _key, string memory _name) private {
        if (keyPoliticalPartieList.length > cantPoliticalParties) {
            keyPoliticalPartieList[cantPoliticalParties] = _key;
        } else {
            keyPoliticalPartieList.push(_key);
        }

        cantPoliticalParties = cantPoliticalParties + 1;

        PoliticalPartie storage politicalPartie = politicalParties[_key];
        politicalPartie.index = cantPoliticalParties;
        politicalPartie.name = _name;
        politicalPartie.cantCandidates = 0;
        politicalPartie.voteCount = 0;
        politicalPartie.indexVotingList = 0;
    }

    function addPoliticalParties(
        uint256[] memory _keys,
        string[] memory _names,
        string[][] memory _candidates
    ) public payable onlyOwner {
        //Delete political Parties
        for (uint256 i = 0; i < keyPoliticalPartieList.length; i++) {
            delete keyPoliticalPartieList[i];
        }
        cantPoliticalParties = 0;

        uint256 posPoliticalPartie;
        for (
            posPoliticalPartie = 0;
            posPoliticalPartie < _keys.length;
            posPoliticalPartie++
        ) {
            addPoliticalPartie(
                _keys[posPoliticalPartie],
                _names[posPoliticalPartie]
            );

            //Delete candidates
            uint256 cantCandidates = politicalParties[posPoliticalPartie].keyCandidatesList.length; 
            for (uint256 i = 0;i < cantCandidates; i++) {
                delete politicalParties[posPoliticalPartie]
                    .keyCandidatesList[i];
            }
            politicalParties[posPoliticalPartie].cantCandidates = 0;

            cantCandidates = _candidates[posPoliticalPartie].length;
            for (
                uint256 posCandidate = 0;
                posCandidate < cantCandidates;
                posCandidate++
            ) {
                addCandidate(
                    posCandidate,
                    _candidates[posPoliticalPartie][posCandidate],
                    _keys[posPoliticalPartie]
                );
            }
        }
    }

    function getPoliticalParties()
        public
        view
        returns (string[] memory _names, string[][] memory _candidates)
    {
        uint256 posPoliticalPartie;
        uint256 posCandidate;

        _names = new string[](cantPoliticalParties);
        _candidates = new string[][](cantPoliticalParties);

        for (
            posPoliticalPartie = 0;
            posPoliticalPartie < cantPoliticalParties;
            posPoliticalPartie++
        ) {
            _names[posPoliticalPartie] = politicalParties[keyPoliticalPartieList[posPoliticalPartie]]
                .name;


                uint256 cantCandidates
             = politicalParties[keyPoliticalPartieList[posPoliticalPartie]]
                .cantCandidates;
            _candidates[posPoliticalPartie] = new string[](cantCandidates);


                PoliticalPartie storage aux
             = politicalParties[keyPoliticalPartieList[posPoliticalPartie]];
            for (
                posCandidate = 0;
                posCandidate < cantCandidates;
                posCandidate++
            ) {
                _candidates[posPoliticalPartie][posCandidate] = aux
                    .candidates[aux.keyCandidatesList[posCandidate]]
                    .name;
            }
        }
        return (_names, _candidates);
    }

    function addCandidateToVotingList(
        uint256 _index,
        uint256 _keyCandidate,
        uint256 _keyPoliticalPartie
    ) public {

            PoliticalPartie storage _politicalPartie
         = politicalParties[_keyPoliticalPartie];
        _politicalPartie.candidates[_keyCandidate].indexVotingList = _index;
    }

    function addPoliticalPartieToVotingList(
        uint256 _index,
        uint256 _keyPoliticalPartie
    ) public {

            PoliticalPartie storage _politicalPartie
         = politicalParties[_keyPoliticalPartie];
        _politicalPartie.indexVotingList = _index;
    }

    function addVotingList(
        uint256 _key,
        string memory _title,
        int256[] memory _keyPoliticalPartie,
        string memory _description,
        string memory _votoType,
        int256[][] memory _candidates
    ) public {
        if (keyVotingList.length > cantVotingLists) {
            keyVotingList[cantVotingLists] = _key;
        } else {
            keyVotingList.push(_key);
        }

        cantVotingLists = cantVotingLists + 1;

        VotingList storage newVotingList = votingLists[_key];
        newVotingList.index = cantVotingLists;
        newVotingList.title = _title;
        newVotingList.description = _description;
        newVotingList.votoType = _votoType;

        //Add votingList in candidates for each PoliticalPartie
        uint256 posPoliticalPartie;
        for (
            posPoliticalPartie = 0;
            posPoliticalPartie < cantPoliticalParties;
            posPoliticalPartie++
        ) {
            //For each PoliticalPartie
            if (_keyPoliticalPartie[posPoliticalPartie] > -1) {
                //If the PoliticalPartie have candidates in this voting list
                addPoliticalPartieToVotingList(
                    newVotingList.index,
                    uint256(_keyPoliticalPartie[posPoliticalPartie])
                );


                    uint256 cantCandidates
                 = politicalParties[keyPoliticalPartieList[posPoliticalPartie]]
                    .cantCandidates;
                for (
                    uint256 posCandidate = 0;
                    posCandidate < cantCandidates;
                    posCandidate++
                ) {
                    if (_candidates[posPoliticalPartie][posCandidate] > -1) {
                        addCandidateToVotingList(
                            newVotingList.index,
                            uint256(
                                _candidates[posPoliticalPartie][posCandidate]
                            ),
                            keyPoliticalPartieList[posPoliticalPartie]
                        );
                    }
                }
            }
        }
    }

    function addVotingLists(
        uint256[] memory _key,
        string[] memory _title,
        int256[][] memory _keyPoliticalPartie,
        string[] memory _description,
        string[] memory _votoType,
        int256[][][] memory _candidates
    ) public payable onlyOwner {
        //Delete voting lists
        for (uint256 i = 0; i < keyVotingList.length; i++) {
            delete keyVotingList[i];
        }
        cantVotingLists = 0;

        for (uint256 pos = 0; pos < _key.length; pos++) {
            addVotingList(
                _key[pos],
                _title[pos],
                _keyPoliticalPartie[pos],
                _description[pos],
                _votoType[pos],
                _candidates[pos]
            );
        }
    }

    function getVotingList(uint256 _key)
        public
        view
        returns (
            string memory,
            string memory,
            string memory
        )
    {
        return (
            votingLists[_key].title,
            votingLists[_key].description,
            votingLists[_key].votoType
        );
    }

    function getVotingLists()
        public
        view
        returns (
            string[] memory,
            string[] memory,
            string[] memory
        )
    {
        uint256 pos;
        string[] memory titles = new string[](cantVotingLists);
        string[] memory descriptions = new string[](cantVotingLists);
        string[] memory votoTypes = new string[](cantVotingLists);
        for (pos = 0; pos < cantVotingLists; pos++) {
            titles[pos] = votingLists[keyVotingList[pos]].title;
            descriptions[pos] = votingLists[keyVotingList[pos]].description;
            votoTypes[pos] = votingLists[keyVotingList[pos]].votoType;
        }
        return (titles, descriptions, votoTypes);
    }

    function setFlagSentEmailElectors(bool flag) public onlyOwner {
        flagSentEmailElectors = flag;
    }

    function setFlagSentEmailTallyAuthorities(bool flag) public onlyOwner {
        flagSentEmailTallyAuthorities = flag;
    }
    function getCandidatesToVotingList()
        public
        view
        returns (uint256[][] memory)
    {
        uint256 posPoliticalPartie;
        uint256 longitud = keyPoliticalPartieList.length;
        uint256[][] memory votingIndex = new uint256[][](longitud);
        for (
            posPoliticalPartie = 0;
            posPoliticalPartie < longitud;
            posPoliticalPartie++
        ) {

                PoliticalPartie storage _politicalPartie
             = politicalParties[keyPoliticalPartieList[posPoliticalPartie]];
            uint256 posCandidate;
            uint256 longCandidate = _politicalPartie.keyCandidatesList.length;
            uint256[] memory aux = new uint256[](longCandidate);
            for (
                posCandidate = 0;
                posCandidate < longCandidate;
                posCandidate++
            ) {
                aux[posCandidate] = (
                    _politicalPartie.candidates[posCandidate].indexVotingList
                );
            }
            votingIndex[posPoliticalPartie] = aux;
        }
        return votingIndex;
    }
}
