import React, { Component } from 'react';
import Navbar from './Navbar';
import Footer from '../Footer';
import ElectoralProcess from '../createProcess/ElectoralProcess';
import Menu from './Menu';
import Cookies from 'universal-cookie';
import Election from '../../../contracts/Election.json';
import getWeb3 from '../../../getWeb3';

const cookies = new Cookies();

export default class HomeAdmin extends Component {
    constructor(props) {
        super(); 
        this.state= {
            workingSpace : ElectoralProcess,
            user : {},
            storageValue: 0, 
            web3: null, 
            accounts: null, 
            contract: null,
            loading: true,
            flagStart: false,
            flagEnd: false
        }   
    }

    logOut= async () => {
        await cookies.remove('user',{path: "/admin"});
        cookies.remove("contract",{path: "/admin"});
        cookies.remove("web3",{path: "/admin"});
        const aux = cookies.get('user',{path: "/admin"});

        window.location.href= './login-admin';
    }

    handleNextComponent = (_nextChildComponent) => {
        this.setState({
            workingSpace: _nextChildComponent
        });
    }

    showTitleEnd = () => {
        this.setState({flagEnd:true});
    }

    showTitleStart = () => {
        this.setState({flagStart :true});
    }
    componentDidMount = async () => {
        const user = cookies.get('user',{path:'/admin'});
        console.log("user", user);
        if (user) {
            this.setState({user: user});
        }else { //If I'm not logged
            window.location.href="./login-admin";
        }

        try {
            // Get network provider and web3 instance.
            const web3 = await getWeb3();
    
            // Use web3 to get the user's accounts.
            const accounts = await web3.eth.getAccounts();
    
            // Get the contract instance.
            const networkId = await web3.eth.net.getId();
            const deployedNetwork = Election.networks[networkId];
            const instance = new web3.eth.Contract(
              Election.abi,
              deployedNetwork && deployedNetwork.address,
            );
    
            // Set web3, accounts, and contract to the state, and then proceed with an
            // example of interacting with the contract's methods.
            this.setState({ web3, accounts, contract: instance });
        } catch (error) {
            // Catch any errors for any of the above operations.
            alert(
              `Failed to load web3, accounts, or contract. Check //console for details.`,
            );
        }
        this.setState({loading: false});
        
    }

    render() {
        const user = this.state.user;
        //console.log("user",user);
        //console.log(this.state, this.props)
        if (this.state.loading) {
            return <div>Loading Web3, accounts, and contract...</div>;
        }
        return (
            <div>
                <Navbar 
                    handleLogOut = {this.logOut}
                    web3 = {this.state.web3}
                    contract = {this.state.contract}
                    accounts = {this.state.accounts}
                    flagStart = {this.state.flagStart}
                    flagEnd = {this.state.flagEnd}
                />
                

                <Menu
                    handleNextComponent = {this.handleNextComponent}
                    dataUser={user}
                />
                <div className="content-wrapper">
                    <this.state.workingSpace 
                    handleNextComponent = {this.handleNextComponent}
                    web3 = {this.state.web3}
                    contract = {this.state.contract}
                    accounts = {this.state.accounts}
                    showTitleEnd = {this.showTitleEnd}
                    showTitleStart = {this.showTitleStart}
                    user={user}
                    />
                </div>
                <Footer />
            </div>
        )
    }
}
