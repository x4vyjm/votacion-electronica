import React, { Component } from "react";
import Electores from "../electores/Electores";
import ElectoralProcess from "../createProcess/ElectoralProcess";
import ActaSufragio from "../auditorias/ActaSufragio";
import ActaEscrutinio from "../auditorias/ActaEscrutinio";
import Report from "../auditorias/Report";
import Settings  from "../encryption/Settings";

export default class Menu extends Component {
  constructor(props) {
    super();
  }
  handleClickElectoralProcess = () => {
    this.props.handleNextComponent(ElectoralProcess);
  };
  handleClickElectores = () => {
    this.props.handleNextComponent(Electores);
  };
  handleClickAuditorialSufragio = () => {
    this.props.handleNextComponent(ActaSufragio);
  };
  handleClickAuditorialEscrutinio = () => {
    this.props.handleNextComponent(ActaEscrutinio);
  };
  handleClickReports = () => {
    this.props.handleNextComponent(Report);
  };
  handleClickEncryption = () => {
    this.props.handleNextComponent(Settings);
  };

  render() {
    const dataUser = this.props.dataUser;
    return (
      <aside
        className="main-sidebar sidebar-dark-primary elevation-4"
        id="mySidebar"
      >
        {/* Brand Logo */}
        <a className="brand-link" style={{ color: "rgba(255,255,255,.8)" }}>
          <img
            src="images/icono.png"
            className="brand-logo"
            style={{ marginLeft: "6px", marginRight: "4px", maxHeight: "33px" }}
          />
          <span>
            <img
              src="images/logo_blanco.png"
              style={{ marginRight: "6px", maxWidth: "170px" }}
            />
          </span>
        </a>
        {/* Sidebar */}
        <div className="sidebar">
          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img
                src="images/user-160x160.jpg"
                className="img-circle elevation-2"
                alt="User Image"
              />
            </div>
            <div className="_info">
              <a className="d-block">
                {dataUser.name + " " + dataUser.lastName}
              </a>
            </div>
          </div>
          {/* Sidebar Menu */}
          <nav className="mt-2">
            <ul
              className="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              <li className="nav-item ">
                <a className="nav-link " onClick={this.handleClickEncryption}>
                  <i className="nav-icon fas fa-cog" />
                  <p>Configuración</p>
                </a>
              </li>
              <li className="nav-item ">
                <a
                  className="nav-link "
                  onClick={this.handleClickElectoralProcess}
                >
                  <i className="nav-icon fas fa-vote-yea" />
                  <p>Proceso electoral</p>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" onClick={this.handleClickElectores}>
                  <i className="nav-icon fas fa-database" />
                  <p>Electores</p>
                </a>
              </li>

              <li className="nav-item has-treeview menu-close">
                <a className="nav-link" onClick={this.handleClickReports}>
                  <i className="nav-icon fas fa-shield-alt" />
                  <p>Reportes</p>
                </a>
              </li>
            </ul>
          </nav>
          {/* /.sidebar-menu */}
        </div>
        {/* /.sidebar */}
      </aside>
    );
  }
}
