import React, { Component } from 'react';

export default class Navbar extends Component {
    constructor(props) {
        super();
        this.state = {};
    }
    render () {
        console.log(this.props)
        return (
        <nav className="main-header navbar navbar-expand navbar-white navbar-light">
            <ul className="navbar-nav">
                <li className="nav-item">
                <a className="nav-link" data-widget="pushmenu" role="button"><i className="fas fa-bars" /></a>
                </li>
            </ul>

            <div
                  className="row"
                  style={{
                    display: this.props.flagStart ? "block" : "none",
                    padding: "5px",
                    marginLeft: "5px",
                    textAlign: "center",
                    color: "#117A8A",
                  }}
                >
                 <h4> <b>¡El proceso electoral ya ha iniciado!</b></h4>
            </div>
            <div
                  className="row"
                  style={{
                    display: this.props.flagEnd ? "block" : "none",
                    padding: "5px",
                    marginLeft: "5px",
                    textAlign: "center",
                    color: "#117A8A",
                  }}
                >
                 <h4> <b>¡El proceso electoral ha finalizado!</b></h4>
            </div>

            <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                    
                </ul>
                <ul className="form-inline my-2 my-lg-0">
                <div className="btn-group dropleft" >
                    <a style={{cursor:'pointer'}} className="nav-link " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i className="far fa-user" />
                    </a>

                    <div className="dropdown-menu dropdown-menu-lg dropdown-menu">
                        
                        <button className="dropdown-item dropdown-footer" type="button" onClick={this.props.handleLogOut}>
                            Cerrar sesión
                        </button>
                        
                    </div>
                </div>
                </ul>
                </div>

        </nav>
        )
    }
}
