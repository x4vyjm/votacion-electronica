import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
        <footer className="main-footer">
            <div className="float-right d-none d-sm-block">
                <b>Versión</b> 1.0
            </div>
            <strong>Copyright © 2020 <a href="#">VoteTESIS</a>.</strong> Todos los derechos reservados
        </footer>

        )
    }
}
