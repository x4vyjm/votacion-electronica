import React, {useState, useEffect } from 'react'

const deleteCandidate = "deleteCandidate";
const addCandidate = "addCandidate";

export default function CandidatesList (props) {
  const {handleDeleteCandidate, handleAddCandidate, candidates, index,politicalPartiesList} = props;
  const [valueCandidate, setValueCandidate] = useState("");

  useEffect(() =>{
    const var1 = "myInputvalueCandidate"+index;
    const var2 = "myButton"+index;
    let myInput = document.getElementById(var1);
    myInput.addEventListener("keyup", function (e) {
      if (e.keyCode  === 13) {
      e.preventDefault();
      document.getElementById(var2).click();
      }
    });
  });
  
  return (
      <div>
        {/** FORM */}
        <div className="input-group" >
          <input
            maxlength="50"
            type="text"
            name="candidate"
            id={"myInputvalueCandidate"+index}
            //ref="itemName"
            value ={valueCandidate}
            className="form-control"
            placeholder="Agregar nuevo candidato"
            onChange= {(e) => {
              setValueCandidate(e.target.value)
            }}
          />
          <div className="input-group-append">
            <button 
              className="btn btn-primary"  
              onClick = {(e) =>  {
                // console.log(valueCandidate);
                handleAddCandidate(valueCandidate, index)
                setValueCandidate("");
              }}
              id={"myButton"+index}
            ><i class="fa fa-plus"></i> </button> 
          </div>
        
          </div>
        {/** Candidates List */}
        <TodoList 
          candidates = {candidates} 
          handleDeleteCandidate = {handleDeleteCandidate} 
          index = {index}
          politicalPartiesList = {politicalPartiesList}
      /> 
      
      </div>
  );
}
    
function TodoList (props) {
   let candidates = props.candidates.map((item, index) => {
      return (
        <Candidate 
            key={index + " " + props.index} 
            item={item} 
            index={index} 
            handleDeleteCandidate={props.handleDeleteCandidate} 
            politicalPartiesList = {props.politicalPartiesList}
            position = {props.index}
        />
      );
  });

  return (
    <ul className="list-group"> {candidates} </ul>
  );
}
    
function Candidate (props) {
  let todoClass = props.item.done ? "done" : "undone";
  return(
    <li className="list-group-item ">
      <div className={todoClass}>
        {props.item.value}
        <button   
          type="button" 
          className="close" 
          onClick={(e) => {
            let index = parseInt(props.index);
            props.handleDeleteCandidate(props.position, index);
          }}
        >&times;</button>
      </div>
    </li>     
    );
}