import React, { Component } from 'react';
import CandidatesList from './CandidatesList';
import DragDropImage from  './DragAndDropImage';
import {connect} from 'react-redux';
import {handleChangePoliticalPartiesList, handleAddPoliticalPartie, handleDeletePoliticalPartie, handleLogo, handleAddCandidate, handleDeleteCandidate} from '../../../redux/actions/politicalPartiesListActions';
import {handleUpdateVotingList } from  '../../../redux/actions/votingListActions';
import '../admin_style.css';

const updateName = "updateName";
const deletePoliticalPartie = "deletePoliticalPartie";
const addPoliticalPartie = "addPoliticalPartie";

class Step2 extends Component {
  constructor(props) {
    super();
    this.state = {
      itemSelected: -1,
    };
    this.handleItemSelected = this.handleItemSelected.bind(this);
  }

  handleItemSelected(index){
    this.setState({itemSelected:index})
  }

  render (){
    const {handleAddPoliticalPartie, handleDeletePoliticalPartie,handleUpdateVotingList, handleLogo, handleAddCandidate, handleDeleteCandidate, handleChangePoliticalPartiesList}  =this.props;
    const {itemSelected} = this.state;
    const politicalPartiesList = this.props.politicalPartiesList;
    console.log("politicalPartiesList",politicalPartiesList, "votingList",this.props.votingList);
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card card-primary card-outline">
              <div className="card-header">
                <h3 className="card-title">Lista de partidos políticos</h3>
              </div>
              <div className="card-body">
                <button
                  style={{ marginBottom: "20px"}}
                  className="btn btn-info"
                  type= "button"
                  onClick={(e) => {
                    const item =  {
                      name: "",
                      partieLogo: [],
                      candidates: [],
                    };

                    handleAddPoliticalPartie(item);
                    handleUpdateVotingList (addPoliticalPartie, item );
                  }}
                >
                Agregar nueva partido político
                </button>

                <div class="modal fade"
                  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h6 className="modal-title">Dese eliminar el partido político?</h6>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div className="modal-body">
                          <p>Si eliminar el presente partido político toda la información relacionada a este partido politico será eliminada</p>
                        </div>
                        <div className="modal-footer justify-content-between">
                          <button type="button" className="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="button" className="btn btn-danger"
                            onClick={(e) => {
                              handleDeletePoliticalPartie(itemSelected);
                              handleUpdateVotingList (deletePoliticalPartie,{itemSelected:itemSelected});
                            }}
                          >Eliminar</button>
                        </div>
                    </div>
                  </div>
                </div>

                <div id="modal-deleteItem" className="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-confirm">
                  <div className="modal-content">
                    <div className="modal-header flex-column">
                      <div className="icon-box">
                        <i class="fas fa-times"></i>
                      </div>
                      <h4 className="modal-title w-100">Está seguro?</h4>
                      <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div className="modal-body">
                      <p>Si continuas toda la información relacionada a este partido politico será eliminada.</p>
                    </div>
                    <div className="modal-footer justify-content-center">
                      <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                      <button
                        type="button"
                        className="btn btn-danger"
                        data-dismiss="modal"
                        onClick={(e) => {
                          handleDeletePoliticalPartie(itemSelected);
                          handleUpdateVotingList (deletePoliticalPartie,{itemSelected:itemSelected});
                        }}
                      >Eliminar</button>
                    </div>
                  </div>
                </div>
              </div>
               {politicalPartiesList.map((item, index) => (
                <div>
              <div key={"key" + index  } className="card card-primary" >
                  <div className="card-header" >
                      <h3 className="card-title">
                          {item.name}
                      </h3>
                      <div className="card-tools">
                      <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus" /></button>
                      <button type="button" className="btn btn-tool" data-toggle="modal"
                        data-target="#modal-deleteItem"
                        onClick={(e) => this.handleItemSelected(index)}
                      ><i className="fas fa-times" /></button>
                      </div>

                  </div>
                  <div className="card-body">
                      <div className="row">
                          <div className="col-sm-12">
                          <div className="form-group row">
                              <label className="col-sm-2 col-form-label">Nombre</label>
                              <div className="col-sm-10">
                                  <input
                                   maxlength="50"
                                  type="text"
                                  className="form-control"
                                  placeholder="Ingresar el nombre del partido político"
                                  id={"input-partido-politico-"+ index}
                                  key={"input-partido-politico-"+ index}
                                  name="name"
                                  onChange={(e) =>{
                                    handleChangePoliticalPartiesList(index, e.target.value,e.target.name);
                                    handleUpdateVotingList (updateName, {index: index, value: e.target.value});
                                  }}
                                  value={item.name}
                                  />
                              </div>
                          </div>

                          <div className="form-group row">
                                <label className="col-sm-2 col-form-label" for="partieLogo" >
                                Logo
                                </label>
                                <div className="col-sm-10">
                                  <div id= {"partieLogo_"+ index}>
                                    <DragDropImage
                                        id= {"partieLogoSRC"+ index} 
                                        key= {"partieLogoSRC"+ index}  
                                        textButton="Agregar logo"
                                        alert="Arrastrar solo archivos de tipo imagen"
                                        handleLogo = {handleLogo}
                                        index = {index}
                                        files = {item.partieLogo}
                                    />
                                  </div>
                                   {/* <UploadImages 
                                      key = {"partieLogo_"+ index}
                                      
                                    />  */}
                                </div>
                            </div>

                          <div className="form-group row">
                              <label className="col-sm-2 col-form-label">Candidatos</label>
                              <div className="col-sm-10">
                                <CandidatesList
                                  candidates= {item.candidates}
                                  index={index}
                                  handleDeleteCandidate = {handleDeleteCandidate}
                                  handleAddCandidate = {handleAddCandidate}
                                  handleUpdateVotingList = {handleUpdateVotingList}
                                  politicalPartiesList = {politicalPartiesList}
                                />
                              </div>
                          </div>

                          </div>
                      </div>
                  </div>
              </div>
                <br/>
            </div>
                )) }
              <button
                  style={{ marginBottom: "20px", display:politicalPartiesList.length === 0 ? 'none':'block' }}
                  className="btn btn-info"
                  type= "button"
                  onClick={(e) => {
                    const item =  {
                      name: "",
                      partieLogo: [],
                      candidates: [],
                    };

                    handleAddPoliticalPartie(item);
                    handleUpdateVotingList (addPoliticalPartie, item );
                  }}
                >
                Agregar nueva partido político
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    politicalPartiesList: state.politicalPartiesList,
    votingList: state.votingList,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleChangePoliticalPartiesList: (index, value, name) => dispatch(handleChangePoliticalPartiesList(index, value, name)),
    handleAddPoliticalPartie: (item) => dispatch(handleAddPoliticalPartie(item)),
    handleDeletePoliticalPartie: (index) => dispatch(handleDeletePoliticalPartie(index)),
    handleLogo: (item,index) => dispatch(handleLogo(item,index)),
    handleAddCandidate: (item,index) => dispatch(handleAddCandidate(item,index)),
    handleDeleteCandidate: (item,index) => dispatch(handleDeleteCandidate(item,index)),
    handleUpdateVotingList: (name, item) => dispatch(handleUpdateVotingList(name, item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Step2);