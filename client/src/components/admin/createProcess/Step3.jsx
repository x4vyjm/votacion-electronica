import React, { useState, Component } from 'react';
import Select from 'react-select';
import {connect} from 'react-redux';
import {handleAddVotingList,handleDeleteVotingList,handleChangeCandidates, handleChangeVotingList, handleChangeSelection, handleChangeCheckPoliticalPartie} from '../../../redux/actions/votingListActions';
import '../admin_style.css';

class Step3 extends Component {
  constructor(props) {
    super();
  }

  render() {
    const {politicalPartiesList, votingList } = this.props;
    console.log(this.props);
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card card-primary card-outline">
              <div className="card-header">
                <h3 className="card-title">Crear proceso electoral</h3>
              </div>
              <div className="card-body">
                <div className="form-horizontal">
                  <VotingList 
                    handleAddVotingList = {this.props.handleAddVotingList} 
                    handleDeleteVotingList = {this.props.handleDeleteVotingList} 
                    handleChangeVotingList = {this.props.handleChangeVotingList}
                    handleChangeSelection = {this.props.handleChangeSelection}
                    politicalPartiesList = {politicalPartiesList} 
                    handleChangeCheckPoliticalPartie = {this.props.handleChangeCheckPoliticalPartie}
                    votingList = {votingList} 
                    handleChangeCandidates = {this.props.handleChangeCandidates}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


function VotingList (props) {
  const {handleAddVotingList, handleDeleteVotingList, handleChangeVotingList, handleChangeSelection, handleChangeCheckPoliticalPartie, handleChangeCandidates } = props;
  const {politicalPartiesList, votingList } = props;
  const [valueVotingList, setValueVotingList] = useState(-1);
  const options = [
    {value: "unico", label:"Único candidato"},
    {value: "multiple", label:"Múltiples candidatos"},
  ];

  console.log(props);

  return (
    <div>
      <div id="modal-deleteItem" className="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-confirm">
          <div className="modal-content">
            <div className="modal-header flex-column">
              <div className="icon-box">
                <i class="fas fa-times"></i>
              </div>						
              <h4 className="modal-title w-100">Está seguro?</h4>	
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div className="modal-body">
              <p>Si continuas toda la información relacionada a este lista será eliminada.</p>
            </div>
            <div className="modal-footer justify-content-center">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancelar</button>
              <button 
                type="button" 
                className="btn btn-danger"  
                data-dismiss="modal" 
                onClick={(e) => handleDeleteVotingList(valueVotingList)}
              >Eliminar</button>
            </div>
          </div>
        </div>
      </div>  
      <button 
          style={{ marginBottom: "20px"}}
          className="btn btn-info"
          type= "button"
          onClick={(e) =>{
            (votingList.length === 4) ? alert("No se pueden crear más de 4 listas de votación") :
           handleAddVotingList(politicalPartiesList)}}
      >
      Agregar nueva lista de votación
      </button>

      {votingList.map((item, index) => {
        return (
          <div>
            <div key={"key" + index  } className="card card-primary" >
                <div className="card-header" >
                    <h3 className="card-title">
                    {item.title}
                    </h3>
                    <div className="card-tools">
                    <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus" /></button>
                    <button 
                      type="button" 
                      className="btn btn-tool" 
                      data-toggle="modal" 
                      data-target="#modal-deleteItem"  
                      onClick={(e) => setValueVotingList(index)}
                    >
                      <i className="fas fa-times" />
                    </button>
                    </div>

                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-sm-12">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label">Título</label>
                            <div className="col-sm-9">
                                <input
                                type="text"
                                maxlength="50"
                                className="form-control"
                                placeholder="Titulo"
                                id="new-todo"
                                name="title"
                                onChange={(e) => handleChangeVotingList(index, e.target.value, e.target.name)}
                                value={item.title}
                                />
                            </div>
                        </div>

                        <div className="form-group row">
                          <label className="col-sm-3 col-form-label">
                            Descripción
                          </label>
                          <div className="col-sm-9">
                            <textarea
                              class="form-control"
                              maxlength="300"
                              rows="3"
                              name="description"
                              value={item.description}
                              onChange={(e) => handleChangeVotingList(index, e.target.value, e.target.name)}
                              placeholder="Escribir una breve descripción para la presente cédula ..."
                            ></textarea>
                          </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label">Tipo</label>
                            <div className="col-sm-9">
                              <Select 
                                options={options}
                                name="type"
                                onChange={(e) => handleChangeSelection (index, e, "type")}
                                className="basic-multi-select"
                                classNamePrefix="select" 
                                placeholder= "Seleccionar..."
                                value={item.type}
                              />
                            </div>
                        </div>
                        <div style={{display: item.display ? 'block':'none'}}>
                          <div className="form-group row" style={{marginBottom:'0px'}}>
                              <label className="col-sm-12 col-form-label">Seleccione los partidos políticos</label>
                              <p className="text-sm mb-0" style={{paddingLeft:'24px',paddingBottom:'15px'}}> Si el tipo de la lista de votación es de "Único candidato", solo podra escoger un candidato representate</p>
                          </div>
                          <div className="form-group">
                            {item.politicalParties.length === 0 ? 
                            (
                              <div>
                                <br/>
                                <i>No existe ningun partido político creado, regrese al paso anterior.</i>
                              </div>
                            ) : 
                            (
                              <div id="accordion">
                                {item.politicalParties.map((partie, indexPartieP) => {
                                  //Hide candidates that were selected by other votation list
                                  // let  candAvailablesPoliticalParties = partie.candidates;
                                  // votingList.map((filter_vl, filter_index1) => {
                                  //   filter_vl.politicalParties[indexPartieP].selectedCandidates.map((filter_cand,filter_index)=> {
                                  //     candAvailablesPoliticalParties.map((candAvailable, index_candAvailable)=> {
                                  //       if (filter_cand.value.localeCompare(candAvailable.value)=== 0){
                                  //         candAvailablesPoliticalParties.splice(index_candAvailable,1);
                                  //       }
                                  //     })  
                                  //   })
                                  // } );
                                  // console.log("candAvailablesPoliticalParties: ", candAvailablesPoliticalParties);

                                  return (
                                  <div className="card" style={{marginBottom:'0px'}}>
                                      <div className="card-header" id={"political-parties"+ indexPartieP + "-"+ index} style={{paddingBottom:'4px',paddingTop:'4px'}}>
                                        <div className="form-check">
                                          <input 
                                            type="checkbox" 
                                            className="form-check-input"
                                            id={"politicalPartieCheck-"+indexPartieP+ "-"+ index} 
                                            checked={partie.isChecked}
                                            onChange={(e) => handleChangeCheckPoliticalPartie(index,indexPartieP)}
                                            style={{marginTop:'22px'}} 
                                          /> 
                                            <button className="btn btn-link collapsed"  data-toggle="collapse" data-target={"#collapse"+indexPartieP+ "-"+ index} aria-expanded="true" aria-controls={"collapse"+index}>
                                              <h6 className="mb-0">{partie.name}</h6>
                                            </button>
                                        </div>
                                      </div>
                                      <div id={"collapse"+indexPartieP+ "-"+ index} className="collapse" aria-labelledby={"political-parties"+ indexPartieP+ "-"+ index} data-parent="#accordion">
                                        <div className="card-body">
                                        <div className="form-group row">
                                          <label className="col-sm-3 col-form-label">Candidatos</label>
                                          <div className="col-sm-9">
                                            <Select 
                                              options={partie.candidates/*candAvailablesPoliticalParties*/}
                                              isMulti
                                              isClearable
                                              isDisabled={!partie.isChecked}
                                              name="selectedCandidates"
                                              onChange={(e) => {
                                                console.log(e);
                                                handleChangeCandidates("selectedCandidates",index,indexPartieP, e)
                                              }}
                                              onInputChange={(e)=> {
                                                console.log(e)
                                              }}
                                              className="basic-multi-select"
                                              classNamePrefix="select" 
                                              placeholder= "Escoger candidatos"
                                              value={partie.selectedCandidates}
                                            />
                                          </div>
                                      </div>  
                                        </div>
                                      </div>
                                    </div>)
                                  })}
                              </div>
                            )}
                          </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
          </div>
        )}) }  
      <button 
          style={{ marginBottom: "20px" , display: votingList.length===0 ? 'none':'block'}}
          className="btn btn-info"
          type= "button"
          onClick={(e) =>{
            (votingList.length === 4) ? alert("No se pueden crear más de 4 listas de votación") :
           handleAddVotingList(politicalPartiesList)}}
      >
      Agregar nueva lista de votación
      </button>
    </div>
  )
}
  

const mapStateToProps = (state) => {
  return {
      politicalPartiesList: state.politicalPartiesList,
      votingList : state.votingList
  }    
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleAddVotingList: (politicalPartiesList) => dispatch(handleAddVotingList(politicalPartiesList)),
    handleDeleteVotingList: (index) => dispatch(handleDeleteVotingList(index)),
    handleChangeVotingList:(index, value, name)  => dispatch(handleChangeVotingList(index, value, name)),
    handleChangeSelection:(index, value, name)  => dispatch(handleChangeSelection(index, value, name)),    
    handleChangeCheckPoliticalPartie:(index, position)  => dispatch(handleChangeCheckPoliticalPartie(index, position)),  
    handleChangeCandidates:(name, index, indexPoliticalPartie, value)  => dispatch(handleChangeCandidates(name, index, indexPoliticalPartie, value)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Step3);