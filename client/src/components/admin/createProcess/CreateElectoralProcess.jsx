import React, { Component } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Typography from "@material-ui/core/Typography";
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import ElectoralProcess from './ElectoralProcess';
import {connect} from 'react-redux';
import {handleUpdateVotingList} from '../../../redux/actions/votingListActions';
import {handleChange} from '../../../redux/actions/electoralProcessActions';


const changeCandidates = "changeCandidates";


function isInList (element, list) {
  for (let i = 0;i <list.length;i++){
    if (list[i].value.localeCompare(element.value) === 0) {
      return true;
    }
  }
  return false;
}

class CreateElectoralProcess extends Component {
  constructor(props) {
      super();
      this.state = {
        web3: null, 
        accounts: null, 
        contract: null,
        msgAlert:false,
      }
  }

  handleElectoralProcess = () => {
      console.log("redirecciona a página de proceso electoral");
      this.props.handleNextComponent(ElectoralProcess);
  }


  render() {
    const {handleUpdateVotingList, politicalPartiesList, votingList, electoralProcessInfo,handleChange} = this.props;
    console.log(this.props, this.state);
      return (
        <div>
            <section className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                    <div className="col-sm-6">
                        <h1>Nuevo proceso electoral</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                        <li className="breadcrumb-item">
                            <a style={{cursor: 'pointer',color:'#007bff', textDecoration:"none", backgroundColor:'transparent'}} onClick={this.handleElectoralProcess}>Proceso electoral</a>
                        </li>
                        <li className="breadcrumb-item active">Crear Proceso electoral</li>
                        </ol>
                    </div>
                    </div>
                </div>
            </section>
            <section
            className="content"
            style={{ maxWidth: "1100px", margin: "auto" }}
            >
            <div className="container-fluid">
                <div className="row" >
                <div className="col-md-12">
                    <CreateProcess
                        politicalPartiesList = {politicalPartiesList}
                        handleUpdateVotingList = {handleUpdateVotingList}
                        electoralProcessInfo = {electoralProcessInfo}
                        votingList = {votingList}
                        handleChange = {handleChange}
                        handleNextComponent = {this.props.handleNextComponent}
                        web3 = {this.props.web3}
                        contract = {this.props.contract}
                        accounts = {this.props.accounts}
                    />
                </div>
                </div>
            </div>
            </section>
        </div>
    )
    
    
  }
}



const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ["Datos generales", "Agregar partidos políticos", "Crear cédulas"];
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return <Step1 />;
    case 1:
      return <Step2 />;
    case 2:
      return <Step3 />;
    default:
      return "Unknown step";
  }
}

function handleReturn (props) {
  console.log("props",props);

  //LLamar a todos los Servicio para actualizar el blockchain con todos los datos guardados en el state
  const contract = props.contract;
  const accounts = props.accounts;
  //1° service
  contract.methods.setGeneralInformation(props.electoralProcessInfo.title, props.electoralProcessInfo.description , 
    props.electoralProcessInfo.dateIni, props.electoralProcessInfo.dateEnd).send({ from: accounts[0] }).then(() => {
    //props.electoralProcessInfo.dateIni, props.electoralProcessInfo.dateEnd).call().then(() => {
      console.log("props",props);
    }).then(() => {
      
      let ids = [];
      let items = [];
      let candidates = []
      props.politicalPartiesList.map((item, index) => {
        ids.push(index);
        items.push(item.name);
        let aux =  [];
        item.candidates.map((candidate) => {
          aux.push(candidate.value);
        })
        candidates.push(aux);
      });
      console.log(ids,items, candidates);
      
     //2° service
      contract.methods.addPoliticalParties(ids, items, candidates).send({ from: accounts[0] }).then(() => {
        let titles = [];
        let descriptions = [];
        let votoTypes = [];
        let keys = [];
        let keyPoliticalParties = [];
        let candidates = [];
        props.votingList.map((item, index) => {
          titles.push(item.title);
          descriptions.push(item.description);
          votoTypes.push(item.type.value);
          keys.push(index);
          let auxKeyPP = [];
          let auxCand1 = [];
          item.politicalParties.map((politicalPartie, position) => {
            if (politicalPartie.isChecked) {
              auxKeyPP.push(position);
            }else {
              auxKeyPP.push(-1);
            }

            let auxCand2 = [];
            politicalPartie.candidates.map((candidate,pos) => {
              //console.log("candidate", candidate);
              if (isInList(candidate,politicalPartie.selectedCandidates)) {
                //console.log("entro");
                auxCand2.push(pos);
              }else {
                auxCand2.push(-1);
              }
            }) 
            auxCand1.push(auxCand2);
          })
          candidates.push(auxCand1);
          keyPoliticalParties.push(auxKeyPP);
        }) 

        console.log(keys, titles, keyPoliticalParties, descriptions, votoTypes, candidates);
         //3° service
        contract.methods.addVotingLists(keys, titles, keyPoliticalParties, descriptions, votoTypes, candidates).send({ from: accounts[0] }).then(() => {
          props.handleNextComponent(ElectoralProcess);
        })
      });
      
    })
}

function CreateProcess(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();
  const {handleUpdateVotingList, handleChange, politicalPartiesList, votingList, electoralProcessInfo} = props;


  const handleNext = () => {
    //Validate all data is completed
    let isComplete = true;
    console.log("props",politicalPartiesList, votingList, electoralProcessInfo);

    if (activeStep === 0) {
      if (electoralProcessInfo.title.length === 0 || electoralProcessInfo.description.length === 0  || 
          electoralProcessInfo.dateIni.length === 0 || electoralProcessInfo.dateEnd.length === 0 ){
            isComplete = false;
      }
      if(!isComplete) {
        alert("Debe completar todos los campos antes de ir al siguiente paso");
        return;
      }
    }
    else if (activeStep === 1) {
      console.log(politicalPartiesList)
      if (politicalPartiesList.length === 0){
        alert("Debe crear al menos un partido político antes de ir al siguiente paso");
        return;
      }
      for (var index = 0; index < politicalPartiesList.length; index++) {
        const partie = politicalPartiesList[index]; 
        console.log(partie);
        if (partie.name.localeCompare("")=== 0){
          isComplete=false;
          break;
        }
        if (partie.partieLogo.length === 0){
          console.log(partie.partieLogo);
          isComplete=false;
          break;
        }
        if (partie.candidates.length === 0){
          isComplete=false;
          break;
        }
      }
      if (!isComplete) {
        alert("Debe completar todos los campos antes de ir al siguiente paso");
        return;
      } 
      handleUpdateVotingList(changeCandidates, politicalPartiesList);
    }
    else if (activeStep === 2){
      if (votingList.length === 0){
        alert("Debe crear almenos una lista de votación");
        return;
      }
      for (var index = 0; index < votingList.length; index++){
        const item = votingList[index];
        if (item.title.length=== 0){
          isComplete=false;
          break;
        }
        if (item.type.length === 0){
          isComplete=false;
          break;
        }
      }
      if (!isComplete) {
        alert("Debe completar todos los campos antes de finalizar la creación del proceso electoral");
        return;
      }

      //CHANGE the state of the electoral process to "created"
      handleChange (isComplete,"created");
      
    }
    if (isComplete){
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }    
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
            <Typography className={classes.instructions}>
                {handleReturn(props)}
          </Typography>
        ) : (
          <div>
            <Typography className={classes.instructions}>
              {getStepContent(activeStep)}
            </Typography>
            <div style={{paddingBottom:"20px"}}>
              <button
                type="button"
                className="btn btn-primary"
                disabled={activeStep === 0}
                onClick={handleBack}
              >
                Regresar
              </button>
              <button
                type="button"
                className="btn btn-primary"
                variant="contained"
                color="primary"
                onClick={ () => {
                  handleNext();
                }}
                style={{ float: "right" }}
              >
                {activeStep === steps.length - 1 ? "Terminar" : "Siguiente"}
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}



const mapStateToProps = (state) => {
  return {
    politicalPartiesList: state.politicalPartiesList,
    electoralProcessInfo: state.electoralProcess,
    votingList: state.votingList,
  }    
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleUpdateVotingList: (name,politicalPartiesList) => dispatch(handleUpdateVotingList(name,politicalPartiesList)),
    handleChange: (value,name) => dispatch(handleChange(value,name)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateElectoralProcess);