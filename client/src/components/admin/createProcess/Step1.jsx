import React, { Component } from 'react'
import { makeStyles } from "@material-ui/core/styles";
import {connect} from 'react-redux';
import {handleChange, validateDate} from '../../../redux/actions/electoralProcessActions';
import  '../admin_style.css';
import dateTime from '../../ManageDates';

const useStylesDATE = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(5),
    marginRight: theme.spacing(5),
    width: 200,
  },
}));

function DateAndTimePickers(props) {
  const classes = useStylesDATE();
  const {dateIni, dateEnd, handleChange, validateDate} = props;
  return (
    <form className={classes.container} noValidate>
       <div className= "col-sm-6">
             <label className="labeldateTimePicker">
              Fecha de inicio
            </label>
            <div >
            <input className="dateTimePicker"
                type="datetime-local" 
                id="datetime-inicio"
                name= "dateIni"
                min={dateTime()} 
                //max= {dateEnd}
                value = {dateIni}
                onChange= {(e) => {
                  handleChange(e.target.value,e.target.name);
                  validateDate();
                }}
            />
            </div>
          </div>

          <div className= "col-sm-6">
            <label className="labeldateTimePicker">
              Fecha fin
            </label>
            <div>
            <input className="dateTimePicker"
                type="datetime-local" 
                id="datetime-fin"
                name="dateEnd"
                value = {dateEnd}
                min={dateIni} 
                onChange= {(e) => handleChange(e.target.value,e.target.name)}
            />
            </div>
          </div>   
    </form>
  );
}

class Step1 extends Component{
  constructor(props) {
    super();
  }

  render () {
    const {title, description, dateEnd, dateIni} = this.props.electoralProcess;
    const {handleChange, validateDate} = this.props;
    console.log(this.props);
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card card-primary card-outline">
              <div className="card-header">
                <h3 className="card-title">
                  Información general del proceso electoral
                </h3>
              </div>
              <div className="card-body">
                <form className="form-horizontal">
                  <div className="row">
                    <div className="col-sm-12">
                      <div className="form-group row">
                        <label className="col-sm-2 col-form-label">
                          Título
                        </label>
                        <div className="col-sm-10">
                          <input
                            maxlength="100"
                            type="text"
                            name="title"
                            className="form-control"
                            placeholder="Titulo"
                            value = {title}
                            onChange= {(e) => handleChange(e.target.value,e.target.name)}
                          />
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="col-sm-2 col-fo rm-label">
                          Descripción
                        </label>
                        <div className="col-sm-10">
                          <textarea
                            maxlength="300"
                            class="form-control"
                            rows="3"
                            name="description"
                            placeholder="Escribir una breve descripción del proceso electoral ..."
                            value = {description}
                            onChange= {(e) => handleChange(e.target.value,e.target.name)}
                          ></textarea>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="col-sm-2 col-form-label">
                          Rango de fecha
                        </label>
                        <div className="col-sm-10">
                          <DateAndTimePickers 
                            handleChange= {handleChange}
                            dateEnd = {dateEnd}
                            dateIni = {dateIni}
                            validateDate = {validateDate}
                          /> 
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      electoralProcess: state.electoralProcess,
  }    
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleChange: (value,name) => dispatch(handleChange(value,name)),
    validateDate: () => dispatch(validateDate()),
    
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Step1);