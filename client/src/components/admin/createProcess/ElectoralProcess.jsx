import React, { Component } from "react";
import CreateElectoralProcess from "../createProcess/CreateElectoralProcess";
import { connect } from "react-redux";
import InfGeneralEdit from "../edit/InfGeneralEdit";
import ListVotingEdit from "../edit/ListVotingEdit";
import PoliticalPartiesListEdit from "../edit/PoliticalPartiesListEdit.jsx";
import { handleSaveListVoting } from "../../../redux/actions/votingListActions";
import { handleSaveElectoralProcess } from "../../../redux/actions/electoralProcessActions";
import { handleSavePoliticalParties } from "../../../redux/actions/politicalPartiesListActions";
import Spinner from "react-bootstrap/Spinner";
import dateTime from "../../ManageDates";

class ElectoralProcess extends Component {
  constructor(props) {
    super();
    this.state = {
      created: false,
      loading: true,
      textswitch: "Iniciar proceso electoral",
      enableStartProcess: false,
      enableFinishtProcess: false,
      startProcess: false,
      finishProcess: false,
      checked: false,
      //timeVoting: false,
    };
    this.createElectoralProcess = this.createElectoralProcess.bind(this);
  }
  createElectoralProcess = () => {
    //console.log("redirecciona a página de crear proceso electoral");
    this.props.handleNextComponent(CreateElectoralProcess);
  };
  handleEditInfGeneral() {
    //console.log("Redireccionando a Editar Informacion")
    this.props.handleNextComponent(InfGeneralEdit);
  }

  handleEditpoliticalPartiesList() {
    //console.log("Redireccionando a Editar Lista de Partidos politicos");
    this.props.handleNextComponent(PoliticalPartiesListEdit);
  }

  handleEditListVoting() {
    //console.log("Redireccionando a Editar Lista de votación");
    this.props.handleNextComponent(ListVotingEdit);
  }

  handleChangeStartProcess = async () => {
    const contract = this.props.contract;
    const accounts = this.props.accounts;
    if (!this.state.startProcess) {
      if (this.state.enableStartProcess) {
        const response = await contract.methods.getTallyAuthorities().call();
        console.log(response);
        let flag = true;
        for (let pos = 0; pos < response[0].length; pos++) {
          console.log(response[3][pos]);
          if (response[3][pos].localeCompare("1") === 0) {
            flag = false;
            console.log("bais");
            break;
          }
        }
        console.log(flag);
        if (response[0].length === 0) {
          alert(
            "Debe completar la configuración para la encriptación antes de iniciar el proceso electoral!"
          );
          return;
        }
        if (flag) {
          const voters = await contract.methods.getVoters().call();
          console.log(voters);
          if (voters[0].length === 0) {
            alert(
              "Debe registrar los electores antes de iniciar el proceso electoral!"
            );
            return;
          }

          await contract.methods
            .setFlagProcessStart()
            .send({ from: accounts[0] });
          alert("Proceso electoral iniciado!");
          this.props.showTitleStart();
          this.setState({
            textswitch: "Finalizar proceso electoral",
            startProcess: true,
          });
        } else {
          alert(
            "Todavia no han completado sus claves de encriptación todos los auditores!"
          );
        }
      } else {
        alert("Todavia no esta habilitado para empezar el proceso electoral!");
        this.setState({ checked: false });
        return;
      }
    } else if (!this.state.finishProcess) {
      if (this.state.enableFinishtProcess) {
        await contract.methods.finishProcess(true).send({ from: accounts[0] });
        this.props.showTitleEnd();
        this.setState({ checked: true });
      } else {
        alert(
          "Todavia no esta habilitado para finalizar el proceso electoral!"
        );
        this.setState({ checked: false });
        return;
      }
    }
    return;
  };

  componentDidMount = async () => {
    console.log(this.props);
    const contract = this.props.contract;

    //Service for general Information
    const title = await contract.methods.title().call();
    const description = await contract.methods.description().call();
    const dateIni = await contract.methods.dateIni().call();
    const dateEnd = await contract.methods.dateEnd().call();

    console.log(title, description, dateIni, dateEnd);

    if (title !== null) {
      //Setting the state
      const stateGeneralInformation = {
        title: title,
        description: description,
        dateIni: dateIni,
        dateEnd: dateEnd,
        types: [
          { value: "unico", label: "Único candidato" },
          { value: "multiple", label: "Múltiples candidatos" },
        ],
        created: true,
      };

      //Validate start date
      const flagProcessStart = await contract.methods.flagProcessStart().call();
      const flagProcessFinish = await contract.methods
        .flagProcessFinish()
        .call();

      this.setState({
        startProcess: flagProcessStart,
        finishProcess: flagProcessFinish,
        textswitch: !flagProcessStart
          ? "Iniciar proceso electoral"
          : "Finalizar proceso electoral",
      });
      if (flagProcessStart) {
        this.props.showTitleStart();
      }
      if (flagProcessFinish) {
        this.props.showTitleEnd();
      }

      //Service for political Parties
      const response2 = await contract.methods.getPoliticalParties().call();
      const totalPoliticalParties = response2[0].length;
      let statePoliticalParties = [];
      for (let pos = 0; pos < totalPoliticalParties; pos++) {
        const name = response2[0][pos];
        let candidates = [];
        for (let cand = 0; cand < response2[1][pos].length; cand++) {
          candidates.push({
            value: response2[1][pos][cand],
            label: response2[1][pos][cand],
          });
        }
        statePoliticalParties.push({
          candidates: candidates,
          name: name,
          partieLogo: [],
        });
      }

      //Service for votingLists
      const response3 = await contract.methods.getVotingLists().call();
      const totalVotingList = response3[0].length;

      let stateVotingList = [];

      for (let pos = 0; pos < totalVotingList; pos++) {
        const title = response3[0][pos];
        const description = response3[1][pos];
        let display = true;
        let label;
        if (response3[2][pos].localeCompare("unico") === 0) {
          label = "Único candidato";
        } else {
          label = "Múltiples candidatos";
        }
        const type = { value: response3[2][pos], label: label };

        let politicalParties = [];
        for (let posPP = 0; posPP < totalPoliticalParties; posPP++) {
          const candidates = statePoliticalParties[posPP].candidates;
          const namePP = statePoliticalParties[posPP].name;
          let selectedCandidates = [];
          let isChecked = true;
          const candidatesIndexVotingList = await contract.methods
            .getCandidatesToVotingList()
            .call();
          console.log(candidatesIndexVotingList);
          const candPoliticalPartie = candidatesIndexVotingList[posPP];

          //Falta crear selecteeCandidates y isChecked

          for (let index = 0; index < candPoliticalPartie.length; index++) {
            // console.log("cand: ", candidatesIndexVotingList[posPP]," ", candidatesIndexVotingList[posPP][index], posPP, "valor: ",statePoliticalParties[posPP].candidates[index]);
            // const aux = posPP + 1;
            // console.log(parseInt(candidatesIndexVotingList[posPP][index]) === aux,  aux,parseInt(candidatesIndexVotingList[posPP][index]));
            if (parseInt(candPoliticalPartie[index]) === pos + 1) {
              console.log(
                "entro: [",
                pos,
                "] => ",
                statePoliticalParties[posPP].candidates[index]
              );
              selectedCandidates.push(
                statePoliticalParties[posPP].candidates[index]
              );
            }
          }
          if (selectedCandidates.length === 0) {
            isChecked = false;
          }
          politicalParties.push({
            candidates: candidates,
            name: namePP,
            isChecked: isChecked,
            selectedCandidates: selectedCandidates,
          });
        }
        stateVotingList.push({
          display: display,
          title: title,
          description: description,
          type: type,
          politicalParties: politicalParties,
        });
      }
      console.log(response2, response3);
      console.log(
        stateGeneralInformation,
        statePoliticalParties,
        stateVotingList
      );
      //Seteamos el local State
      await this.props.handleSaveElectoralProcess(stateGeneralInformation);
      await this.props.handleSavePoliticalParties(statePoliticalParties);
      await this.props.handleSaveListVoting(stateVotingList);

      let _validateDate = false;
      if (dateTime().localeCompare(dateIni) > -1) _validateDate = true;
      //if (dateTime.localeCompare((this.state.dateEnd))> -1) _validateDate = true;
      console.log(dateTime(), dateIni);

      let _endProcess = false;
      if (dateTime().localeCompare(dateEnd) > -1) _endProcess = true;
      console.log(_endProcess, dateEnd);

      await this.setState({
        loading: false,
        created: title.length !== 0,
        enableStartProcess: _validateDate,
        enableFinishtProcess: _endProcess,
      });
      console.log(this.state);
    } else {
      this.setState({ loading: false });
    }
  };

  render() {
    const { electoralProcess, politicalPartiesList, votingList } = this.props;
    console.log(electoralProcess, politicalPartiesList, votingList);
    console.log(this.state);
    if (this.state.loading) {
      return (
        <div style={{ width: "min-content", margin: "auto", zoom: 3 }}>
          <Spinner animation="border" variant="info" />
        </div>
      );
    }
    if (this.state.created) {
      return (
        <div>
          <section className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1>Proceso electoral</h1>
                </div>
                <div className="col-sm-6">
                  <ol className="breadcrumb float-sm-right">
                    <li className="breadcrumb-item active">
                      Proceso electoral
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </section>
          <section style={{ maxWidth: "900px", margin: "auto" }}>
            <div style={{ margin: "20px" }}>
              <div
                style={{
                  borderRadius: "20px",
                  border: "2px solid #007BFF",
                  width: "fit-content",
                  margin: "auto",
                  display:
                    this.props.user.role.localeCompare("auditor") === 0
                      ? "none"
                      : "block",
                }}
              >
                <div
                  className="custom-control custom-switch"
                  style={{
                    cursor: this.state.enableStartProcess
                      ? "context-menu"
                      : "not-allowed",
                    margin: "4px",
                    zoom: "1.5",
                    display: this.state.finishProcess ? "none" : "block",
                  }}
                >
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customSwitchElectoralProcess"
                    onChange={this.handleChangeStartProcess}
                    checked={this.state.checked}
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="customSwitchElectoralProcess"
                  >
                    {this.state.textswitch}
                  </label>
                </div>
              </div>
            </div>

            <div className="container-fluid">
              <div className="col-md-12">
                <div className="row" style={{ paddingBottom: "20px" }}>
                  <div
                    className="card card-primary card-outline"
                    style={{ width: "100%" }}
                  >
                    <div className="card-header">
                      <h3 className="card-title">
                        <b>Información general</b>
                      </h3>
                      <div className="card-tools">
                        <a
                          onClick={() => this.handleEditInfGeneral()}
                          style={{
                            cursor: "pointer",
                            color: "#007bff",
                            textDecoration: "none",
                            backgroundColor: "transparent",
                            display:
                              this.props.user.role.localeCompare("auditor") ===
                                0 || this.state.startProcess
                                ? "none"
                                : "block",
                          }}
                        >
                          Editar
                        </a>
                      </div>
                    </div>
                    <div className="card-body" style={{ lineHeight: 1.6 }}>
                      <dl style={{ marginBottom: "0px" }}>
                        <dt>Titulo:</dt>
                        <dd>{electoralProcess.title}</dd>
                        <dt>Descripción:</dt>
                        <dd>{electoralProcess.description}</dd>
                        <div className="row" style={{ marginLeft: "0px" }}>
                          <div className="column" style={{ width: "50%" }}>
                            <dt>Fecha inicio:</dt>
                            <dd>{electoralProcess.dateIni}</dd>
                          </div>
                          <div className="column" style={{ width: "50%" }}>
                            <dt>Fecha Fin:</dt>
                            <dd>{electoralProcess.dateEnd}</dd>
                          </div>
                        </div>
                      </dl>
                    </div>
                  </div>
                </div>

                <div className="row" style={{ paddingBottom: "20px" }}>
                  <div
                    className="card card-primary card-outline"
                    style={{ width: "100%" }}
                  >
                    <div className="card-header">
                      <h3 className="card-title">
                        <b>Lista de partidos políticos</b>
                      </h3>
                      <div className="card-tools">
                        <ul className="pagination pagination-sm float-right">
                          <a
                            onClick={() =>
                              this.handleEditpoliticalPartiesList()
                            }
                            style={{
                              cursor: "pointer",
                              color: "#007bff",
                              textDecoration: "none",
                              backgroundColor: "transparent",
                              display:
                                this.props.user.role.localeCompare(
                                  "auditor"
                                ) === 0 || this.state.startProcess
                                  ? "none"
                                  : "block",
                            }}
                          >
                            Editar
                          </a>
                        </ul>
                      </div>
                    </div>
                    <div class="card-body">
                      {politicalPartiesList.map((politicalPartie) => {
                        return (
                          <div className="callout callout-info">
                            <div className="row" style={{ marginLeft: "0px" }}>
                              <div className="column" style={{ width: "80%" }}>
                                <p>
                                  <b>Nombre:&nbsp;</b> {politicalPartie.name}
                                </p>
                                <p>
                                  <b>Lista de candidatos:&nbsp;</b>
                                </p>
                                <div style={{ marginLeft: "30px" }}>
                                  {politicalPartie.candidates.map(
                                    (candidate, index) => {
                                      return (
                                        <ul
                                          key={index}
                                          style={{ marginBottom: "0px" }}
                                        >
                                          <li>{candidate.label}</li>
                                        </ul>
                                      );
                                    }
                                  )}
                                </div>
                              </div>
                              <div className="column" style={{ width: "20%" }}>
                                <div style={{ width: "30%" }}>
                                  <img
                                    alt="Avatar"
                                    style={{ maxHeight: "95px" }}
                                    className="table-avatar"
                                    src="https://www.staffdigital.pe/blog/wp-content/uploads/01-10-feos-logos-de-partidos-pol%C3%ADticos-peruanos.jpg"
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>

                <div className="row" style={{ paddingBottom: "20px" }}>
                  <div
                    className="card card-primary card-outline"
                    style={{ width: "100%" }}
                  >
                    <div className="card-header">
                      <h3 className="card-title">
                        <b>Listas de votación </b>
                      </h3>
                      <div className="card-tools">
                        <a
                          onClick={() => this.handleEditListVoting()}
                          style={{
                            cursor: "pointer",
                            color: "#007bff",
                            textDecoration: "none",
                            backgroundColor: "transparent",
                            display:
                              this.props.user.role.localeCompare("auditor") ===
                                0 || this.state.startProcess
                                ? "none"
                                : "block",
                          }}
                        >
                          Editar
                        </a>
                      </div>
                    </div>
                    <div class="card-body">
                      {votingList.map((cedula) => {
                        return (
                          <div className="callout callout-info">
                            {/* <h5><b>{cedula.title} </b></h5>
                                <p>{cedula.description}</p> */}
                            <p>
                              <b>Titulo:&nbsp;</b>
                              {cedula.title}
                            </p>

                            <p>
                              <b>Descripición:&nbsp;</b>
                              {cedula.description}
                            </p>

                            <p>
                              <b>Tipo:&nbsp;</b>
                              {cedula.type.label}
                            </p>

                            <p>
                              <b>Lista de partidos políticos:&nbsp;</b>
                            </p>
                            {cedula.politicalParties.map((partie) => {
                              if (partie.isChecked) {
                                return (
                                  <div style={{ marginLeft: "30px" }}>
                                    <p style={{ marginBottom: "0.1rem" }}>
                                      <b>Partido:&nbsp;</b>
                                      {partie.name}
                                    </p>
                                    <div
                                      style={{
                                        marginLeft: "30px",
                                        marginBottom: "15px",
                                      }}
                                    >
                                      {partie.selectedCandidates.map(
                                        (candidate, index) => {
                                          return (
                                            <ul
                                              key={index}
                                              style={{ marginBottom: "0px" }}
                                            >
                                              <li>{candidate.label}</li>
                                            </ul>
                                          );
                                        }
                                      )}
                                    </div>
                                  </div>
                                );
                              }
                            })}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      );
    } else {
      return (
        <div>
          <section className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1> No existe ningún proceso electoral</h1>
                </div>
                <div className="col-sm-6">
                  <ol className="breadcrumb float-sm-right">
                    <li className="breadcrumb-item">
                      <a>Proceso electoral</a>
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </section>
          <div
            style={{
              width: "fit-content",
              margin: "40px auto",
              display:
                this.props.user.role.localeCompare("auditor") === 0
                  ? "none"
                  : "block",
            }}
          >
            <button
              className="btn btn-primary btn-lg"
              onClick={this.createElectoralProcess}
            >
              {" "}
              Crear proceso electoral
            </button>
          </div>

          <section></section>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    politicalPartiesList: state.politicalPartiesList,
    electoralProcess: state.electoralProcess,
    votingList: state.votingList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleSaveElectoralProcess: (value) =>
      dispatch(handleSaveElectoralProcess(value)),
    handleSavePoliticalParties: (value) =>
      dispatch(handleSavePoliticalParties(value)),
    handleSaveListVoting: (value) => dispatch(handleSaveListVoting(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ElectoralProcess);
