import React from "react";
import {
  handleSubmitLogin,
  handleChangeLogin,
} from "../../redux/actions/loginAdminActions";
import Cookies from "universal-cookie";
import { connect } from "react-redux";
import Election from "../../contracts/Election.json";
import getWeb3 from "../../getWeb3";
import Spinner from "react-bootstrap/Spinner";

const cookies = new Cookies();

class LoginAdmin extends React.Component {
  constructor(props) {
    super();
    this.state = {
      form: {
        dni: "",
      },
      storageValue: 0,
      web3: null,
      accounts: null,
      contract: null,
      msgAlert: false,
      loadingLogin: false,
    };
  }

  componentDidMount = async () => {
    

    const user = cookies.get("user", { path: "/admin" });
    //console.log("user", user);
    if (user) {
      //If I'm already logged
      window.location.href = "./admin";
    }

    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Election.networks[networkId];
      const instance = new web3.eth.Contract(
        Election.abi,
        deployedNetwork && deployedNetwork.address
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({ web3, accounts, contract: instance });
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check //console for details.`
      );
      console.error(error);
    }

    console.log(this.props);

    const varInput = "input-dni";
    const varButton = "input-button";

    let myInput = document.getElementById(varInput);
    myInput.addEventListener("keyup", function (e) {
      if (e.keyCode === 13) {
        e.preventDefault();
        document.getElementById(varButton).click();
      }
    });
  };

  handleSubmit = async () => {
    const form = this.state.form;

    if (form.dni.length === 0) {
      alert("Introduzca su DNI para iniciar sesion");
      return;
    } else {
      try {
        this.setState({ loadingLogin: true });

        const { contract, accounts } = this.state;
        const staff_address = await contract.methods
          .getTallyAuthorities()
          .call();

        console.log(
          staff_address,
          accounts[0],
          typeof accounts[0],
          typeof staff_address[1][0]
        );

        let flag = false;
        for (let pos = 0; pos < staff_address[1].length; pos++) {
          if (staff_address[1][pos].localeCompare(accounts[0])===0) {
            console.log("holi bb");
            flag = true;
            break;
          }
        }
        //Now I gonna validate with the admin
        if (!flag) {
          const admin_address = await contract.methods.owner().call();
          if (admin_address === accounts[0]){
            flag = true;
          }
        }
        if (!flag) {
          alert(
            "La cuenta " +
              accounts[0] +
              " no tiene permisos para realizar transacciones como administrador"
          );
          this.setState({ loadingLogin: false });
          return;
        }

        const response = await contract.methods.validateLoginAdmin(form.dni, accounts[0]).call();

        console.log("response", response);

        if (response.flag) {
          this.setState({ msgAlert: false });
          const user = {
            type: response.role,
            lastName: response.lastName,
            name: response.name,
            role: response.role,
            account: this.state.accounts[0],
          };
          cookies.set("user", user, { path: "/admin" });

          const contract = this.state.contract;
          cookies.set("contract", contract, { path: "/admin" });

          window.location.href = "./admin";
        } else {
          this.setState({ loadingLogin: false });
          this.setState({ msgAlert: true });
        }
      } catch (err) {
        console.log(err);
        alert("Error!", err);
        this.setState({ loadingLogin: false });
      }
    }
  };

  handleChange = async (e) => {
    await this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  render() {
    const { handleChangeLogin, dataLogin } = this.props;

    console.log(this.props);
    console.log(this.state);
    if (!this.state.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    return (
      <div style={{ paddingTop: "50px" }} className="background-login">
        <div className="login-box" style={styles.body}>
          <div className="login-logo">
            <img
              src="images/logo.png"
              alt="AdminLTE Logo"
              className="brand-logo"
              style={{
                opacity: ".8",
                marginRight: "6px",
                maxHeight: "90px",
                marginBottom: "10px",
              }}
            />
          </div>
          {/* /.login-logo */}
          <div className="card">
            <div className="card-header p-2">
              <ul className="nav nav-pills">
                <h4 style={{ margin: "auto" }}>Iniciar sesión</h4>
              </ul>
            </div>
            <div className="card-body">
              <div>
                {/* /.INICIAR SESION - ELECTOR */}
                <p className="login-box-msg">
                  <b>Personal Adminstrativo</b>
                </p>
                {/* /.tab-pane */}
                <div className="tab-pane" id="admin">
                  <div className="form-admin">
                    {/* /.INICIAR SESION - ELECTOR */}
                    <div className="input-group mb-3">
                      <input
                        id="input-dni"
                        maxLength="20"
                        type="text"
                        className="form-control"
                        name="dni"
                        placeholder="Ingrese su DNI"
                        onChange={(e) => {
                          this.handleChange(e);
                          handleChangeLogin(e.target.value, e.target.name);
                        }}
                        required
                      />
                      <div className="input-group-append">
                        <div className="input-group-text">
                          <span className="fas fa-envelope" />
                        </div>
                      </div>
                    </div>

                    <div
                      style={{
                        display: this.state.msgAlert ? "block" : "none",
                      }}
                    >
                      <div
                        className="alert alert-danger alert-dismissible"
                        style={styleAlert}
                        role="alert"
                      >
                        DNI incorrecto!
                      </div>
                    </div>
                    <button
                      id="input-button"
                      type="submit"
                      className="btn btn-block btn-primary "
                      name="submit_admin"
                      onClick={() => this.handleSubmit()}
                      disabled={this.state.loadingLogin ? true : false}
                    >
                      <Spinner
                        style={{
                          display: this.state.loadingLogin
                            ? "inline-block"
                            : "none",
                        }}
                        as="span"
                        animation="grow"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                      />
                      Iniciar sesión
                    </button>
                  </div>
                </div>
                {/* /.tab-pane */}
              </div>
              {/* /.tab-content */}
            </div>
            {/* /.card-body */}
          </div>
        </div>
        <br />
        <br />

        <div
          style={{
            textAlign: "center",
            margin: "auto",
            opacity: "0.78",
            color: "#fff !important",
            backgroundColor: "#000!important",
            padding: "12px 24px!important",
          }}
        >
          <span className="w3-center w3-padding-large w3-black w3-large w3-wide w3-animate-opacity">
            SU CUENTA ES
            <span className="w3-hide-small" style={{ fontSize: "small" }}>
              :{this.state.accounts[0]}
            </span>
          </span>
        </div>
      </div>
    );
  }
}

var styles = {
  body: {
    margin: "auto",
  },
};

var styleAlert = {
  color: "#721c24",
  backgroundColor: "#f8d7da",
  borderColor: " #f5c6cb",
  fontSize: "1rem",
};

const mapStateToProps = (state) => {
  return {
    dataLogin: state.dataLogin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleSubmitLogin: () => dispatch(handleSubmitLogin()),
    handleChangeLogin: (value, name) =>
      dispatch(handleChangeLogin(value, name)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginAdmin);
