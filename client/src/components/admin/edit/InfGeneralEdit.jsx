import React, { Component } from 'react';
import Step1 from '../createProcess/Step1';
import ElectoralProcess from '../createProcess/ElectoralProcess';
import {handleSaveElectoralProcess,handleCancelElectoralProcess} from '../../../redux/actions/electoralProcessActions';
import { connect } from 'react-redux';

class InfGeneralEdit extends Component {
    constructor(props) {
        super();
    }
    handleElectoralProcess() {
        //console.log("Redireccionando a ElectoralProcess")
        this.props.handleNextComponent(ElectoralProcess);
    }
    handleElectoralProcess() {
        //console.log("Redireccionando a ElectoralProcess")
        this.props.handleNextComponent(ElectoralProcess);
    }
    handleSaveChanges = async () => {
        const contract = this.props.contract;
        const accounts = this.props.accounts;
        const electoralProcess = this.props.electoralProcess;
        //console.log(contract, accounts, electoralProcess);

        let isComplete =  true;
        if (electoralProcess.title.length === 0 || electoralProcess.description.length === 0  || 
            electoralProcess.dateIni.length === 0 || electoralProcess.dateEnd.length === 0 ){
              isComplete = false;
        }
        if(!isComplete) {
          alert("Debe completar todos los campos para poder guardar los cambios");
          return;
        }

        await contract.methods.setGeneralInformation(electoralProcess.title, electoralProcess.description , 
            electoralProcess.dateIni, electoralProcess.dateEnd).send({ from: accounts[0] })

        this.handleElectoralProcess();
    }
    render() {
        const {handleCancelElectoralProcess} = this.props;
        //console.log("ed")
        return (
            <div >
              <section className="content-header" >
                <div className="container-fluid">
                    <div className="row mb-2">
                    <div className="col-sm-6">
                      <h1>Editar Información general</h1>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-sm-right">
                        <li className="breadcrumb-item">
                            <a style={{cursor: 'pointer',color:'#007bff', textDecoration:"none", backgroundColor:'transparent'}} onClick={() => this.handleElectoralProcess()}>Proceso Electoral</a>
                        </li>
                        <li className="breadcrumb-item active">Editar Información general</li>
                        </ol>
                    </div>
                    </div>
                </div>
              </section>
              <section style={{ maxWidth: "1100px", margin: "auto" }}>
                <div className="container-fluid">
                <div className="col-md-12">
                  <div className="row" >
                    <Step1/>
                   </div>
                    <div className="col">
                        <button 
                            className="btn btn-default" 
                            onClick={() => {
                                handleCancelElectoralProcess();
                                this.handleElectoralProcess();
                            }}
                        > Cancelar</button>
                        <button 
                            className="btn btn-primary  float-right" 
                            onClick={() => {
                                this.handleSaveChanges();
                            }}
                        > Aceptar</button>
                    </div>
                </div>
                </div>
              </section>
            </div>
            )
    }
}

const mapStateToProps = (state) => {
    return {
        electoralProcess: state.electoralProcess
    }    
}

const mapDispatchToProps = (dispatch) => {
    return {
      handleSaveElectoralProcess: () => dispatch(handleSaveElectoralProcess()),
      handleCancelElectoralProcess: () => dispatch(handleCancelElectoralProcess()),
    };
  }

export default connect(mapStateToProps, mapDispatchToProps)(InfGeneralEdit)
