import React, { Component } from "react";
import { connect } from "react-redux";
import DragDropFile from "./DragDropFile";
import { parse } from "papaparse";
import { MDBDataTableV5 } from "mdbreact";
import {
  proccesData,
  handleUpload,
} from "../../../redux/actions/votersActions";
import Spinner from "react-bootstrap/Spinner";
import "../admin_style.css";
import axios from "axios";

class Electores extends Component {
  constructor(props) {
    super();
    this.state = {
      flag: false,
      data: {
        columns: [
          {
            label: "Nombre",
            field: "name",
            width: "40",
            attributes: {
              "aria-controls": "DataTable",
              "aria-label": "Name",
            },
          },
          {
            label: "Apellidos",
            field: "lastName",
            width: "40",
          },
          {
            label: "Email",
            field: "email",
            width: "60",
          },
          {
            label: "DNI",
            field: "dni",
            sort: "asc",
            width: "40",
          },
          {
            label: "Clave pública",
            field: "publicKey",
            sort: "disabled",
            width: "300",
          },
        ],
        rows: [],
      },
      loading: true,
      emailsSent: false,
    };
  }

  handleUpload = (file) => {
    this.props.handleUpload(file);
    this.setState({ flag: true });
  };

  handleClick = async () => {
    //Processing the data of the file
    const dataFile = this.props.voters.file;

    const text = await dataFile.text();
    const result = await parse(text, { header: true });
    let aux = [];

    console.log(result.data);

    result.data.map((item) => {
      aux.push({
        dni: item.dni,
        castVote: false,
        email: item.email,
        index: "",
        lastName: item.lastName,
        name: item.name,
        publicKey: item.publicKey,
      });
    });

    console.log(aux);

    await this.setState({
      data: {
        ...this.state.data,
        rows: aux,
      },
    });
    console.log(this.state.data.rows);

    this.savingDatainBlockchain();
  };

  savingDatainBlockchain = async () => {
    //Saving the data
    let _dnis = [];
    let _emails = [];
    let _firstNames = [];
    let _lastNames = [];
    let _publicKeys = [];

    for (let index = 0; index < this.state.data.rows.length; index++) {
      const item = this.state.data.rows[index];
      _dnis.push(item.dni);
      _emails.push(item.email);
      _firstNames.push(item.name);
      _lastNames.push(item.lastName);
      _publicKeys.push(item.publicKey);
    }

    const contract = this.props.contract;
    const accounts = this.props.accounts;

    try {
      console.log(_dnis, _emails, _firstNames, _lastNames, _publicKeys);
      console.log("accounts[0]:",accounts[0], "contract:",contract);
    

      contract.methods
        .addVoters(_dnis, _emails, _firstNames, _lastNames, _publicKeys)
        .send({ from: accounts[0] })
        .then(() => {
          document.getElementById("row-lista-electores").style.display =
            "block";
          this.setState({ flag: false });
        });
    } catch (err) {
      console.log(err);
    }
  };

  componentDidMount = async () => {
    const contract = this.props.contract;
    const response = await contract.methods.getVoters().call();
    console.log("response : ",response);
    //If data exist save in local state and show
    try {
      if (response[0].length > 0) {
        let aux = [];
        console.log("entre");
        response[0].map((item, index) => {
          aux.push({
            dni: response[0][index],
            email: response[1][index],
            lastName: response[3][index],
            name: response[2][index],
            publicKey: response[4][index],
          });
        });

        console.log(aux);

        await this.setState({
          data: {
            ...this.state.data,
            rows: aux,
          },
        });
        await this.setState({ loading: false, flag: false });
        try {
          document.getElementById("row-lista-electores").style.display = "block";
        } catch (err) {
          console.log(err);
        }
      } else {
        this.setState({ loading: false });
      }
    }catch(err){
      console.log("err");
    }
    

    //Validate if I sent the emails before
    const response2 = await contract.methods.flagSentEmailElectors().call();
    console.log(response2);
    this.setState({ emailsSent: response2 });
  };

  handleSendNotifications = async () => {
    const accounts = this.props.accounts;
    const contract = this.props.contract;
    try {
      //Calling the service to send emails
      console.log(this.state.data.rows);
      axios
        .post(
          "http://localhost:8080/electors/notifyElectors",
          this.state.data.rows
        )
        .then((response) => {
          console.log("Emails are sendind ...", response);
          this.setState({ emailsSent: true });

          contract.methods
            .setFlagSentEmailElectors(true)
            .send({ from: accounts[0] });
        });
    } catch (error) {
      console.log(error);
    }
  };
  render() {
    console.log(this.props, "state: ", this.state);
    if (this.state.loading) {
      return (
        <div style={{ width: "min-content", margin: "auto", zoom: 3 }}>
          <Spinner animation="border" variant="info" />
        </div>
      );
    }
    return (
      <div>
        <div
          id="modal-notification"
          className="modal fade"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-success">
            <div className="modal-content" style={{ paddingBottom: "0px" }}>
              <div className="modal-header flex-column">
                <div className="icon-box">
                  <i className="fas fa-exclamation"></i>
                </div>
                <h3 className="modal-title w-100">Alerta!</h3>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-hidden="true"
                >
                  ×
                </button>
              </div>
              <div
                style={{ paddingTop: "5px", paddingBottom: "5px" }}
                className="modal-body"
              >
                <p style={{ lineHeight: "1.7 rem" }}>
                  Se enviarán una notificación a cada elector brindandoles la
                  información sobre el proceso electoral
                </p>
              </div>
              <div className="modal-footer justify-content-center">
                <button
                  type="button"
                  className="btn btn-info"
                  data-dismiss="modal"
                  style={{ marginRight: "30px" }}
                  onClick={() => this.handleSendNotifications()}
                >
                  Enviar
                </button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  style={{ marginLeft: "30px" }}
                  data-dismiss="modal"
                >
                  Cancelar
                </button>
              </div>
            </div>
          </div>
        </div>
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Lista de electores</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item active">Electores</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="container-fluid">
            <div className="col-md-12">
              <div
                style={{
                  display:
                    this.props.user.role.localeCompare("auditor") === 0
                      ? "none"
                      : "block",
                }}
              >
                <section
                  id="section-get-data"
                  // style={{ display: this.state.emailsSent ? "none" : "block" }}
                >
                  <div
                    className="row"
                    id="dragDropFile"
                    style={{
                      paddingBottom: "20px",
                      maxWidth: "600px",
                      margin: "auto",
                    }}
                  >
                    <DragDropFile
                      handleDisplayButton={this.handleDisplayButton}
                      handleUpload={this.handleUpload}
                    />
                  </div>
                  <div style={{ width: "fit-content", margin: "auto" }}>
                    <button
                      className="btn btn-primary btn-lg"
                      style={{ margin: "20px" }}
                      disabled={!this.state.flag}
                      onClick={() => this.handleClick()}
                    >
                      {" "}
                      Procesar archivo
                    </button>
                  </div>
                </section>
              </div>
              <div
                className="alert alert-info alert-dismissible fade show"
                role="alert"
                style={{
                  color: "#0c5460",
                  backgroundColor: "#d1ecf1",
                  borderColor: "#bee5eb",
                  display: this.state.emailsSent ? "block" : "none",
                }}
              >
                {this.state.emailsSent ? (
                  <div>
                    <strong>Proceso exitoso!</strong> Los correos ya han sido
                    enviados.
                  </div>
                ) : (
                  <div>
                    <strong>Proceso exitoso!</strong> Se han enviado los correos
                    con credenciales a los usuarios.
                  </div>
                )}
                <button
                  type="button"
                  className="close"
                  data-dismiss="alert"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>

              <div
                className="row"
                style={{ margin: "20px", display: "none" }}
                id="row-lista-electores"
              >
                <div className="card card-primary card-outline">
                  <div className="card-header row" style={{ margin: "0px" }}>
                    <div className="col" style={{ paddingTop: "8px" }}>
                      <h3 className="card-title">
                        <b>Lista de electores</b>
                      </h3>
                    </div>
                    <div className="col" style={{ display: "contents" }}>
                      <span
                        className="align-right"
                        style={{
                          marginRight: "15px",
                          marginTop: "5px",
                          marginBottom: "5px",
                        }}
                      >
                        <a
                          data-toggle="modal"
                          data-target="#modal-notification"
                          style={{
                            cursor: "pointer",
                            lineHeight: "1.5",
                            borderRadius: ".3rem",
                            color: "#fff",
                            backgroundColor: "#007bff",
                            borderColor: "#007bff",
                            boxShadow: "none",
                            border: "1px solid transparent",
                            padding: ".575rem .75rem",
                            display:
                              this.props.user.role.localeCompare("auditor") ===
                              0
                                ? "none"
                                : "block",
                          }}
                          title="Enviar notificación a electores"
                        >
                          <i
                              class="far fa-envelope"
                              style={{marginRight: "5px" }}
                            />
                          Enviar notificación
                        </a>
                      </span>
                    </div>
                  </div>
                  <div className="card-body" style={{ lineHeight: 1.6 }}>
                    <MDBDataTableV5
                      hover
                      entriesOptions={[5, 20, 25]}
                      entries={5}
                      pagesAmount={4}
                      data={this.state.data}
                      searchTop={false}
                      //searching={true}
                      striped
                      bordered
                      infoLabel={["Mostrando", "al", "de", "entradas"]}
                      entriesLabel=" Cantidad"
                      paginationLabel={["Prev", "Sigui"]}
                      responsive
                      searchLabel="Buscar"
                      scrollY
                      maxHeight="400px"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    voters: state.voters,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleUpload: (file) => dispatch(handleUpload(file)),
    proccesData: () => dispatch(proccesData()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Electores);
