import React, {useMemo, useState, useEffect } from 'react';
import {useDropzone} from 'react-dropzone';

const baseStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '20px',
    borderWidth: 2,
    borderRadius: 2,
    borderColor: '#eeeeee',
    borderStyle: 'dashed',
    backgroundColor: '#fafafa',
    color: '#bdbdbd',
    outline: 'none',
    transition: 'border .24s ease-in-out'
};
  
const activeStyle = {
    borderColor: '#2196f3'
};
  
const acceptStyle = {
    borderColor: '#00e676'
};
  
const rejectStyle = {
    borderColor: '#ff1744'
};


  
export default function DragDropFile(props) {
  const [file, setFile] = useState([]);
    const {
      getRootProps,
      getInputProps,
      isDragActive,
      isDragAccept,
      isDragReject,
      acceptedFiles,
      open
    } = useDropzone({
      noClick: true,
      noKeyboard: true,
      accept: '.csv',
      onDrop: acceptedFiles => {
        console.log("He cargado archivo!",acceptedFiles,acceptedFiles[0]);
        setFile(acceptedFiles.map(file => Object.assign(file, {
          preview: URL.createObjectURL(file)
        })));
        if (acceptedFiles.length === 0){
          alert("Solo subir archivos de tipo .csv");
        }else {
          props.handleUpload(acceptedFiles[0]);
        }
        
      },
    });
  
    const style = useMemo(() => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {borderColor: "#17a2b8"}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {})
    }), [
      isDragActive,
      isDragReject,
      isDragAccept
    ]);

    const nameFile = acceptedFiles.map(file => {
      return (
        <p key={file.path} style={{padding:'10px', marginBottom:'0px'}}>
         <b>Archivo: &nbsp;</b> {file.path} - {file.size} bytes
        </p>
      ) 
    });
    
    return (
      <div className="container">
        <div {...getRootProps({style})}>
          <input {...getInputProps()} />
          <p>Arrastrar solo archivos .csv</p>
          <button type="button" className="btn btn-block btn-info" onClick={open}>
            Subir archivo de electores
          </button>
        </div>
        <aside>
          <div>
            {nameFile}
          </div>    
        </aside>
      </div>
    );
}


