import React, { Component } from 'react';
import {connect} from 'react-redux';

class Resume extends Component {
    constructor (props){
        super();
        this.state= {}
    }
    render() {
        const {votingList, electorSelections } = this.props;
        console.log(this.props);

        return (
            <div className="container-fluid">
                <div className="row">
                <div className="col-md-12">
                    <div className="card card-primary card-outline" style={{borderTop:'3px solid #17a2b8'}}>
                    <div className="card-header">
                        <h1 className="card-title">
                        <b>Resumen de selección de listas de votación</b>
                        </h1>
                    </div>
                    <div className="card-body">
                        <div className="alert alert-info" style={styleAlertInfo} role="alert">
                            Este es un resumen de su selección de las listas de votación. Sin embargo, puede modificarlas en cualquier momento
                            antes de confirmar la emisión de su voto. 
                        </div> 


                        <form className="form" style={{border: '1px solid rgb(23, 162, 184)', padding:'10px'}}>
                            {votingList.map((item,index) => {
                                console.log(item);
                                const selected = electorSelections.listVotation[index].selected;
                                console.log(selected);
                                if (electorSelections.listVotation[index].indexInList < 0) {
                                    return (
                                        <div  style={{padding:'10px'}}>
                                        <h5>{"Selección de "+item.title}</h5>
                                        <div className="inputGroup row">
                                            <input 
                                            className="col"
                                            name={"radio"+index} 
                                            type="radio" 
                                            checked
                                            />
                                            <label htmlFor={"radio"+index} className="row">
                                                <div className="col" style={{width:'30%'}}>
                                                    <a>VOTO EN BLANCO<br/> <small>No votar por nadie</small></a>
                                                </div>
                                                <div className="col" style={{width:'70%'}}>
                                                    <div style={{maxWidth:'150px', border: '1px solid black'}}> <h3 style={{paddingLeft:'20px'}}><b> VOTO EN BLANCO</b></h3> </div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    )
                                }
                                return (
                                    <div  style={{padding:'10px'}}>
                                        <h5>{"Selección de "+item.title}</h5>
                                        <div className="inputGroup row">
                                            <input 
                                            className="col"
                                            name={"radio"+index} 
                                            type="radio" 
                                            checked
                                            />
                                            <label htmlFor={"radio"+index} className="row">
                                                <div className="col" style={{width:'30%'}}>
                                                    <a>{selected.politicalPartie} <br/> <small style={{display: item.type.value.localeCompare("unico")===0 ? 'block': 'none'}}>{selected.candidate}</small></a>
                                                </div>
                                                <div className="col" style={{width:'70%'}}>
                                                    <img style={{height:'112px' ,width:'150px'}}src={selected.image}></img>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                )  
                            })}
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            );
    }
}

const mapStateToProps = (state) => {
    return {
        //politicalPartiesList: state.politicalPartiesList,
        //electoralProcess: state.electoralProcess,
        votingList: state.votingList,
        electorSelections: state.electorSelections,
    }    
  }
    
  const mapDispatchToProps = (dispatch) => {
    return {
      //handleChangeSelectionCandidate: (selected,posVotingList) => dispatch(handleChangeSelectionCandidate(selected,posVotingList)),
    };
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(Resume);

const styleAlertInfo = {
    color: '#0c5460',
    backgroundColor: '#d1ecf1',
    borderColor: '#bee5eb',
}
const line= {
    width: '112px',
    height: '47px',
    borderBottom: '1px solid black',
    position: 'absolute',
}