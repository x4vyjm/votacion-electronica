import React, { Component, useState } from 'react'
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Typography from "@material-ui/core/Typography";
import Resume from './Resume';
import VotoUnicoCard from './VotoUnicoCard';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner'
import Button from 'react-bootstrap/Button';
import IndividualVerification from './IndividualVerification';
import {connect} from 'react-redux';
import Cookies from 'universal-cookie';
//import { StepFunctions } from 'aws-sdk';
import {handleGeneralInformationSelection} from '../../../redux/actions/electorSelectionsActions';
//import VotoPreferencial from './VotoPreferencial';

const cookies = new Cookies();

class VotationList extends Component {
    constructor(props) {
        super();
        this.state = {
          forms: [],
          loading: true,
        }
    }
    componentDidMount  = async () => {
      console.log(this.state,this.props);
      const votingList = this.props.votingList;
      //Cedulas
      const lengthVotingList = votingList.length; 
      var forms = new Array(lengthVotingList + 1);
      console.log(forms);
      
      votingList.map((item,index) => {
        var cedula = 
                  <div key = {"cedula-0"+ index}>
                    <VotoUnicoCard
                        title = {item.title}
                        description = {item.description}
                        selected = {-1}
                        posVotingList = {index}
                        display = {item.type.value.localeCompare("unico")===0 ?  'block' : 'none'}
                        data = {item.politicalParties}
                    />
                  </div>;
        forms[index] = cedula;
      })
      forms[lengthVotingList] = <div key="resume-cedula"><Resume/>
                                </div>;
      console.log(forms);
      
      //Steps
      let steps = [];
      votingList.map((step) => {
        steps.push(step.title);
      })
      steps.push("Resumen de selección");

      const elector = cookies.get('elector',{path:'/elector'});
      console.log("elector", elector);
      //General Informaton needed for Selections

      this.props.handleGeneralInformationSelection(elector.publicKey,elector.DNI, lengthVotingList);

      await this.setState({steps:steps, forms:forms, loading: false});
      return;
    }

    render() {
      console.log(this.props);
      if (this.state.loading){
        return <div style={{marginTop:'100px'}}> Cargando listas...</div>;
      }
      return (
        <section
        className="content"
        style={{ maxWidth: "1100px", margin: "auto",paddingTop:'70px'}}
        >
        <div className="container-fluid">
            <div className="row" >
            <div className="col-md-12">
            <h2><b>Listas de votación</b></h2>
                <CastingVote
                    handleNextComponent = {this.props.handleNextComponent}
                    votingList = {this.props.votingList}
                    politicalPartiesList = {this.props.politicalPartiesList}
                    forms = {this.state.forms}
                    steps = {this.state.steps}
                    electorSelections = {this.props.electorSelections}
                    contract={this.props.contract}
                    accounts={this.props.accounts}
                />
            </div>
            </div>
        </div>
        </section>
      )
    }
}
  
  
  
const useStyles = makeStyles((theme) => ({
    root: {
      width: "100%",
    },
    backButton: {
      marginRight: theme.spacing(1),
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  }));
  
function getSteps(props) {
  let steps = [];
  props.votingList.map((step) => {
    steps.push(step.title);
  })
  steps.push("Resumen de selección")
  return steps;
  
}
  
function getStepContent(stepIndex) {
    // switch (stepIndex) {
    //   case 0:
    //     return <div><VotoUnicoCard/></div>;
    //   case 1:
    //     return  <div><VotoPreferencial/></div>;
    //   case 2:
    //     return  <div><Resume/></div>;
    //   default:
    //     return "Unknown step";
    // }
}
  
  
function CastingVote(props) {
  console.log(props)
    
    //Some functions for the steps
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = props.steps; //getSteps(props);
    const handleNext = () => {  
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {

      setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
    //Functions for the  Modal verification individual
    const [show, setShow] = useState(false);
    const [loading,setLoading] = useState(true);
    const handleClose = () => setShow(false);
    
    const handleReturn = () => {
      setShow(false);
      props.handleNextComponent(IndividualVerification);
    }  
  
    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
          >
            <Modal.Header>
              <Modal.Title>Comprobante de emisión de voto</Modal.Title>
            </Modal.Header>
            {(loading) ?
              (<div>
                <Modal.Body>
                  <h5>Por favor esperar, se esta generando su comprobante... </h5>
                  <div style={{width:'min-content', margin:'auto'}}><Spinner animation="border" style={{zoom:2}}/></div>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="primary" disabled>
                    <Spinner
                      as="span"
                      animation="grow"
                      size="sm"
                      role="status"
                      aria-hidden="true"
                    />
                    Cargando...
                  </Button>
                </Modal.Footer>
              </div>
            )
            :
            (<div>
              <Modal.Body>
                <h5>Se ha enviado el comprobante de emisión de voto a su correo electrónico</h5>
                <h6>Puede ver los resultados en la sección <b>Resultados</b> después de que haya finalizado el proceso de votación.</h6>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="primary" onClick={handleReturn}>Ir a Resultados</Button>
              </Modal.Footer>
            </div>
            )}
            
            </Modal>

          {activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions} >
                {() => {handleReturn()}}
              </Typography>
            </div>
          ) : (
            <div>
              <Typography className={classes.instructions}>
                {/* {getStepContent(props.votingList, activeStep)} */}
                {props.forms[activeStep]}
              </Typography>
              <div style={{paddingBottom:"20px"}}>
                <button
                  type="button"
                  className="w3-button w3-light-gray"
                  disabled={activeStep === 0}
                  onClick={handleBack}
                >
                  Regresar
                </button>
                <button
                  type="button"
                  className="w3-button w3-black"
                  variant="contained"
                  color="primary"
                  onClick={async () => {
                    if (activeStep === steps.length - 1){
                      setLoading(true);
                      setShow(true);

                      //Llamar a servicio para guardar votos..... 
                      const contract = props.contract;
                      const accounts = props.accounts;
                      console.log(props.electorSelections, props.votingList);
                      
                      let myVote = [];
                      props.electorSelections.listVotation.map((item, index)=> {
                        myVote.push(item.indexInList - 1);
                      })
                      try {
                        await contract.methods.emitVote(props.electorSelections.DNI, myVote).send({ from: accounts[0] });
                        let voteEncrypted = [];
                        props.votingList.map(async (intem,index)=> {
                          const element = await contract.methods.getVoteEncrypted(props.electorSelections.DNI, index).send({ from: accounts[0] });
                          voteEncrypted.push(element);
                        });
                        console.log("voteEncrypted:",voteEncrypted);
                        let elector = cookies.get("elector", { path: "/elector" });
                        elector.castVote = true;
                        
                        cookies.set("elector", elector, { path: "/elector" });
                        console.log(elector);

                      }catch(err){
                        console.log("err", err);
                      }
                      setLoading(false);
                    }else {
                      handleNext();
                    }
                  }}
                  style={{ float: "right" }}
                >
                  {activeStep === steps.length - 1 ? "Enviar voto" : "Siguiente"}
                </button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
}
  
const mapStateToProps = (state) => {
  return {
      politicalPartiesList: state.politicalPartiesList,
      electoralProcess: state.electoralProcess,
      votingList: state.votingList,
      electorSelections: state.electorSelections,
  }    
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleGeneralInformationSelection: (publicKey,DNI, lengthVotingList) => dispatch(handleGeneralInformationSelection(publicKey,DNI, lengthVotingList)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(VotationList);