import React, { Component } from 'react'

export default class VotoPreferencial extends Component {
    constructor (props){
        super();
        this.state= {
          data: [
            { id: 1,
              candidates :10,
              politicalPartie: "Partido político La Luz",
              image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMQEBUQDxIQFRUPFRAQEA8QFRAQDxUQFRUXFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGi0fHR0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAAECBAUGBwj/xAA8EAABAwMCBAQEBAUDAwUAAAABAAIRAwQhEjEFQVFhBhMicRQygZFCobHBB1LR4fBicoIzkvEVFiOi0v/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACIRAQEAAgICAgMBAQAAAAAAAAABAhESIQMxE2EiQVEEQv/aAAwDAQACEQMRAD8A9alPKqCspisqJZlKVX81P5qAPKeVW81LzkBZlPqVXzkvOQFrUlqVTzkvOQFvUlqVXzkvOQFqUpVbzkvOQFqUtSq+cn85AWZTalW81I1kBZ1JalUNZRNwmFvWmNRUnXKG66SC+aigaiznXaE+7SNpOrIZrrJfed0B973S5Q9VtOuVA3Swal+hHiHdLnD41uVLpVKl33WLW4h3VCrxDulfJDmDoXX2VB193XL1L/uhu4h3UXyq4OnN/wB0y5T/ANQ7pKflHB3rbxEbdrlad93R2Xq6ecY6rpfi0/xa574xN8alzg06H4tN8WueN4m+MT5wadH8Wm+LXO/GJjep8oWq6I3iXxndc2bwpfFlHKFquk+MSF4ua+MKk27KOUGq6T4xP8YueFyU/wASUcoeq6D4tL4xc/8AElL4oo5QadB8YmN4ue+JKY3RRyg03zeoTr1YD7ooL7sqb5IqY1vvv+6A/iPdc/UuiqtS5Kxy80azxuifxJCdf91zpuSom5Kwy8zSeNvOvlXqXqxzdIL7lZXzVpwa1S9VZ98sp9wq766n5KLjGpVvZ5qs+6Wc6shOrI50l6pcoFS7VJ9ZV6lZG6m1eN33TrK81JPdTt37AjMTNaiNCPmafGUlLKlCcJfMPjMElJJP5i+MycJ04Cr5i+MgFPSnaEQNT+YvjCDERjEVtNWaNuXGAJRPNaPjVRTUxSK6Ph1g1nzgE9NwtCo2mDOlk7yAJW0l1u1Go5ijwiq7Zh/5EN/VH/8Abtb/AEf9y6Wm5EcZVDTkK/Aa7c6J/wBpDvy3Wa+2fMaXT0gyvR2OTuCLPsPLa7C3cEe+FUqPXo/GuFsrtIIh34X9D36hcBxHhFem7SabzzBYC9pHWQsPJyjXDTNqVFVqVU1w4tJBBBGCDgqjVrLnuVadLLqygayourKBqpFtcdXQKlwqj6qBUrJaLktvroTq6pOqqPmKpim5LTqyG6qga1BzlUiLRX1UF71BzkNxVSJtTLkkKUyrSdvVwphQBUgVyV3bSSCScBIbOEk4TwgbMFIJoThNIjQjMCCxWKQQF22psDS+oYA6blRPGGhkUmkcpPzFZ3F6xbDT8oAnv9VXtnNe/Bx06coXTj11Bx/db1PiDgwOzsSYgR9SQrNnWqVCNpOZPILneMVgzy6IP/UcC/8A2j1QB3hWb7jwt6L3sD3Oa0nTTBc8nk2YIEnmtZe9IynW3Y2r2B2hzvVvCs1KfOeYXkHgvid5fXoL2uDRqdUIY+kxgEQAXO9ZMkbDbuvZxR9MLfix2gHhoyma8PEtMpr6yL6ZaDEgwRuD1Xl3h2lxS24kaVRhdRl/mOLgWFpnTUYYkchpJPPZFEei1ap1Fh6SPZUX3xaCHDb9OqqcT4yKVxQFQaRWFQerGRp/qU3E6hDtIG4MH/SVzZZNsY5nxrbNLm1WaRqBDmYBkcx1XIPYu28TEOpsB+YE/wDbH9lzBtlhe6KyzSUHUlq/DJjaoSxX0lXqUVvOtEF9onKLGC6ioeUtp1moGzT2nTH8tQcxbBtEJ1onKmxjuahkLWfaITrRPknTM0pLQ+FSRyLT0QFSCg0ojVjp07TaiAKDUQJaG0mtU9KZqIAjQ2hpS0o7Wp9CehsJrVYpBQDUWmEtDZcXs/OoGPmbkfRYvhajqeXOEaN+5C6eh06qlZ2ga40m71SS6OVMnP15fVa43va5fxsctUDq3EwTIaPUCf5YgQPZdtRtWl2lgHf+sLF4xaGjfeZyLYg4GAIAVujxRzZAbk887clt47re0Z/lJp1HCqLGuOgNEfMRAkqh4i8c0rUim0a6n8oIgD3StbkU6QJJ9WTjV9149/EHhZNd1ZpqkOJIaI0z32+/suqfbnr1Tgf8QRWqCnVYWFxhrp9PsV2NbThx57FfLfh2z1VWPeKgLXyH6howciN5wea+lbe4D7b/AGtG/sjL6Ecp/FG08ynbvYJcysWAjHzscZ+7B909Em4oUKxcRpBZVHduM/ZG8U0XOoMEy5tRj2xkgAOz+ajw6mKQfQqEAVx5lMnDS5wyBOx2XF5JuunC6jGv3eY+eQwPZVfIV19MgwcEbhR0rFKn5CXkK3pShLYUjbqDrZXy1RLUbDNNqoOtFplqiWJ7DKNooOs1r6E5pJ7LTBdZoL7NdCaKE+gjZcXOm0TrcNukgcVlgRmhDYjsCjY2doUwEg1S0pbPZ2o7EJoRaYRsbGaFLSotCmnszQpNCZTYEbCxRC1OCWbWEua3cySZJn3Kz7dslbwboZHM7rp8GG7v+M88utMTxXbB4DxuOa5RwLmOJJ9AnVnEbD3XYXnqBB2XL3p0SD8p3G/1Wvlx1eR+PLrTN4d4pY+l5byGVGahDjEgZOfofsuc8Rcapk6nannk1hnqZJyI2/JU/F/D9FVrmiA7TkbTJ/UOIW9ZcBbXptqUw2HcubXA+ph9tlrLbjKvx+KZXKfxynAeKtdUANJzclw0ukEkzmQvXeBeJ21dNKmwmTFUgfI0TjbJkLz6w4H5TiCMhxYJ7Ohd54OtDNWqNqjmta7k4MGnV9TJU5Wrz8Mxxlrqbewa5/q9QBkTKn4nsA6n5gBlnTaO4V60pQFcq0tbCw/iBCfCXGxy3Lt5k90mTud00I1zbljy07tJCYNXnVtKFpTaUbSolqnY2EQokIpCiQjZgkJQplqbSnKSEJKRaolGzMUNydxUUbG0U6UJ0bBwjUyq8otNyGW1lhUyUNpRWtRYeyCLTSFNEYxLQ2cFSlRIUUHsTUpNegppQNtnhjvUFq3NcbLG4Q+CewRhcanO7L0f88/Fln7PdvkYXJcccR+63rq4A2d/Rc5xh+qGtMudgdAObz7frCPJV4MG4qMrtfRMSBDajswY27ZQfDHHxw6p8PetdTpvk+cQ57fMxDiRyiRO+30u0OGQ46ciNP8A591Yq2ZPpqhr2gS0uHqAmNJ677rPDyTGXbecpZYld8co1bgMsnCvWcf/AI20wXUw52NTnbCM916D4a4aLeiyiSCWSXRkBxOoge0wuS8PBlH5GMpkggloAPQGV1PCq5HzTPXqrnkl9F5cssvf6dI0YVlizqL5V+gto5q4fjlDTcPHeR9cqmGrrvEvDNY85u7cOH+nquXDVwebC45VeN6AcoFHqU0PQsaoKEtCMKak2mjRq/lqJYrhpqLqaejUnNQXhX301XqMSJTc1MGo5amLUhoLSkpynQpTcVOmUVtvKXkEJdxkLTcrlJyptolWKbSjYW5UgUFrSiNYUbp6J5Vdz8qy6mqrmGVNJMPTgqDGFTc1VD1VuxqwTnkVatwNBzucrKpughaNF/oMdV6P+S7xsZ5zsOtTaN/7rHfbNJJGHHpOByC1qty1o9UE9s/RUbmm4DWBAP4TurzPEKg/QdL2zMDVjfkStWvw0VRLfqOo3/osSpegiCQIznsRzWvw+9IwOcFY5Sem0tVK1kGep2wg/XotPhl018iRg4joqniQ+pgmAROnudysU3Yp/wDTgkR9PdTJxov5PQaVcgYytGyqk/N9OQXGcEv6jo1zB2lsD7rrKExzHtBXRhltllNNKoQ5paeYIXA1sOI6ErrrysadJzhkgc1xL3yZWP8ApvosBSVGEI1FHzlx2tIshOCqwqKWtLahi9NrVY1E7XJ7GxnFVnohKEUhtHSn0qbWqZVSDav5aSMYSRobTo0wnNMShsekamcJ2I2uttxCXkgJ6VcKNaoiyHySYBKOxqqU3o7KiIXIc0wqtRgU6tTCpioZU5XsSrQphDexD8+FB11lPo+Q4t5Vig0NBmN+aDSuEO8rz+y7P83VqPJdrFOmC6QAYRq9B1QRAjnIQOHV+S37YAraiPPPENNlOc5PJaXhavrptJ5emfbYrJ/ijbeXVpv2B1R7iFiWXGzTaGMkl2A0dSs/+le46nxPfa7ltOkNRAgNGcq74e8NnUalbLnQTyHsieEbQMZrfmo/L3GCfaei6u3uGiEtS9nvU0cUGtbpAEItCoCIzhCrV2zEhK1qZ7LTFFV/EVWKQbzcfyXN6VqeI601AP5QFmMMrl813mMfQFQIOkq5UppNasOC9wKmxSqNwjsamqhHAcopKbQiPEITHpcdDawGKvVZlH8xAfUkqrINi0mYQ6zEahUwgXFwG5PsnqaJDQUkbzAnT4wbZ7A4iVF1QhWQ4huya3tS45UUcUbaoSfqtJ1OQhW1iKcgSfVOe6vuGMJyUrAqFKUT4cjdBZdaHQQrla5lsgJzRaRbQhR+GnKi6vAn6p2XeMJ/iFY20oHwUFXXXHOFSqXeowNuqnoaDqV2SGAyecKN7ho94QWUWtcS3PUp7uajHR0x9Fr4s9ZQrFnhj4In7krreGmf8hefcE4hqEO3YYM9RzXacKvR1XZfYnpzf8ZKB+HouA2qlpPYsJ/Zee+HaMVg9/0C954vwll7buo1I9Ylrty1/wCF49v6rw3iFB9vUdTcIfRcWOHcHl2WXk3O2mDv7a8aAAyB2T23ETqIPPnyIXnNPjTu8rb4fxBzKbq1Q4y1gg5eVjN1pdO0uLppcPVmJwSJWtwysdOZxledeGHPe+XnVzk7yvQKlUMokjciPutsb+2OTI4hXL3l3VCFSEDiFVzWt0BpLnsB1fyT6j9kqQnc7YXJbSaAfqT7Kh6mgxvuFNhed+YBRyC057tYgektdJ6OGRj2lFe4QqzZ+yenS1E525KpQFXPJQp0zhEpQXQeUj+6Df13ARTMdHD9lP2NLD6aqVHhro6oHD/MAOvPdQuRLhG6nlv9C+mpUMBVnt1COmUStho1YjOVTFxDo6/ontXZnsdKStF7TzSQOK1ViCAZHLt2VinUGkEf5yWdVI8wRicOBxBiY/IKXF7xrNDW7loMcj6nT7Zj7p7klo21q1VrQHbyApfENOJAjecBYlSo5w0EENAw7bmIH6oDnGu2G5Hp1tIgxuc7Hr9E+XYtdBXLRDiJGDP7oNa79YYxmoO5/hHVZzOIkt8toEGGhvvt+QJRuH1dOtrtmj0vMZbnP0IhLl39BauMxkRsQfsCE1OoAdEZ/KVmcReXM9E4AkfiDpdMddkJr6msA5aBG3zS2Zz9FO+xuNKtcCIOM6SE1R7R6Y2AJKw+Jte4+gmXeoMzIDZMg9Eri6eC1xyHhpdEEkBrfT7kk/ZPf9G9LlvcjU4Dnmeya0vmkEA9Z9lz1zeOYT6okERmSJwR9CqRt3u1+UXAtBLoOTnTEfb8leMJfdisQPxuxH5LseDAyBPZebNqVGDXUcdTcNaNw7+Y+wXpPhKkXAP2HM/suzlMqnH07q0wAF45/Fqg6jxEvDTprsY8O5FwGlw98fmvXmXLRjpGFzXi68bUDDTLXOb5mDB+WCHZU+Szicuq8jtbRz8imdwBIOZnPthG4jULi1jJ00pA6Fx3d/nRdDUvHl7WwXQXDMxqkiT901rZU6Ze/cCZGHOIzkDaJC58PJO9r3sTwo0Ne3Vzj6grseOVmtDWHoXGO0Y+xBXKVrIvDvJ1BhxgkfKAS5p3icfRXLUVIp+YHVNDS10mdQ5ARHq5HcQ0ct1807kK91fs6LaoyCNW08m8h+/uVmXlF7bhlKiRp1O81x3DdJDY/wCX6K4ajm6n0oDgHNAO4LdiQd5lB4e9z9b3gBzm0xI/A9vztP8AyEz0IUctjTSbUYG5yY/MKlVusdzt9EClBa4ukTmnjJHMEdzkIzbHDXNkFkEg5BPKO249ys75LehYercAMDuePYzt+6s2TQWPqg/JJztH9FXbaQ3TPyE78ugI9kJxdofTYAGNYWhwk6g50FpntP5JzKz2NJi5a/QWjL8O5RzKd9AOMbYJHuM/59Vm2r/MdqDCA3W4ASMeoDH027K7w17g3W8fixMQBj0k85H6KZ5NjpA62g7dY7Ko9xcadRrmwX6TO8Qdu8hazLQPJe47RLTtkE//AJVGtTaSJaBokADbI+bHufupyysP6WeIVhUDG7uyDH8oiZQalIEZkGd4xHJVzSaxzSXTAJJOwJPqPfl9lq2lyXAkfK2RBAJI3aZ/ZPHK2051WBcteHkNLYGMxM8/zlJanMn4cOlzyHEkSC4kbdkye/ouId/eg+txOIcO7gIH+dlWtm+fprHdktg7acn9SgXbmvYBzH5odqCG6ZgDollj+W0S1sVNLyA8emZABIgAyFSfexUcwSGtBDO0nEkKvUeZhivW7QRJaNXPunjv9nalb05hweXOZocOQDwSSY6bD6K1VuJdkxOqAciJUrJgYCXRlULqrLsc0spqC0W4voMA77nutGxqamyTvkjbIGAsmhYh7gX+6vABroGwwplvspv9i3dcZAbLnc+8Z9lGpbMqQ+puwuOnrq0yf/r+ak2J1FC82TBGFpbLFT7Uxw2k9znuG4IA7ncjooUuGsn0yI0hzpOQO3P+yuVIBwgVq2kYG6cuj3ArngtKWuAJDSDE57g+612X/lU9DBpnn26rMta5Jyi1aevnsr52ehuC07xzqoOokCPaRElQrV26nE5zuMKm0+W6CmrZ2U7tK0YNbh4EQZ5c8lN5gfUmNxpJ/X7oLNvUpW9UA4yi/wAKNSAxmhhEEzHbooWkguLhjkh+ZiUJ1d3JVqH2lSonUXAmSSjioKWWGCdz190A1oampjXuos/g9LFOH7/gPpjGP3VujdtBId0075Va2o6RBVG/cGnCmelel62fpJA2J55VuuGEEj8W45Yz+yxrO8E5Vx1y1E36EFs3gamuiHc9io1Kg1aDloEgHaVRBLjlSIMiUu9ahWLdOqCCBA5CP3Q3kNaZzP6pmtgodzUxnrCWUG6p3dkXwQcolq4thhKnReQZ5KvUBLtQOyUw/Za7ErWjtR0uxyyks2pxB0lJPgFdit0EkldTEbT51p0vmSSSFWahws5/zBJJPL0nJpWyVRJJZfpaVPZSbukkqwUFdrPqlJJaVCzaIvVMkrvoM28PrRKCSSIR7j5Ss21PqSSU32cbTdk1VJJNpPaDj6VY4ckkgX2uVisq7SSU08vSswbq0E6SScfY9PZTKSSeK6MxVbnknSRmKASn/CfZMkqnpE9ueq7lOkkkl//Z',
              enabled: false,
              candidatesSelected:["",""]
            },
            {
              id: 2,
              candidates: 5,
              politicalPartie: "Partido político La Luz",
              image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMQEBUQDxIQFRUPFRAQEA8QFRAQDxUQFRUXFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGi0fHR0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAAECBAUGBwj/xAA8EAABAwMCBAQEBAUDAwUAAAABAAIRAwQhEjEFQVFhBhMicRQygZFCobHBB1LR4fBicoIzkvEVFiOi0v/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACIRAQEAAgICAgMBAQAAAAAAAAABAhESIQMxE2EiQVEEQv/aAAwDAQACEQMRAD8A9alPKqCspisqJZlKVX81P5qAPKeVW81LzkBZlPqVXzkvOQFrUlqVTzkvOQFvUlqVXzkvOQFqUpVbzkvOQFqUtSq+cn85AWZTalW81I1kBZ1JalUNZRNwmFvWmNRUnXKG66SC+aigaiznXaE+7SNpOrIZrrJfed0B973S5Q9VtOuVA3Swal+hHiHdLnD41uVLpVKl33WLW4h3VCrxDulfJDmDoXX2VB193XL1L/uhu4h3UXyq4OnN/wB0y5T/ANQ7pKflHB3rbxEbdrlad93R2Xq6ecY6rpfi0/xa574xN8alzg06H4tN8WueN4m+MT5wadH8Wm+LXO/GJjep8oWq6I3iXxndc2bwpfFlHKFquk+MSF4ua+MKk27KOUGq6T4xP8YueFyU/wASUcoeq6D4tL4xc/8AElL4oo5QadB8YmN4ue+JKY3RRyg03zeoTr1YD7ooL7sqb5IqY1vvv+6A/iPdc/UuiqtS5Kxy80azxuifxJCdf91zpuSom5Kwy8zSeNvOvlXqXqxzdIL7lZXzVpwa1S9VZ98sp9wq766n5KLjGpVvZ5qs+6Wc6shOrI50l6pcoFS7VJ9ZV6lZG6m1eN33TrK81JPdTt37AjMTNaiNCPmafGUlLKlCcJfMPjMElJJP5i+MycJ04Cr5i+MgFPSnaEQNT+YvjCDERjEVtNWaNuXGAJRPNaPjVRTUxSK6Ph1g1nzgE9NwtCo2mDOlk7yAJW0l1u1Go5ijwiq7Zh/5EN/VH/8Abtb/AEf9y6Wm5EcZVDTkK/Aa7c6J/wBpDvy3Wa+2fMaXT0gyvR2OTuCLPsPLa7C3cEe+FUqPXo/GuFsrtIIh34X9D36hcBxHhFem7SabzzBYC9pHWQsPJyjXDTNqVFVqVU1w4tJBBBGCDgqjVrLnuVadLLqygayourKBqpFtcdXQKlwqj6qBUrJaLktvroTq6pOqqPmKpim5LTqyG6qga1BzlUiLRX1UF71BzkNxVSJtTLkkKUyrSdvVwphQBUgVyV3bSSCScBIbOEk4TwgbMFIJoThNIjQjMCCxWKQQF22psDS+oYA6blRPGGhkUmkcpPzFZ3F6xbDT8oAnv9VXtnNe/Bx06coXTj11Bx/db1PiDgwOzsSYgR9SQrNnWqVCNpOZPILneMVgzy6IP/UcC/8A2j1QB3hWb7jwt6L3sD3Oa0nTTBc8nk2YIEnmtZe9IynW3Y2r2B2hzvVvCs1KfOeYXkHgvid5fXoL2uDRqdUIY+kxgEQAXO9ZMkbDbuvZxR9MLfix2gHhoyma8PEtMpr6yL6ZaDEgwRuD1Xl3h2lxS24kaVRhdRl/mOLgWFpnTUYYkchpJPPZFEei1ap1Fh6SPZUX3xaCHDb9OqqcT4yKVxQFQaRWFQerGRp/qU3E6hDtIG4MH/SVzZZNsY5nxrbNLm1WaRqBDmYBkcx1XIPYu28TEOpsB+YE/wDbH9lzBtlhe6KyzSUHUlq/DJjaoSxX0lXqUVvOtEF9onKLGC6ioeUtp1moGzT2nTH8tQcxbBtEJ1onKmxjuahkLWfaITrRPknTM0pLQ+FSRyLT0QFSCg0ojVjp07TaiAKDUQJaG0mtU9KZqIAjQ2hpS0o7Wp9CehsJrVYpBQDUWmEtDZcXs/OoGPmbkfRYvhajqeXOEaN+5C6eh06qlZ2ga40m71SS6OVMnP15fVa43va5fxsctUDq3EwTIaPUCf5YgQPZdtRtWl2lgHf+sLF4xaGjfeZyLYg4GAIAVujxRzZAbk887clt47re0Z/lJp1HCqLGuOgNEfMRAkqh4i8c0rUim0a6n8oIgD3StbkU6QJJ9WTjV9149/EHhZNd1ZpqkOJIaI0z32+/suqfbnr1Tgf8QRWqCnVYWFxhrp9PsV2NbThx57FfLfh2z1VWPeKgLXyH6howciN5wea+lbe4D7b/AGtG/sjL6Ecp/FG08ynbvYJcysWAjHzscZ+7B909Em4oUKxcRpBZVHduM/ZG8U0XOoMEy5tRj2xkgAOz+ajw6mKQfQqEAVx5lMnDS5wyBOx2XF5JuunC6jGv3eY+eQwPZVfIV19MgwcEbhR0rFKn5CXkK3pShLYUjbqDrZXy1RLUbDNNqoOtFplqiWJ7DKNooOs1r6E5pJ7LTBdZoL7NdCaKE+gjZcXOm0TrcNukgcVlgRmhDYjsCjY2doUwEg1S0pbPZ2o7EJoRaYRsbGaFLSotCmnszQpNCZTYEbCxRC1OCWbWEua3cySZJn3Kz7dslbwboZHM7rp8GG7v+M88utMTxXbB4DxuOa5RwLmOJJ9AnVnEbD3XYXnqBB2XL3p0SD8p3G/1Wvlx1eR+PLrTN4d4pY+l5byGVGahDjEgZOfofsuc8Rcapk6nannk1hnqZJyI2/JU/F/D9FVrmiA7TkbTJ/UOIW9ZcBbXptqUw2HcubXA+ph9tlrLbjKvx+KZXKfxynAeKtdUANJzclw0ukEkzmQvXeBeJ21dNKmwmTFUgfI0TjbJkLz6w4H5TiCMhxYJ7Ohd54OtDNWqNqjmta7k4MGnV9TJU5Wrz8Mxxlrqbewa5/q9QBkTKn4nsA6n5gBlnTaO4V60pQFcq0tbCw/iBCfCXGxy3Lt5k90mTud00I1zbljy07tJCYNXnVtKFpTaUbSolqnY2EQokIpCiQjZgkJQplqbSnKSEJKRaolGzMUNydxUUbG0U6UJ0bBwjUyq8otNyGW1lhUyUNpRWtRYeyCLTSFNEYxLQ2cFSlRIUUHsTUpNegppQNtnhjvUFq3NcbLG4Q+CewRhcanO7L0f88/Fln7PdvkYXJcccR+63rq4A2d/Rc5xh+qGtMudgdAObz7frCPJV4MG4qMrtfRMSBDajswY27ZQfDHHxw6p8PetdTpvk+cQ57fMxDiRyiRO+30u0OGQ46ciNP8A591Yq2ZPpqhr2gS0uHqAmNJ677rPDyTGXbecpZYld8co1bgMsnCvWcf/AI20wXUw52NTnbCM916D4a4aLeiyiSCWSXRkBxOoge0wuS8PBlH5GMpkggloAPQGV1PCq5HzTPXqrnkl9F5cssvf6dI0YVlizqL5V+gto5q4fjlDTcPHeR9cqmGrrvEvDNY85u7cOH+nquXDVwebC45VeN6AcoFHqU0PQsaoKEtCMKak2mjRq/lqJYrhpqLqaejUnNQXhX301XqMSJTc1MGo5amLUhoLSkpynQpTcVOmUVtvKXkEJdxkLTcrlJyptolWKbSjYW5UgUFrSiNYUbp6J5Vdz8qy6mqrmGVNJMPTgqDGFTc1VD1VuxqwTnkVatwNBzucrKpughaNF/oMdV6P+S7xsZ5zsOtTaN/7rHfbNJJGHHpOByC1qty1o9UE9s/RUbmm4DWBAP4TurzPEKg/QdL2zMDVjfkStWvw0VRLfqOo3/osSpegiCQIznsRzWvw+9IwOcFY5Sem0tVK1kGep2wg/XotPhl018iRg4joqniQ+pgmAROnudysU3Yp/wDTgkR9PdTJxov5PQaVcgYytGyqk/N9OQXGcEv6jo1zB2lsD7rrKExzHtBXRhltllNNKoQ5paeYIXA1sOI6ErrrysadJzhkgc1xL3yZWP8ApvosBSVGEI1FHzlx2tIshOCqwqKWtLahi9NrVY1E7XJ7GxnFVnohKEUhtHSn0qbWqZVSDav5aSMYSRobTo0wnNMShsekamcJ2I2uttxCXkgJ6VcKNaoiyHySYBKOxqqU3o7KiIXIc0wqtRgU6tTCpioZU5XsSrQphDexD8+FB11lPo+Q4t5Vig0NBmN+aDSuEO8rz+y7P83VqPJdrFOmC6QAYRq9B1QRAjnIQOHV+S37YAraiPPPENNlOc5PJaXhavrptJ5emfbYrJ/ijbeXVpv2B1R7iFiWXGzTaGMkl2A0dSs/+le46nxPfa7ltOkNRAgNGcq74e8NnUalbLnQTyHsieEbQMZrfmo/L3GCfaei6u3uGiEtS9nvU0cUGtbpAEItCoCIzhCrV2zEhK1qZ7LTFFV/EVWKQbzcfyXN6VqeI601AP5QFmMMrl813mMfQFQIOkq5UppNasOC9wKmxSqNwjsamqhHAcopKbQiPEITHpcdDawGKvVZlH8xAfUkqrINi0mYQ6zEahUwgXFwG5PsnqaJDQUkbzAnT4wbZ7A4iVF1QhWQ4huya3tS45UUcUbaoSfqtJ1OQhW1iKcgSfVOe6vuGMJyUrAqFKUT4cjdBZdaHQQrla5lsgJzRaRbQhR+GnKi6vAn6p2XeMJ/iFY20oHwUFXXXHOFSqXeowNuqnoaDqV2SGAyecKN7ho94QWUWtcS3PUp7uajHR0x9Fr4s9ZQrFnhj4In7krreGmf8hefcE4hqEO3YYM9RzXacKvR1XZfYnpzf8ZKB+HouA2qlpPYsJ/Zee+HaMVg9/0C954vwll7buo1I9Ylrty1/wCF49v6rw3iFB9vUdTcIfRcWOHcHl2WXk3O2mDv7a8aAAyB2T23ETqIPPnyIXnNPjTu8rb4fxBzKbq1Q4y1gg5eVjN1pdO0uLppcPVmJwSJWtwysdOZxledeGHPe+XnVzk7yvQKlUMokjciPutsb+2OTI4hXL3l3VCFSEDiFVzWt0BpLnsB1fyT6j9kqQnc7YXJbSaAfqT7Kh6mgxvuFNhed+YBRyC057tYgektdJ6OGRj2lFe4QqzZ+yenS1E525KpQFXPJQp0zhEpQXQeUj+6Df13ARTMdHD9lP2NLD6aqVHhro6oHD/MAOvPdQuRLhG6nlv9C+mpUMBVnt1COmUStho1YjOVTFxDo6/ontXZnsdKStF7TzSQOK1ViCAZHLt2VinUGkEf5yWdVI8wRicOBxBiY/IKXF7xrNDW7loMcj6nT7Zj7p7klo21q1VrQHbyApfENOJAjecBYlSo5w0EENAw7bmIH6oDnGu2G5Hp1tIgxuc7Hr9E+XYtdBXLRDiJGDP7oNa79YYxmoO5/hHVZzOIkt8toEGGhvvt+QJRuH1dOtrtmj0vMZbnP0IhLl39BauMxkRsQfsCE1OoAdEZ/KVmcReXM9E4AkfiDpdMddkJr6msA5aBG3zS2Zz9FO+xuNKtcCIOM6SE1R7R6Y2AJKw+Jte4+gmXeoMzIDZMg9Eri6eC1xyHhpdEEkBrfT7kk/ZPf9G9LlvcjU4Dnmeya0vmkEA9Z9lz1zeOYT6okERmSJwR9CqRt3u1+UXAtBLoOTnTEfb8leMJfdisQPxuxH5LseDAyBPZebNqVGDXUcdTcNaNw7+Y+wXpPhKkXAP2HM/suzlMqnH07q0wAF45/Fqg6jxEvDTprsY8O5FwGlw98fmvXmXLRjpGFzXi68bUDDTLXOb5mDB+WCHZU+Szicuq8jtbRz8imdwBIOZnPthG4jULi1jJ00pA6Fx3d/nRdDUvHl7WwXQXDMxqkiT901rZU6Ze/cCZGHOIzkDaJC58PJO9r3sTwo0Ne3Vzj6grseOVmtDWHoXGO0Y+xBXKVrIvDvJ1BhxgkfKAS5p3icfRXLUVIp+YHVNDS10mdQ5ARHq5HcQ0ct1807kK91fs6LaoyCNW08m8h+/uVmXlF7bhlKiRp1O81x3DdJDY/wCX6K4ajm6n0oDgHNAO4LdiQd5lB4e9z9b3gBzm0xI/A9vztP8AyEz0IUctjTSbUYG5yY/MKlVusdzt9EClBa4ukTmnjJHMEdzkIzbHDXNkFkEg5BPKO249ys75LehYercAMDuePYzt+6s2TQWPqg/JJztH9FXbaQ3TPyE78ugI9kJxdofTYAGNYWhwk6g50FpntP5JzKz2NJi5a/QWjL8O5RzKd9AOMbYJHuM/59Vm2r/MdqDCA3W4ASMeoDH027K7w17g3W8fixMQBj0k85H6KZ5NjpA62g7dY7Ko9xcadRrmwX6TO8Qdu8hazLQPJe47RLTtkE//AJVGtTaSJaBokADbI+bHufupyysP6WeIVhUDG7uyDH8oiZQalIEZkGd4xHJVzSaxzSXTAJJOwJPqPfl9lq2lyXAkfK2RBAJI3aZ/ZPHK2051WBcteHkNLYGMxM8/zlJanMn4cOlzyHEkSC4kbdkye/ouId/eg+txOIcO7gIH+dlWtm+fprHdktg7acn9SgXbmvYBzH5odqCG6ZgDollj+W0S1sVNLyA8emZABIgAyFSfexUcwSGtBDO0nEkKvUeZhivW7QRJaNXPunjv9nalb05hweXOZocOQDwSSY6bD6K1VuJdkxOqAciJUrJgYCXRlULqrLsc0spqC0W4voMA77nutGxqamyTvkjbIGAsmhYh7gX+6vABroGwwplvspv9i3dcZAbLnc+8Z9lGpbMqQ+puwuOnrq0yf/r+ak2J1FC82TBGFpbLFT7Uxw2k9znuG4IA7ncjooUuGsn0yI0hzpOQO3P+yuVIBwgVq2kYG6cuj3ArngtKWuAJDSDE57g+612X/lU9DBpnn26rMta5Jyi1aevnsr52ehuC07xzqoOokCPaRElQrV26nE5zuMKm0+W6CmrZ2U7tK0YNbh4EQZ5c8lN5gfUmNxpJ/X7oLNvUpW9UA4yi/wAKNSAxmhhEEzHbooWkguLhjkh+ZiUJ1d3JVqH2lSonUXAmSSjioKWWGCdz190A1oampjXuos/g9LFOH7/gPpjGP3VujdtBId0075Va2o6RBVG/cGnCmelel62fpJA2J55VuuGEEj8W45Yz+yxrO8E5Vx1y1E36EFs3gamuiHc9io1Kg1aDloEgHaVRBLjlSIMiUu9ahWLdOqCCBA5CP3Q3kNaZzP6pmtgodzUxnrCWUG6p3dkXwQcolq4thhKnReQZ5KvUBLtQOyUw/Za7ErWjtR0uxyyks2pxB0lJPgFdit0EkldTEbT51p0vmSSSFWahws5/zBJJPL0nJpWyVRJJZfpaVPZSbukkqwUFdrPqlJJaVCzaIvVMkrvoM28PrRKCSSIR7j5Ss21PqSSU32cbTdk1VJJNpPaDj6VY4ckkgX2uVisq7SSU08vSswbq0E6SScfY9PZTKSSeK6MxVbnknSRmKASn/CfZMkqnpE9ueq7lOkkkl//Z',
              enable:false,
              candidatesSelected:["",""]
            },
            {
              id:3,
              candidates: 6,
              politicalPartie: "Partido político La Luz",
              image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMQEBUQDxIQFRUPFRAQEA8QFRAQDxUQFRUXFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGi0fHR0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAAECBAUGBwj/xAA8EAABAwMCBAQEBAUDAwUAAAABAAIRAwQhEjEFQVFhBhMicRQygZFCobHBB1LR4fBicoIzkvEVFiOi0v/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACIRAQEAAgICAgMBAQAAAAAAAAABAhESIQMxE2EiQVEEQv/aAAwDAQACEQMRAD8A9alPKqCspisqJZlKVX81P5qAPKeVW81LzkBZlPqVXzkvOQFrUlqVTzkvOQFvUlqVXzkvOQFqUpVbzkvOQFqUtSq+cn85AWZTalW81I1kBZ1JalUNZRNwmFvWmNRUnXKG66SC+aigaiznXaE+7SNpOrIZrrJfed0B973S5Q9VtOuVA3Swal+hHiHdLnD41uVLpVKl33WLW4h3VCrxDulfJDmDoXX2VB193XL1L/uhu4h3UXyq4OnN/wB0y5T/ANQ7pKflHB3rbxEbdrlad93R2Xq6ecY6rpfi0/xa574xN8alzg06H4tN8WueN4m+MT5wadH8Wm+LXO/GJjep8oWq6I3iXxndc2bwpfFlHKFquk+MSF4ua+MKk27KOUGq6T4xP8YueFyU/wASUcoeq6D4tL4xc/8AElL4oo5QadB8YmN4ue+JKY3RRyg03zeoTr1YD7ooL7sqb5IqY1vvv+6A/iPdc/UuiqtS5Kxy80azxuifxJCdf91zpuSom5Kwy8zSeNvOvlXqXqxzdIL7lZXzVpwa1S9VZ98sp9wq766n5KLjGpVvZ5qs+6Wc6shOrI50l6pcoFS7VJ9ZV6lZG6m1eN33TrK81JPdTt37AjMTNaiNCPmafGUlLKlCcJfMPjMElJJP5i+MycJ04Cr5i+MgFPSnaEQNT+YvjCDERjEVtNWaNuXGAJRPNaPjVRTUxSK6Ph1g1nzgE9NwtCo2mDOlk7yAJW0l1u1Go5ijwiq7Zh/5EN/VH/8Abtb/AEf9y6Wm5EcZVDTkK/Aa7c6J/wBpDvy3Wa+2fMaXT0gyvR2OTuCLPsPLa7C3cEe+FUqPXo/GuFsrtIIh34X9D36hcBxHhFem7SabzzBYC9pHWQsPJyjXDTNqVFVqVU1w4tJBBBGCDgqjVrLnuVadLLqygayourKBqpFtcdXQKlwqj6qBUrJaLktvroTq6pOqqPmKpim5LTqyG6qga1BzlUiLRX1UF71BzkNxVSJtTLkkKUyrSdvVwphQBUgVyV3bSSCScBIbOEk4TwgbMFIJoThNIjQjMCCxWKQQF22psDS+oYA6blRPGGhkUmkcpPzFZ3F6xbDT8oAnv9VXtnNe/Bx06coXTj11Bx/db1PiDgwOzsSYgR9SQrNnWqVCNpOZPILneMVgzy6IP/UcC/8A2j1QB3hWb7jwt6L3sD3Oa0nTTBc8nk2YIEnmtZe9IynW3Y2r2B2hzvVvCs1KfOeYXkHgvid5fXoL2uDRqdUIY+kxgEQAXO9ZMkbDbuvZxR9MLfix2gHhoyma8PEtMpr6yL6ZaDEgwRuD1Xl3h2lxS24kaVRhdRl/mOLgWFpnTUYYkchpJPPZFEei1ap1Fh6SPZUX3xaCHDb9OqqcT4yKVxQFQaRWFQerGRp/qU3E6hDtIG4MH/SVzZZNsY5nxrbNLm1WaRqBDmYBkcx1XIPYu28TEOpsB+YE/wDbH9lzBtlhe6KyzSUHUlq/DJjaoSxX0lXqUVvOtEF9onKLGC6ioeUtp1moGzT2nTH8tQcxbBtEJ1onKmxjuahkLWfaITrRPknTM0pLQ+FSRyLT0QFSCg0ojVjp07TaiAKDUQJaG0mtU9KZqIAjQ2hpS0o7Wp9CehsJrVYpBQDUWmEtDZcXs/OoGPmbkfRYvhajqeXOEaN+5C6eh06qlZ2ga40m71SS6OVMnP15fVa43va5fxsctUDq3EwTIaPUCf5YgQPZdtRtWl2lgHf+sLF4xaGjfeZyLYg4GAIAVujxRzZAbk887clt47re0Z/lJp1HCqLGuOgNEfMRAkqh4i8c0rUim0a6n8oIgD3StbkU6QJJ9WTjV9149/EHhZNd1ZpqkOJIaI0z32+/suqfbnr1Tgf8QRWqCnVYWFxhrp9PsV2NbThx57FfLfh2z1VWPeKgLXyH6howciN5wea+lbe4D7b/AGtG/sjL6Ecp/FG08ynbvYJcysWAjHzscZ+7B909Em4oUKxcRpBZVHduM/ZG8U0XOoMEy5tRj2xkgAOz+ajw6mKQfQqEAVx5lMnDS5wyBOx2XF5JuunC6jGv3eY+eQwPZVfIV19MgwcEbhR0rFKn5CXkK3pShLYUjbqDrZXy1RLUbDNNqoOtFplqiWJ7DKNooOs1r6E5pJ7LTBdZoL7NdCaKE+gjZcXOm0TrcNukgcVlgRmhDYjsCjY2doUwEg1S0pbPZ2o7EJoRaYRsbGaFLSotCmnszQpNCZTYEbCxRC1OCWbWEua3cySZJn3Kz7dslbwboZHM7rp8GG7v+M88utMTxXbB4DxuOa5RwLmOJJ9AnVnEbD3XYXnqBB2XL3p0SD8p3G/1Wvlx1eR+PLrTN4d4pY+l5byGVGahDjEgZOfofsuc8Rcapk6nannk1hnqZJyI2/JU/F/D9FVrmiA7TkbTJ/UOIW9ZcBbXptqUw2HcubXA+ph9tlrLbjKvx+KZXKfxynAeKtdUANJzclw0ukEkzmQvXeBeJ21dNKmwmTFUgfI0TjbJkLz6w4H5TiCMhxYJ7Ohd54OtDNWqNqjmta7k4MGnV9TJU5Wrz8Mxxlrqbewa5/q9QBkTKn4nsA6n5gBlnTaO4V60pQFcq0tbCw/iBCfCXGxy3Lt5k90mTud00I1zbljy07tJCYNXnVtKFpTaUbSolqnY2EQokIpCiQjZgkJQplqbSnKSEJKRaolGzMUNydxUUbG0U6UJ0bBwjUyq8otNyGW1lhUyUNpRWtRYeyCLTSFNEYxLQ2cFSlRIUUHsTUpNegppQNtnhjvUFq3NcbLG4Q+CewRhcanO7L0f88/Fln7PdvkYXJcccR+63rq4A2d/Rc5xh+qGtMudgdAObz7frCPJV4MG4qMrtfRMSBDajswY27ZQfDHHxw6p8PetdTpvk+cQ57fMxDiRyiRO+30u0OGQ46ciNP8A591Yq2ZPpqhr2gS0uHqAmNJ677rPDyTGXbecpZYld8co1bgMsnCvWcf/AI20wXUw52NTnbCM916D4a4aLeiyiSCWSXRkBxOoge0wuS8PBlH5GMpkggloAPQGV1PCq5HzTPXqrnkl9F5cssvf6dI0YVlizqL5V+gto5q4fjlDTcPHeR9cqmGrrvEvDNY85u7cOH+nquXDVwebC45VeN6AcoFHqU0PQsaoKEtCMKak2mjRq/lqJYrhpqLqaejUnNQXhX301XqMSJTc1MGo5amLUhoLSkpynQpTcVOmUVtvKXkEJdxkLTcrlJyptolWKbSjYW5UgUFrSiNYUbp6J5Vdz8qy6mqrmGVNJMPTgqDGFTc1VD1VuxqwTnkVatwNBzucrKpughaNF/oMdV6P+S7xsZ5zsOtTaN/7rHfbNJJGHHpOByC1qty1o9UE9s/RUbmm4DWBAP4TurzPEKg/QdL2zMDVjfkStWvw0VRLfqOo3/osSpegiCQIznsRzWvw+9IwOcFY5Sem0tVK1kGep2wg/XotPhl018iRg4joqniQ+pgmAROnudysU3Yp/wDTgkR9PdTJxov5PQaVcgYytGyqk/N9OQXGcEv6jo1zB2lsD7rrKExzHtBXRhltllNNKoQ5paeYIXA1sOI6ErrrysadJzhkgc1xL3yZWP8ApvosBSVGEI1FHzlx2tIshOCqwqKWtLahi9NrVY1E7XJ7GxnFVnohKEUhtHSn0qbWqZVSDav5aSMYSRobTo0wnNMShsekamcJ2I2uttxCXkgJ6VcKNaoiyHySYBKOxqqU3o7KiIXIc0wqtRgU6tTCpioZU5XsSrQphDexD8+FB11lPo+Q4t5Vig0NBmN+aDSuEO8rz+y7P83VqPJdrFOmC6QAYRq9B1QRAjnIQOHV+S37YAraiPPPENNlOc5PJaXhavrptJ5emfbYrJ/ijbeXVpv2B1R7iFiWXGzTaGMkl2A0dSs/+le46nxPfa7ltOkNRAgNGcq74e8NnUalbLnQTyHsieEbQMZrfmo/L3GCfaei6u3uGiEtS9nvU0cUGtbpAEItCoCIzhCrV2zEhK1qZ7LTFFV/EVWKQbzcfyXN6VqeI601AP5QFmMMrl813mMfQFQIOkq5UppNasOC9wKmxSqNwjsamqhHAcopKbQiPEITHpcdDawGKvVZlH8xAfUkqrINi0mYQ6zEahUwgXFwG5PsnqaJDQUkbzAnT4wbZ7A4iVF1QhWQ4huya3tS45UUcUbaoSfqtJ1OQhW1iKcgSfVOe6vuGMJyUrAqFKUT4cjdBZdaHQQrla5lsgJzRaRbQhR+GnKi6vAn6p2XeMJ/iFY20oHwUFXXXHOFSqXeowNuqnoaDqV2SGAyecKN7ho94QWUWtcS3PUp7uajHR0x9Fr4s9ZQrFnhj4In7krreGmf8hefcE4hqEO3YYM9RzXacKvR1XZfYnpzf8ZKB+HouA2qlpPYsJ/Zee+HaMVg9/0C954vwll7buo1I9Ylrty1/wCF49v6rw3iFB9vUdTcIfRcWOHcHl2WXk3O2mDv7a8aAAyB2T23ETqIPPnyIXnNPjTu8rb4fxBzKbq1Q4y1gg5eVjN1pdO0uLppcPVmJwSJWtwysdOZxledeGHPe+XnVzk7yvQKlUMokjciPutsb+2OTI4hXL3l3VCFSEDiFVzWt0BpLnsB1fyT6j9kqQnc7YXJbSaAfqT7Kh6mgxvuFNhed+YBRyC057tYgektdJ6OGRj2lFe4QqzZ+yenS1E525KpQFXPJQp0zhEpQXQeUj+6Df13ARTMdHD9lP2NLD6aqVHhro6oHD/MAOvPdQuRLhG6nlv9C+mpUMBVnt1COmUStho1YjOVTFxDo6/ontXZnsdKStF7TzSQOK1ViCAZHLt2VinUGkEf5yWdVI8wRicOBxBiY/IKXF7xrNDW7loMcj6nT7Zj7p7klo21q1VrQHbyApfENOJAjecBYlSo5w0EENAw7bmIH6oDnGu2G5Hp1tIgxuc7Hr9E+XYtdBXLRDiJGDP7oNa79YYxmoO5/hHVZzOIkt8toEGGhvvt+QJRuH1dOtrtmj0vMZbnP0IhLl39BauMxkRsQfsCE1OoAdEZ/KVmcReXM9E4AkfiDpdMddkJr6msA5aBG3zS2Zz9FO+xuNKtcCIOM6SE1R7R6Y2AJKw+Jte4+gmXeoMzIDZMg9Eri6eC1xyHhpdEEkBrfT7kk/ZPf9G9LlvcjU4Dnmeya0vmkEA9Z9lz1zeOYT6okERmSJwR9CqRt3u1+UXAtBLoOTnTEfb8leMJfdisQPxuxH5LseDAyBPZebNqVGDXUcdTcNaNw7+Y+wXpPhKkXAP2HM/suzlMqnH07q0wAF45/Fqg6jxEvDTprsY8O5FwGlw98fmvXmXLRjpGFzXi68bUDDTLXOb5mDB+WCHZU+Szicuq8jtbRz8imdwBIOZnPthG4jULi1jJ00pA6Fx3d/nRdDUvHl7WwXQXDMxqkiT901rZU6Ze/cCZGHOIzkDaJC58PJO9r3sTwo0Ne3Vzj6grseOVmtDWHoXGO0Y+xBXKVrIvDvJ1BhxgkfKAS5p3icfRXLUVIp+YHVNDS10mdQ5ARHq5HcQ0ct1807kK91fs6LaoyCNW08m8h+/uVmXlF7bhlKiRp1O81x3DdJDY/wCX6K4ajm6n0oDgHNAO4LdiQd5lB4e9z9b3gBzm0xI/A9vztP8AyEz0IUctjTSbUYG5yY/MKlVusdzt9EClBa4ukTmnjJHMEdzkIzbHDXNkFkEg5BPKO249ys75LehYercAMDuePYzt+6s2TQWPqg/JJztH9FXbaQ3TPyE78ugI9kJxdofTYAGNYWhwk6g50FpntP5JzKz2NJi5a/QWjL8O5RzKd9AOMbYJHuM/59Vm2r/MdqDCA3W4ASMeoDH027K7w17g3W8fixMQBj0k85H6KZ5NjpA62g7dY7Ko9xcadRrmwX6TO8Qdu8hazLQPJe47RLTtkE//AJVGtTaSJaBokADbI+bHufupyysP6WeIVhUDG7uyDH8oiZQalIEZkGd4xHJVzSaxzSXTAJJOwJPqPfl9lq2lyXAkfK2RBAJI3aZ/ZPHK2051WBcteHkNLYGMxM8/zlJanMn4cOlzyHEkSC4kbdkye/ouId/eg+txOIcO7gIH+dlWtm+fprHdktg7acn9SgXbmvYBzH5odqCG6ZgDollj+W0S1sVNLyA8emZABIgAyFSfexUcwSGtBDO0nEkKvUeZhivW7QRJaNXPunjv9nalb05hweXOZocOQDwSSY6bD6K1VuJdkxOqAciJUrJgYCXRlULqrLsc0spqC0W4voMA77nutGxqamyTvkjbIGAsmhYh7gX+6vABroGwwplvspv9i3dcZAbLnc+8Z9lGpbMqQ+puwuOnrq0yf/r+ak2J1FC82TBGFpbLFT7Uxw2k9znuG4IA7ncjooUuGsn0yI0hzpOQO3P+yuVIBwgVq2kYG6cuj3ArngtKWuAJDSDE57g+612X/lU9DBpnn26rMta5Jyi1aevnsr52ehuC07xzqoOokCPaRElQrV26nE5zuMKm0+W6CmrZ2U7tK0YNbh4EQZ5c8lN5gfUmNxpJ/X7oLNvUpW9UA4yi/wAKNSAxmhhEEzHbooWkguLhjkh+ZiUJ1d3JVqH2lSonUXAmSSjioKWWGCdz190A1oampjXuos/g9LFOH7/gPpjGP3VujdtBId0075Va2o6RBVG/cGnCmelel62fpJA2J55VuuGEEj8W45Yz+yxrO8E5Vx1y1E36EFs3gamuiHc9io1Kg1aDloEgHaVRBLjlSIMiUu9ahWLdOqCCBA5CP3Q3kNaZzP6pmtgodzUxnrCWUG6p3dkXwQcolq4thhKnReQZ5KvUBLtQOyUw/Za7ErWjtR0uxyyks2pxB0lJPgFdit0EkldTEbT51p0vmSSSFWahws5/zBJJPL0nJpWyVRJJZfpaVPZSbukkqwUFdrPqlJJaVCzaIvVMkrvoM28PrRKCSSIR7j5Ss21PqSSU32cbTdk1VJJNpPaDj6VY4ckkgX2uVisq7SSU08vSswbq0E6SScfY9PZTKSSeK6MxVbnknSRmKASn/CfZMkqnpE9ueq7lOkkkl//Z',
              enable:false,
              candidatesSelected:["",""]
            },
            {
              id:4,
              candidates: 4,
              politicalPartie: "Partido político La Luz",
              image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMQEBUQDxIQFRUPFRAQEA8QFRAQDxUQFRUXFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGi0fHR0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAAECBAUGBwj/xAA8EAABAwMCBAQEBAUDAwUAAAABAAIRAwQhEjEFQVFhBhMicRQygZFCobHBB1LR4fBicoIzkvEVFiOi0v/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACIRAQEAAgICAgMBAQAAAAAAAAABAhESIQMxE2EiQVEEQv/aAAwDAQACEQMRAD8A9alPKqCspisqJZlKVX81P5qAPKeVW81LzkBZlPqVXzkvOQFrUlqVTzkvOQFvUlqVXzkvOQFqUpVbzkvOQFqUtSq+cn85AWZTalW81I1kBZ1JalUNZRNwmFvWmNRUnXKG66SC+aigaiznXaE+7SNpOrIZrrJfed0B973S5Q9VtOuVA3Swal+hHiHdLnD41uVLpVKl33WLW4h3VCrxDulfJDmDoXX2VB193XL1L/uhu4h3UXyq4OnN/wB0y5T/ANQ7pKflHB3rbxEbdrlad93R2Xq6ecY6rpfi0/xa574xN8alzg06H4tN8WueN4m+MT5wadH8Wm+LXO/GJjep8oWq6I3iXxndc2bwpfFlHKFquk+MSF4ua+MKk27KOUGq6T4xP8YueFyU/wASUcoeq6D4tL4xc/8AElL4oo5QadB8YmN4ue+JKY3RRyg03zeoTr1YD7ooL7sqb5IqY1vvv+6A/iPdc/UuiqtS5Kxy80azxuifxJCdf91zpuSom5Kwy8zSeNvOvlXqXqxzdIL7lZXzVpwa1S9VZ98sp9wq766n5KLjGpVvZ5qs+6Wc6shOrI50l6pcoFS7VJ9ZV6lZG6m1eN33TrK81JPdTt37AjMTNaiNCPmafGUlLKlCcJfMPjMElJJP5i+MycJ04Cr5i+MgFPSnaEQNT+YvjCDERjEVtNWaNuXGAJRPNaPjVRTUxSK6Ph1g1nzgE9NwtCo2mDOlk7yAJW0l1u1Go5ijwiq7Zh/5EN/VH/8Abtb/AEf9y6Wm5EcZVDTkK/Aa7c6J/wBpDvy3Wa+2fMaXT0gyvR2OTuCLPsPLa7C3cEe+FUqPXo/GuFsrtIIh34X9D36hcBxHhFem7SabzzBYC9pHWQsPJyjXDTNqVFVqVU1w4tJBBBGCDgqjVrLnuVadLLqygayourKBqpFtcdXQKlwqj6qBUrJaLktvroTq6pOqqPmKpim5LTqyG6qga1BzlUiLRX1UF71BzkNxVSJtTLkkKUyrSdvVwphQBUgVyV3bSSCScBIbOEk4TwgbMFIJoThNIjQjMCCxWKQQF22psDS+oYA6blRPGGhkUmkcpPzFZ3F6xbDT8oAnv9VXtnNe/Bx06coXTj11Bx/db1PiDgwOzsSYgR9SQrNnWqVCNpOZPILneMVgzy6IP/UcC/8A2j1QB3hWb7jwt6L3sD3Oa0nTTBc8nk2YIEnmtZe9IynW3Y2r2B2hzvVvCs1KfOeYXkHgvid5fXoL2uDRqdUIY+kxgEQAXO9ZMkbDbuvZxR9MLfix2gHhoyma8PEtMpr6yL6ZaDEgwRuD1Xl3h2lxS24kaVRhdRl/mOLgWFpnTUYYkchpJPPZFEei1ap1Fh6SPZUX3xaCHDb9OqqcT4yKVxQFQaRWFQerGRp/qU3E6hDtIG4MH/SVzZZNsY5nxrbNLm1WaRqBDmYBkcx1XIPYu28TEOpsB+YE/wDbH9lzBtlhe6KyzSUHUlq/DJjaoSxX0lXqUVvOtEF9onKLGC6ioeUtp1moGzT2nTH8tQcxbBtEJ1onKmxjuahkLWfaITrRPknTM0pLQ+FSRyLT0QFSCg0ojVjp07TaiAKDUQJaG0mtU9KZqIAjQ2hpS0o7Wp9CehsJrVYpBQDUWmEtDZcXs/OoGPmbkfRYvhajqeXOEaN+5C6eh06qlZ2ga40m71SS6OVMnP15fVa43va5fxsctUDq3EwTIaPUCf5YgQPZdtRtWl2lgHf+sLF4xaGjfeZyLYg4GAIAVujxRzZAbk887clt47re0Z/lJp1HCqLGuOgNEfMRAkqh4i8c0rUim0a6n8oIgD3StbkU6QJJ9WTjV9149/EHhZNd1ZpqkOJIaI0z32+/suqfbnr1Tgf8QRWqCnVYWFxhrp9PsV2NbThx57FfLfh2z1VWPeKgLXyH6howciN5wea+lbe4D7b/AGtG/sjL6Ecp/FG08ynbvYJcysWAjHzscZ+7B909Em4oUKxcRpBZVHduM/ZG8U0XOoMEy5tRj2xkgAOz+ajw6mKQfQqEAVx5lMnDS5wyBOx2XF5JuunC6jGv3eY+eQwPZVfIV19MgwcEbhR0rFKn5CXkK3pShLYUjbqDrZXy1RLUbDNNqoOtFplqiWJ7DKNooOs1r6E5pJ7LTBdZoL7NdCaKE+gjZcXOm0TrcNukgcVlgRmhDYjsCjY2doUwEg1S0pbPZ2o7EJoRaYRsbGaFLSotCmnszQpNCZTYEbCxRC1OCWbWEua3cySZJn3Kz7dslbwboZHM7rp8GG7v+M88utMTxXbB4DxuOa5RwLmOJJ9AnVnEbD3XYXnqBB2XL3p0SD8p3G/1Wvlx1eR+PLrTN4d4pY+l5byGVGahDjEgZOfofsuc8Rcapk6nannk1hnqZJyI2/JU/F/D9FVrmiA7TkbTJ/UOIW9ZcBbXptqUw2HcubXA+ph9tlrLbjKvx+KZXKfxynAeKtdUANJzclw0ukEkzmQvXeBeJ21dNKmwmTFUgfI0TjbJkLz6w4H5TiCMhxYJ7Ohd54OtDNWqNqjmta7k4MGnV9TJU5Wrz8Mxxlrqbewa5/q9QBkTKn4nsA6n5gBlnTaO4V60pQFcq0tbCw/iBCfCXGxy3Lt5k90mTud00I1zbljy07tJCYNXnVtKFpTaUbSolqnY2EQokIpCiQjZgkJQplqbSnKSEJKRaolGzMUNydxUUbG0U6UJ0bBwjUyq8otNyGW1lhUyUNpRWtRYeyCLTSFNEYxLQ2cFSlRIUUHsTUpNegppQNtnhjvUFq3NcbLG4Q+CewRhcanO7L0f88/Fln7PdvkYXJcccR+63rq4A2d/Rc5xh+qGtMudgdAObz7frCPJV4MG4qMrtfRMSBDajswY27ZQfDHHxw6p8PetdTpvk+cQ57fMxDiRyiRO+30u0OGQ46ciNP8A591Yq2ZPpqhr2gS0uHqAmNJ677rPDyTGXbecpZYld8co1bgMsnCvWcf/AI20wXUw52NTnbCM916D4a4aLeiyiSCWSXRkBxOoge0wuS8PBlH5GMpkggloAPQGV1PCq5HzTPXqrnkl9F5cssvf6dI0YVlizqL5V+gto5q4fjlDTcPHeR9cqmGrrvEvDNY85u7cOH+nquXDVwebC45VeN6AcoFHqU0PQsaoKEtCMKak2mjRq/lqJYrhpqLqaejUnNQXhX301XqMSJTc1MGo5amLUhoLSkpynQpTcVOmUVtvKXkEJdxkLTcrlJyptolWKbSjYW5UgUFrSiNYUbp6J5Vdz8qy6mqrmGVNJMPTgqDGFTc1VD1VuxqwTnkVatwNBzucrKpughaNF/oMdV6P+S7xsZ5zsOtTaN/7rHfbNJJGHHpOByC1qty1o9UE9s/RUbmm4DWBAP4TurzPEKg/QdL2zMDVjfkStWvw0VRLfqOo3/osSpegiCQIznsRzWvw+9IwOcFY5Sem0tVK1kGep2wg/XotPhl018iRg4joqniQ+pgmAROnudysU3Yp/wDTgkR9PdTJxov5PQaVcgYytGyqk/N9OQXGcEv6jo1zB2lsD7rrKExzHtBXRhltllNNKoQ5paeYIXA1sOI6ErrrysadJzhkgc1xL3yZWP8ApvosBSVGEI1FHzlx2tIshOCqwqKWtLahi9NrVY1E7XJ7GxnFVnohKEUhtHSn0qbWqZVSDav5aSMYSRobTo0wnNMShsekamcJ2I2uttxCXkgJ6VcKNaoiyHySYBKOxqqU3o7KiIXIc0wqtRgU6tTCpioZU5XsSrQphDexD8+FB11lPo+Q4t5Vig0NBmN+aDSuEO8rz+y7P83VqPJdrFOmC6QAYRq9B1QRAjnIQOHV+S37YAraiPPPENNlOc5PJaXhavrptJ5emfbYrJ/ijbeXVpv2B1R7iFiWXGzTaGMkl2A0dSs/+le46nxPfa7ltOkNRAgNGcq74e8NnUalbLnQTyHsieEbQMZrfmo/L3GCfaei6u3uGiEtS9nvU0cUGtbpAEItCoCIzhCrV2zEhK1qZ7LTFFV/EVWKQbzcfyXN6VqeI601AP5QFmMMrl813mMfQFQIOkq5UppNasOC9wKmxSqNwjsamqhHAcopKbQiPEITHpcdDawGKvVZlH8xAfUkqrINi0mYQ6zEahUwgXFwG5PsnqaJDQUkbzAnT4wbZ7A4iVF1QhWQ4huya3tS45UUcUbaoSfqtJ1OQhW1iKcgSfVOe6vuGMJyUrAqFKUT4cjdBZdaHQQrla5lsgJzRaRbQhR+GnKi6vAn6p2XeMJ/iFY20oHwUFXXXHOFSqXeowNuqnoaDqV2SGAyecKN7ho94QWUWtcS3PUp7uajHR0x9Fr4s9ZQrFnhj4In7krreGmf8hefcE4hqEO3YYM9RzXacKvR1XZfYnpzf8ZKB+HouA2qlpPYsJ/Zee+HaMVg9/0C954vwll7buo1I9Ylrty1/wCF49v6rw3iFB9vUdTcIfRcWOHcHl2WXk3O2mDv7a8aAAyB2T23ETqIPPnyIXnNPjTu8rb4fxBzKbq1Q4y1gg5eVjN1pdO0uLppcPVmJwSJWtwysdOZxledeGHPe+XnVzk7yvQKlUMokjciPutsb+2OTI4hXL3l3VCFSEDiFVzWt0BpLnsB1fyT6j9kqQnc7YXJbSaAfqT7Kh6mgxvuFNhed+YBRyC057tYgektdJ6OGRj2lFe4QqzZ+yenS1E525KpQFXPJQp0zhEpQXQeUj+6Df13ARTMdHD9lP2NLD6aqVHhro6oHD/MAOvPdQuRLhG6nlv9C+mpUMBVnt1COmUStho1YjOVTFxDo6/ontXZnsdKStF7TzSQOK1ViCAZHLt2VinUGkEf5yWdVI8wRicOBxBiY/IKXF7xrNDW7loMcj6nT7Zj7p7klo21q1VrQHbyApfENOJAjecBYlSo5w0EENAw7bmIH6oDnGu2G5Hp1tIgxuc7Hr9E+XYtdBXLRDiJGDP7oNa79YYxmoO5/hHVZzOIkt8toEGGhvvt+QJRuH1dOtrtmj0vMZbnP0IhLl39BauMxkRsQfsCE1OoAdEZ/KVmcReXM9E4AkfiDpdMddkJr6msA5aBG3zS2Zz9FO+xuNKtcCIOM6SE1R7R6Y2AJKw+Jte4+gmXeoMzIDZMg9Eri6eC1xyHhpdEEkBrfT7kk/ZPf9G9LlvcjU4Dnmeya0vmkEA9Z9lz1zeOYT6okERmSJwR9CqRt3u1+UXAtBLoOTnTEfb8leMJfdisQPxuxH5LseDAyBPZebNqVGDXUcdTcNaNw7+Y+wXpPhKkXAP2HM/suzlMqnH07q0wAF45/Fqg6jxEvDTprsY8O5FwGlw98fmvXmXLRjpGFzXi68bUDDTLXOb5mDB+WCHZU+Szicuq8jtbRz8imdwBIOZnPthG4jULi1jJ00pA6Fx3d/nRdDUvHl7WwXQXDMxqkiT901rZU6Ze/cCZGHOIzkDaJC58PJO9r3sTwo0Ne3Vzj6grseOVmtDWHoXGO0Y+xBXKVrIvDvJ1BhxgkfKAS5p3icfRXLUVIp+YHVNDS10mdQ5ARHq5HcQ0ct1807kK91fs6LaoyCNW08m8h+/uVmXlF7bhlKiRp1O81x3DdJDY/wCX6K4ajm6n0oDgHNAO4LdiQd5lB4e9z9b3gBzm0xI/A9vztP8AyEz0IUctjTSbUYG5yY/MKlVusdzt9EClBa4ukTmnjJHMEdzkIzbHDXNkFkEg5BPKO249ys75LehYercAMDuePYzt+6s2TQWPqg/JJztH9FXbaQ3TPyE78ugI9kJxdofTYAGNYWhwk6g50FpntP5JzKz2NJi5a/QWjL8O5RzKd9AOMbYJHuM/59Vm2r/MdqDCA3W4ASMeoDH027K7w17g3W8fixMQBj0k85H6KZ5NjpA62g7dY7Ko9xcadRrmwX6TO8Qdu8hazLQPJe47RLTtkE//AJVGtTaSJaBokADbI+bHufupyysP6WeIVhUDG7uyDH8oiZQalIEZkGd4xHJVzSaxzSXTAJJOwJPqPfl9lq2lyXAkfK2RBAJI3aZ/ZPHK2051WBcteHkNLYGMxM8/zlJanMn4cOlzyHEkSC4kbdkye/ouId/eg+txOIcO7gIH+dlWtm+fprHdktg7acn9SgXbmvYBzH5odqCG6ZgDollj+W0S1sVNLyA8emZABIgAyFSfexUcwSGtBDO0nEkKvUeZhivW7QRJaNXPunjv9nalb05hweXOZocOQDwSSY6bD6K1VuJdkxOqAciJUrJgYCXRlULqrLsc0spqC0W4voMA77nutGxqamyTvkjbIGAsmhYh7gX+6vABroGwwplvspv9i3dcZAbLnc+8Z9lGpbMqQ+puwuOnrq0yf/r+ak2J1FC82TBGFpbLFT7Uxw2k9znuG4IA7ncjooUuGsn0yI0hzpOQO3P+yuVIBwgVq2kYG6cuj3ArngtKWuAJDSDE57g+612X/lU9DBpnn26rMta5Jyi1aevnsr52ehuC07xzqoOokCPaRElQrV26nE5zuMKm0+W6CmrZ2U7tK0YNbh4EQZ5c8lN5gfUmNxpJ/X7oLNvUpW9UA4yi/wAKNSAxmhhEEzHbooWkguLhjkh+ZiUJ1d3JVqH2lSonUXAmSSjioKWWGCdz190A1oampjXuos/g9LFOH7/gPpjGP3VujdtBId0075Va2o6RBVG/cGnCmelel62fpJA2J55VuuGEEj8W45Yz+yxrO8E5Vx1y1E36EFs3gamuiHc9io1Kg1aDloEgHaVRBLjlSIMiUu9ahWLdOqCCBA5CP3Q3kNaZzP6pmtgodzUxnrCWUG6p3dkXwQcolq4thhKnReQZ5KvUBLtQOyUw/Za7ErWjtR0uxyyks2pxB0lJPgFdit0EkldTEbT51p0vmSSSFWahws5/zBJJPL0nJpWyVRJJZfpaVPZSbukkqwUFdrPqlJJaVCzaIvVMkrvoM28PrRKCSSIR7j5Ss21PqSSU32cbTdk1VJJNpPaDj6VY4ckkgX2uVisq7SSU08vSswbq0E6SScfY9PZTKSSeK6MxVbnknSRmKASn/CfZMkqnpE9ueq7lOkkkl//Z',
              enable:false,
              candidatesSelected:["",""]
            },
          ],
          title: "Lista de voto preferencial",
          description : "Si desea coloque dentro de los recuadros dos numeros de los representantes del partido político.",
          politicalPartiesSelected: {
            id:-2,
            candidates:[],
            candidatesSelected:["",""],
            image: "",
            politicalPartie: ""
          },
          blankVote: false,
          
        }
      }

    componentDidMount(){
      this.setState({
        description: this.props.description,
        titple: this.props.title
      })
    }

    handleSelection = (e,index) => {
      console.log(e.checked,e.name,index);
      console.log(index);
      let selected = {};

      //To change the selection
      if (index  != -1){
        selected = {
          ...this.state.data[index],
          candidatesSelected : [],
          indexList : index
        }
        this.setState({blankVote:false})
      }
      else {
        selected = {
          id: -1,
          politicalPartie: "VOTO EN BLANCO",
          candidate: "Voto en blanco",
          image: "",
          candidatesSelected : [],
        }
        this.setState({blankVote:true})
      }

      //To enable the input for preferential candidates

      let auxData = this.state.data;
      auxData.map((item,indexList)=> {
        if (indexList === index){
          auxData[indexList].enabled=true;
        }else {
          auxData[indexList].enabled=false;
          auxData[indexList].candidatesSelected= ["",""]
        }
      })
      

      //Update data
      this.setState({
        politicalPartieSelected: selected,
        data : auxData
      });                                   
      console.log("Selected", selected, 'index: ', index);
    }

    handleChangePreferential = (e,pos,index) => {
      let auxData = this.state.data;
      auxData[index].candidatesSelected[pos]= e.value;
      this.setState({data: auxData});
      console.log(auxData)
    }

    render() {
      const {data,title, description} = this.state;
        return (
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card card-primary card-outline" style={{borderTop:'3px solid #17a2b8'}}>
                    <div className="card-header">
                      <h2 className="card-title">
                        <b>{title}</b>
                      </h2>
                    </div>
                    <div className="card-body">
                        <div className="alert alert-info" style={styleAlertInfo} role="alert">
                            {description}
                        </div> 

                        <div className="form" style={{border: '1px solid rgb(23, 162, 184)'}}>
                            {data.map((item,index) => {
                              return (
                                <div className="row">
                                  <div className="col-9">
                                      <div className="inputGroup row">
                                          <input 
                                            className="col"
                                            id={"radioPreferencial"+index} 
                                            //name="checkboxVotoPreferencial" 
                                            //type="checkbox"
                                            name="radioVotoPreferencial" 
                                            type="radio" 
                                            onChange = {(e) => this.handleSelection(e.target,index)} 
                                          />
                                          <label htmlFor={"radioPreferencial"+index} className="row">
                                              <div className="col" style={{width:'30%'}}>
                                                  <a>{item.politicalPartie} <br/></a>
                                              </div>
                                              <div className="col" style={{width:'70%'}}>
                                                <img style={{height:'112px' ,width:'150px'}} src={item.image}></img>
                                            </div>
                                          </label>
                                      </div>
                                  </div>
                                  <div className="col-3">
                                      <div className="inputGroupPreferencial row form-inline">
                                        <div className="col">
                                          <input 
                                              className="form-control" 
                                              type="text" 
                                              maxLength="2" 
                                              onChange = {(e) => this.handleChangePreferential(e.target,0,index)}  
                                              disabled ={!item.enabled} 
                                              value = {item.candidatesSelected[0]}
                                            />
                                        </div>                                            
                                        <div className="col">
                                          <input 
                                            className="form-control" 
                                            type="text" 
                                            maxLength="2"
                                            onChange = {(e) => this.handleChangePreferential(e.target,1,index)}  
                                            disabled ={!item.enabled}
                                            value = {item.candidatesSelected[1]}
                                          />
                                        </div>
                                      </div>
                                  </div>
                              </div>)
                            })}


                            <div className="row">
                                  <div className="col-9">
                                      <div className="inputGroup row">
                                          <input 
                                            className="col" 
                                            id="radioPreferencial-blanco" 
                                            name="radioVotoPreferencial" 
                                            type="radio" 
                                            onChange = {(e) => this.handleSelection(e.target,-1)} 
                                          />
                                          <label htmlFor={"radioPreferencial-blanco"} className="row">
                                            <div className="col" style={{width:'30%'}}>
                                                  <a>VOTO EN BLANCO </a>
                                            </div>
                                            <div className="col" style={{width:'70%'}}>
                                                <div style={{maxWidth:'150px', border: '1px solid black'}}> <h3 style={{padding:'5px'}}> VOTO EN BLANCO</h3> </div>
                                            </div> 
                                          </label>
                                          
                                      </div>
                                  </div>
                                  <div className="col-3">
                                      <div className="inputGroupPreferencial row form-inline">
                                          <div className="col">
                                            <input 
                                              className="form-control" 
                                              type="text" 
                                              maxLength="2" 
                                              disabled
                                            />
                                          </div>                                            
                                          <div className="col">
                                            <input 
                                              className="form-control" 
                                              type="text" 
                                              maxLength="2" 
                                              disabled 
                                            />
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            
                        </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
    }
  }
    
    const styleAlertInfo = {
        color: '#0c5460',
        backgroundColor: '#d1ecf1',
        borderColor: '#bee5eb',
    }