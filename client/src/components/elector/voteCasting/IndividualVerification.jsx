import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import Results from '../containers/Results';
import axios from 'axios';
import {connect} from 'react-redux';

class IndividualVerification extends Component {
  constructor(props) {
    super();
    this.state = {
    }
  }
  handleClickResultados (e) {
    e.preventDefault();
    this.props.handleNextComponent(Results);
  } 
  componentDidMount() {

  }
  render() {
    console.log(this.props);
    return (
      <div>
        <section 
        className="content"
        style={{ maxWidth: "1100px", margin: "auto",paddingTop:'130px'}}
        >
              <div className="row" style={{marginLeft:'0px', marginRight:'0px'}}>
                  <h2 style={{margin: 'auto', height:'50px'}}> <b>¡Su proceso de emisión de voto ha concluido!</b></h2>
              </div>
        </section>
        <section>
          <Card>
              <Card.Body>
                <div className="row">
                  <i class="fa fa-check-circle-o" aria-hidden="true" style={{margin:'auto', borderColor:"#17A2B8",color:"#17A2B8", zoom:13}}></i>
                </div>
                <div className="row">
                  <h4 style={{margin: 'auto', height:'50px'}}> <b>Voto exitoso</b></h4>
                </div>
                
                  {this.props.votingList.map((item,index)=> {
                    console.log(item);
                    return (
                      <div className="row" style={{margin:'auto', width:'fit-content'}}>
                          <div  className="col">
                            <p>Su identificador para la {" "+ item.title} es "1-1-1-1"</p>
                          </div>
                      </div>
                    )
                  })}
                  <div className="row">
                  <a style={myStyle} onClick = {(e) => this.handleClickResultados(e)}>Ir a resultados</a>
                </div>
              </Card.Body>
          </Card>
        </section>
      </div>
    )
    }
}
  
const mapStateToProps = (state) => {
  return {
      politicalPartiesList: state.politicalPartiesList,
      electoralProcess: state.electoralProcess,
      votingList: state.votingList,
      electorSelections: state.electorSelections,
  }    
}


export default connect(mapStateToProps)(IndividualVerification);

const myStyle= {
  margin: 'auto', height:'50px', cursor: 'pointer',color:'#007bff', textDecoration:"none", backgroundColor:'transparent'
}