import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <footer className="footer" style={footerStyle}>
                <div className="float-right d-none d-sm-block">
                    <b>Versión</b> 1.0
                </div>
                <strong>Copyright © 2020 <a href="#">VoteTESIS</a>.</strong> Todos los derechos reservados
            </footer>
        )
    }
}

var footerStyle = {
    background: '#fff',
    borderTop: '1px solid #dee2e6',
    color: '#869099',
    padding: '1rem',
    marginTop: '10px'
}

// export default class Footer extends Component {
//     render() {
//         return (
//         <footer className="w3-center w3-black w3-padding-64" >
//             <h6><strong>Copyright © 2020 <a href="#">VoteTESIS</a>.</strong></h6>
//             <a href="#" className="w3-button w3-light-grey w3-margin"><i className="fa fa-arrow-up w3-margin-right" />Volver al inicio</a>
//             <div className="float-center d-none d-sm-block">
//             <span>Todos los derechos reservados</span>
//             </div>
//         </footer>
//         )
//     }
// }



