import React, { Component } from "react";
import { connect } from "react-redux";
import $ from "jquery";
import Cookies from "universal-cookie";
import "./elector_style.css";
import getWeb3 from "../../getWeb3";
import Election from "../../contracts/Election.json";
import Spinner from "react-bootstrap/Spinner";

const cookies = new Cookies();

class LoginElector extends Component {
  constructor(props) {
    super();
    this.state = {
      form: {
        dni: "",
      },
      storageValue: 0,
      web3: null,
      accounts: null,
      contract: null,
      msgAlert: false,
    };
  }

  componentDidMount = async () => {
    const elector = cookies.get("elector", { path: "/elector" });
    //console.log("elector", elector);
    if (elector) {
      //If I'm already logged
      window.location.href = "./elector";
    }

    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Election.networks[networkId];
      const instance = new web3.eth.Contract(
        Election.abi,
        deployedNetwork && deployedNetwork.address
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({ web3, accounts, contract: instance });
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check //console for details.`
      );
      //console.error(error);
    }

    //console.log(this.props);

    const varInput = "input-dni";
    const varButton = "input-button-elector";

    let myInput = document.getElementById(varInput);
    myInput.addEventListener("keyup", function (e) {
      if (e.keyCode === 13) {
        e.preventDefault();
        document.getElementById(varButton).click();
      }
    });
  };

  handleSubmit = async () => {
    const form = this.state.form;

    if (form.dni.length === 0) {
      alert("Introduzca su DNI para iniciar sesion");
      return;
    } else {
      this.setState({ loadingLogin: true });
      const { contract, accounts } = this.state;
      try {
        const response = await contract.methods
          .validateLoginElector(accounts[0], form.dni)
          .call();
        console.log("response",response);
        if (response.flag) {
          console.log("response: ", response);
          this.setState({ msgAlert: false });

          const elector = {
            DNI: form.dni,
            account: this.state.accounts[0],
            castVote: response._castVote,
            email: response._email
          };

          cookies.set("elector", elector, { path: "/elector" });

          const contract = this.state.contract;
          cookies.set("contract", contract, { path: "/elector" });
          console.log(elector);
          window.location.href = "./elector";
        } else {
          if (response.username.localeCompare("0") === 0) {
            // Isnot the account for this credential
            alert(
              "La cuenta " +
                accounts[0] +
                " no esta asociado al dni: " +
                form.dni
            );
            this.setState({ loadingLogin: false });
            return;
          }
          if (response.username.localeCompare("1") === 0) {
            // Is not the correct password
            this.setState({ loadingLogin: false, msgAlert: true });
          }
        }
      } catch (err) {
        this.setState({ loadingLogin: false });
        console.log(err);
        alert("No esta registrado en el sistema!");
        return;
      }
    }
  };

  handleChange = async (e) => {
    await this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });

    //console.log(this.state.form);
  };

  render() {
    if (!this.state.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    return (
      <div style={{ paddingTop: "50px" }} className="background-login">
        <div className="login-box" style={styles.body}>
          <div className="login-logo">
            <img
              src="images/logo.png"
              alt="BlockVoting Logo"
              className="brand-logo"
              style={{
                opacity: ".8",
                marginRight: "6px",
                maxHeight: "90px",
                marginBottom: "10px",
              }}
            />
          </div>
          {/* /.login-logo */}
          <div className="card">
            <div className="card-header p-2">
              <ul className="nav nav-pills">
                <h4 style={{ margin: "auto" }}>Iniciar sesión</h4>
              </ul>
            </div>
            <div className="card-body">
              <div>
                {/* /.INICIAR SESION - ELECTOR */}
                <p className="login-box-msg">
                  Iniciar sesión en la plataforma de voto electrónico
                </p>
                {/* /.tab-pane */}
                <div className="tab-pane" id="admin">
                  <div className="form-admin">
                    {/* /.INICIAR SESION - ELECTOR */}
                    <div className="input-group mb-3">
                      <input
                        id="input-dni"
                        maxLength="10"
                        type="text"
                        className="form-control"
                        name="dni"
                        placeholder="Ingrese su DNI"
                        onChange={(e) => {
                          this.handleChange(e);
                        }}
                        required
                      />
                      <div className="input-group-append">
                        <div className="input-group-text">
                          <span className="fas fa-envelope" />
                        </div>
                      </div>
                    </div>

                    <div
                      style={{
                        display: this.state.msgAlert ? "block" : "none",
                      }}
                    >
                      <div
                        className="alert alert-danger alert-dismissible"
                        style={styleAlert}
                        role="alert"
                      >
                        Contraseña y/o usuario incorrectos!
                      </div>
                    </div>
                    <button
                      id="input-button-elector"
                      type="submit"
                      className="btn btn-block btn-primary "
                      name="submit_elector"
                      onClick={() => this.handleSubmit()}
                      disabled={this.state.loadingLogin ? true : false}
                    >
                      <Spinner
                        style={{
                          display: this.state.loadingLogin
                            ? "inline-block"
                            : "none",
                        }}
                        as="span"
                        animation="grow"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                      />
                      Iniciar sesión
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />

        <div
          style={{
            textAlign: "center",
            margin: "auto",
            opacity: "0.78",
            color: "#fff !important",
            backgroundColor: "#000!important",
            padding: "12px 24px!important",
          }}
        >
          <span className="w3-center w3-padding-large w3-black w3-large w3-wide w3-animate-opacity">
            SU CUENTA ES
            <span className="w3-hide-small" style={{ fontSize: "small" }}>
              :{this.state.accounts[0]}
            </span>
          </span>
        </div>
      </div>
    );
  }
}

var styles = {
  body: {
    margin: "auto",
  },
};

var styleAlert = {
  color: "#721c24",
  backgroundColor: "#f8d7da",
  borderColor: " #f5c6cb",
  fontSize: "1rem",
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginElector);

// export default class LoginElector extends Component {
//     constructor(props) {
//       super();
//       this.state = {
//           file_QR: []
//       }
//     }

//     componentDidMount() {
//       //JQUERY for update photo
//       var btnUpload = $("#upload_file"),
//         btnOuter = $(".button_outer");
//       btnUpload.on("change", function (e) {
//         var ext = btnUpload.val().split(".").pop().toLowerCase();
//         if ($.inArray(ext, ["gif", "png", "jpg", "jpeg"]) === -1) {
//           $(".error_msg").text("No es una imagen ...");
//         } else {
//           $(".error_msg").text("");
//           btnOuter.addClass("file_uploading");
//           setTimeout(function () {
//             btnOuter.addClass("file_uploaded");
//           }, 3000);
//           var uploadedFile = URL.createObjectURL(e.target.files[0]);
//           setTimeout(function () {
//             $("#uploaded_view")
//               .append('<img src="' + uploadedFile + '" />')
//               .addClass("show");
//           }, 3500);
//         }
//       });
//       var _this = this;
//       $(".file_remove").on("click", async function (e) {
//         $("#uploaded_view").removeClass("show");
//         $("#uploaded_view").find("img").remove();
//         btnOuter.removeClass("file_uploading");
//         btnOuter.removeClass("file_uploaded");
//         _this.setState({file_QR: []});
//         //console.log(this.state)

//       });
//     }

//     handleChange=async e => {
//       await this.setState({
//           ...this.state.form,
//           [e.target.name]: e.target.files
//       })
//       //console.log(this.state);
//     }

//     handleLogin = () => {
//       //console.log(this.state.file_QR);
//       if (this.state.file_QR.length > 0) {
//         const dataUser = {
//           type: 'elector',
//           lastName : 'Sanchez Herrera',
//           name: "Sebastian",
//           privateKey : "sdffg43245hfdae",
//           publicKey : "sdffg43245hfdae"
//         }
//         cookies.set('user',dataUser,{path: "/elector"});
//         //console.log("Iniciar sesion Elector")
//         window.location.href="./elector"

//       }else {
//         alert("Debe ingresar una imagen del código QR que se le asigno!")
//       }
//     }

//     render() {
//         return (
//             <div className="background-login" style={{paddingTop:'50px'}}>
//               <div className="login-box" style={styles.body}>
//                 <div className="login-logo">
//                 <img
//                   src="images/logo.png"
//                   alt="AdminLTE Logo"
//                   className="brand-logo"
//                   style={{ opacity: ".8", marginRight:'6px', maxHeight:'33px'}}
//                 />
//                   </div>
//                 {/* /.login-logo */}
//                 <div className="card">
//                   <div className="card-header p-2">
//                     <ul className="nav nav-pills">
//                         <h4 style={{margin:'auto'}} >
//                           Iniciar sesión
//                         </h4>
//                     </ul>
//                   </div>
//                   {/* /.card-header */}
//                   <div className="card-body">
//                       {/* /.INICIAR SESION - ELECTOR */}
//                     <p className="login-box-msg">Iniciar sesión en la plataforma de voto electrónico</p>
//                       <div className="form-elector" id="voter">
//                           {/* Load Image */}
//                           <div className="container">
//                             <div className="panel">
//                               <div className="button_outer">
//                                 <div className="btn_upload">
//                                   <input
//                                     type="file"
//                                     id="upload_file"
//                                     name ="file_QR"
//                                     accept='image/*'
//                                     style={{cursor:'pointer'}}
//                                     onChange= {this.handleChange}/>
//                                   <a>Subir Imagen</a>
//                                 </div>
//                                 <div className="processing_bar" />
//                                 <div className="success_box" />
//                               </div>
//                             </div>
//                             <div className="error_msg" />
//                             <div className="uploaded_file_view" id="uploaded_view">
//                               <span className="file_remove">X</span>
//                             </div>
//                           </div>

//                           <button
//                             type="submit"
//                             className="btn btn-block btn-primary "
//                             name= "submit_elector"
//                             onClick = {() => this.handleLogin()}
//                           >
//                             Iniciar sesión
//                           </button>
//                         </div>
//                     {/* /.tab-content */}
//                   </div>
//                   {/* /.card-body */}
//                 </div>
//               </div>
//             </div>
//           )
//     }
// }

// var styles = {
//     body: {
//       margin: 'auto'
//     },
// };
