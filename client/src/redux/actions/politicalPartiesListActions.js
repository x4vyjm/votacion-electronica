export const typeHandleChangePoliticalPartiesList = 'handleChangePoliticalPartiesList';
export const typeHandleAddPoliticalPartie = 'addPoliticalPartie';
export const typeHandleDeletePoliticalPartie = 'deletePoliticalPartie';
export const typeHandleLogo = 'handleLogo';
export const typeHandleAddCandidate = 'handleAddCandidate';
export const typeHandleDeleteCandidate = 'handleDeleteCandidate';
export const typeHandleSavePoliticalParties = 'handleSavePoliticalParties';
export const typeHandleCancelPoliticalParties = 'handleCancelPoliticalParties';


const  handleChangePoliticalPartiesList = (index, value, name) => {
    return {
        type: typeHandleChangePoliticalPartiesList,
        payload: {name: name, value:value, index : index}, 
    };
}

const handleAddPoliticalPartie = (item) => {
    return {
        type: typeHandleAddPoliticalPartie,
        payload: item
    };
}

const handleDeletePoliticalPartie = (index) => {
    return {
        type: typeHandleDeletePoliticalPartie,
        payload: {
            index: index
        }
    };
}

const handleLogo = (item,index) => {
    return {
        type: typeHandleLogo,
        payload: {
            item: item,
            index: index
        }
    };
}

const handleAddCandidate = (item,index) => {
    return {
        type: typeHandleAddCandidate,
        payload: {
            item: item,
            index: index
        }
    };
}

const handleDeleteCandidate = (position,index) => {
    return {
        type: typeHandleDeleteCandidate,
        payload: {
            position: position,
            index: index
        }
    };
}

const handleSavePoliticalParties = (value) => {
    return {
        type: typeHandleSavePoliticalParties,
        payload: value
    };
}
const handleCancelPoliticalParties = () => {
    return {
        type: typeHandleCancelPoliticalParties,
    };
} 
export {handleSavePoliticalParties, handleCancelPoliticalParties, handleChangePoliticalPartiesList, 
    handleAddPoliticalPartie, handleDeletePoliticalPartie, handleLogo, handleDeleteCandidate, handleAddCandidate};
