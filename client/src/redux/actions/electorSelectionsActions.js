export const typeHandleChangeSelectionCandidate = 'handleChangeSelectionCandidate';
export const typeHandleGeneralInformationSelection = 'handleGeneralInformationSelection';

const handleChangeSelectionCandidate = (selected, indexInList, posVotingList) => {
    return {
        type: typeHandleChangeSelectionCandidate,
        payload: {selected: selected, indexInList: indexInList, posVotingList: posVotingList}, 
    };
}
const handleGeneralInformationSelection = (publicKey,DNI, lengthVotingList)=> {
    return {
        type: typeHandleGeneralInformationSelection,
        payload: {publicKey: publicKey,DNI: DNI, lengthVotingList: lengthVotingList}
    }
}

export  { handleChangeSelectionCandidate, handleGeneralInformationSelection };