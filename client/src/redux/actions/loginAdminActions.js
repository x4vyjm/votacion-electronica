export const typeHandleChangeLogin = 'handleChangeLogin';
export const typeHandleSubmitLogin = 'handleSubmitLogin';
export const typeHandleLogOut = 'handleLogOut';


const handleSubmitLogin = () => {
    return {
        type: typeHandleSubmitLogin,
    };
}
const handleChangeLogin = (value, name) => {
    return {
        type: typeHandleChangeLogin,
        payload: {name: name, value:value}, 
    };
}

const handleLogOut = (value, name) => {
    return {
        type: typeHandleLogOut,
        payload: {name: name, value:value}, 
    };
}

export {handleChangeLogin, handleSubmitLogin,handleLogOut};
