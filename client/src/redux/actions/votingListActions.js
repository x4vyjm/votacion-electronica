export const typeHandleChangeVotingList  = 'handleChangeVotingList ';
export const typeHandleAddVotingList = 'handleAddVotingList';
export const typeHandleDeleteVotingList = 'handleDeleteVotingList';
export const typeHandleDeleteIteminVotingList = 'handleDeleteIteminVotingList';
export const typeHandleAddIteminVotingList = 'handleAddIteminVotingList';
export const typeHandleChangeSelection = 'handleChangeSelection';
export const typeHandleChangeCheckPoliticalPartie = 'handleChangeCheckPoliticalPartie';
export const typeHandleChangeCandidates = 'handleChangeCandidates';
export const typeHandleUpdateVotingList = 'handleUpdateVotingList';
export const typeHandleSaveListVoting = 'handleSaveListVoting';
export const typeHandleCancelListVoting = 'handleCancelListVoting';

const handleChangeCandidates = (name, index, indexPoliticalPartie, value) => {
    return {
        type: typeHandleChangeCandidates,
        payload:{name: name,index: index,indexPoliticalPartie: indexPoliticalPartie, value: value}
    }
}

const  handleChangeVotingList  = (index, value, name) => {
    return {
        type: typeHandleChangeVotingList ,
        payload: {index:index, name: name, value:value}, 
    };
}

const  handleChangeSelection  = (index, value, name) => {
    return {
        type: typeHandleChangeSelection ,
        payload: {index:index, name: name, value:value}, 
    };
}

const handleAddVotingList = (politicalParties) => {
    return {
        type: typeHandleAddVotingList,
        payload: politicalParties
    };
}

const handleDeleteVotingList = (index) => {
    return {
        type: typeHandleDeleteVotingList,
        payload: index,
    };
}

const handleDeleteIteminVotingList = (item) => {
    return {
        type: typeHandleDeleteIteminVotingList,
        payload: item,
    };
}
const handleAddIteminVotingList = (item) => {
    return {
        type: typeHandleAddIteminVotingList,
        payload: item,
    };
}

const handleChangeCheckPoliticalPartie = (index,position) => {
    return {
        type : typeHandleChangeCheckPoliticalPartie,
        payload:{index: index, position: position}
    }
}

const handleUpdateVotingList = (name, item ) => {
    return {
        type: typeHandleUpdateVotingList,
        payload: {name: name, item: item}
    }
}
const handleSaveListVoting = (value) => {
    return {
        type: typeHandleSaveListVoting,
        payload: value
    };
}
const handleCancelListVoting = () => {
    return {
        type: typeHandleCancelListVoting,
    };
} 

export {
    handleSaveListVoting, handleCancelListVoting,
    handleUpdateVotingList, 
    handleChangeCandidates, handleChangeSelection, handleChangeCheckPoliticalPartie, 
    handleChangeVotingList, handleAddVotingList, handleDeleteVotingList, 
    handleDeleteIteminVotingList,handleAddIteminVotingList};
