import { createStore, combineReducers } from 'redux';
import politicalPartiesList from './reducers/politicalPartiesList';
import electoralProcess from './reducers/electoralProcess';
import votingList from './reducers/votingList';
import dataLogin from './reducers/loginAdmin';
import voters from './reducers/voters';
import electorSelections from './reducers/electorSelections';
import encryption from './reducers/encryption';

const reducer = combineReducers({
    dataLogin,
    politicalPartiesList,
    electoralProcess,
    votingList,
    voters,
    electorSelections,
    encryption
});

const store = createStore(reducer);

export default store;