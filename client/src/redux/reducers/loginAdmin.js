import {typeHandleChangeLogin as handleChangeLogin} from '../actions/loginAdminActions';
import {typeHandleSubmitLogin as handleSubmitLogin} from '../actions/loginAdminActions';
import {typeHandleLogOut as handleLogOut} from '../actions/loginAdminActions';


const defaultState = {
    username: '',
    password: '',
    msgAlert: false,
    lastName: '',
    name:'',
    idUser:-1,
    dataCompleted: false,
    loged: false,
};


function reducer(state = defaultState, {type,payload}) {
    switch (type) {
        case handleSubmitLogin : {
            let newState = {...state};
            if (newState.username.localeCompare("")!== 0 || newState.password.localeCompare("")!== 0){
                newState.id =1;
                newState.lastName ="Sanchez Herrera";
                newState.name = "Sebastian";
                newState.loged = true;
                console.log(newState);
                newState.dataCompleted = true;
            }
            else {
                newState.dataCompleted = false;
                console.log("Datos incompletos")
            }
            return newState;
        }
        case handleChangeLogin : {
            console.log(type,payload)
            return {
                ... state,
                [payload.name]: payload.value,
            }
        }
        case handleLogOut : {
            return {
                ... state,
                [payload.name]: payload.value,
            }
        }
        default:
            return state;
    }
}

export default reducer;