
import {typeHandleChangePoliticalPartiesList as handleChangePoliticalPartiesList} from '../actions/politicalPartiesListActions';
import {typeHandleAddPoliticalPartie as handleAddPoliticalPartie} from '../actions/politicalPartiesListActions';
import {typeHandleDeletePoliticalPartie as handleDeletePoliticalPartie} from '../actions/politicalPartiesListActions';
import {typeHandleLogo as handleLogo}  from '../actions/politicalPartiesListActions';
import {typeHandleAddCandidate as handleAddCandidate}  from '../actions/politicalPartiesListActions';
import {typeHandleDeleteCandidate as handleDeleteCandidate}  from '../actions/politicalPartiesListActions';
import {typeHandleSavePoliticalParties as handleSavePoliticalParties}  from '../actions/politicalPartiesListActions';
import {typeHandleCancelPoliticalParties as handleCancelPoliticalParties}  from '../actions/politicalPartiesListActions';

const initialState = [];

function reducer(state=initialState, {type,payload}) {
    switch (type) {
        case handleSavePoliticalParties: {
            console.log(payload);
            //Llamar al servicio para que actualize la blockchain
            return payload;
        }
        case handleCancelPoliticalParties: {
            //Llamar al servicio para que se resetee todos los valores del state
            return state;
        }

        case handleChangePoliticalPartiesList: {
            let newState = [...state];
            newState[payload.index][payload.name] = payload.value;
            console.log(newState)
            return newState;
        }
        case handleAddPoliticalPartie : {
            let newState = [...state];
            const item =  {
                name: "",
                partieLogo: [],
                candidates: [],
            };

            newState.push(item);
            console.log("state: ",state," newState: ",newState," item:", item);
            return newState;
        }
        case handleDeletePoliticalPartie : {
            console.log(state)
            let newState = [...state];
            newState.splice(payload.index, 1);
            return newState;
        }
        case handleLogo : {
            console.log(payload)
            console.log("before change: ", state, "partieLogo: ",state[payload.index].partieLogo );
            console.log(state, payload);
            //let logos =  state.logos;
            let newState = [...state];
            //logos.push(state[payload.index].partieLogo);
            // newState[state.length - payload.index - 1].partieLogo = {
            //     name: payload.item.name,
            //     src: payload.item.preview,
            //     size: payload.item.size,
            // };
            newState[payload.index].partieLogo = payload.item;
            console.log("after change: ", newState,  "partieLogo: ",newState[payload.index].partieLogo );
            
            //console.log(state, payload,newState);
            return newState;
        }
        case handleAddCandidate : {
            console.log("handleAddCandidate")
            let newState = [...state];
            console.log(payload);
            if (payload.item.length !== 0) {
                newState[payload.index].candidates.push({ 
                    value: payload.item,
                    label: payload.item
                });
               
                return newState; 
            }
            else {
                console.log("la cadena es vacia")
                return state;
            }
            
        }
        case handleDeleteCandidate : {
            let newState = [...state];
            newState[payload.position].candidates.splice(payload.index, 1);
            return newState;
        }
        default:
            return state;
    }
}

export default reducer;