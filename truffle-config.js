const path = require("path");

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*", //match any network
      //websockets: true,
      //gas: 281474976710655,
      //pasPrice: 123,

    },
  },
  compilers: {
    solc: {
      version: "0.5.8",
      // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
      settings: {
        // See the solidity docs for advice about optimization and evmVersion
        optimizer: {
          enabled: true,
          runs: 20,
        },
        //  evmVersion: "byzantium"
      },
    },
  },
};
